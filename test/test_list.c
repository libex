#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "../include/libex/str.h"
#include "../include/libex/list.h"

typedef struct {
    int x;
    cstr_t *s;
} item_t;
void on_item_free (item_t *item) {
    free(item->s);
}

DECLARE_LIST(test_list, item_t)
DECLARE_LIST_FUN(test_list, item_t)

void test_01 (void) {
    test_list_t list, *l = &list;
    item_t x;
    memset(&list, 0, sizeof(test_list_t));
    l->on_free = on_item_free;
    for (int i = 0; i < 10; ++i) {
        x.x = i + 1;
        x.s = cstrfmt("%d", x.x);
        item_t *item = add_tail_test_list(l, &x, 0);
    }
    foreach_test_list(l, ({
        int fn (item_t *item, void *dummy) {
            printf("%d: %s\n", item->x, item->s->ptr);
            return 0;
        } fn;
    }), NULL, 0);
    x.x = 10;
    item_t *y = find_test_list(l, ({
        int fn (const item_t *x, item_t *y) {
            return x->x == y->x ? CF_FOUND : CF_NOTFOUND;
        } fn;
    }), &x);
    if (y) {
        printf("found: %d: %s\n", y->x, y->s->ptr);
        item_t *p;
        p = next_test_list(y);
        printf("next: %d: %s\n", p->x, p->s->ptr);
        p = prev_test_list(y);
        printf("prev: %d: %s\n", p->x, p->s->ptr);
        del_test_list(y, CF_FREE);
    }
    foreach_test_list(l, ({
        int fn (item_t *item, void *dummy) {
            printf("%d: %s\n", item->x, item->s->ptr);
            return 0;
        } fn;
    }), NULL, 0);
    printf("backward:\n");
    foreach_test_list(l, ({
        int fn (item_t *item, void *dummy) {
            printf("%d: %s\n", item->x, item->s->ptr);
            return 0;
        } fn;
    }), NULL, ENUM_BACKWARD);
    clear_test_list(l);
}

int main (int argc, const char *argv[]) {
#ifndef __GNUC__
    libex_str_init();
#endif
    test_01();
    return 0;
}
