#include <stdio.h>
#include <stdint.h>
#include <time.h>
#include "../include/libex/str.h"
#include "../include/libex/array.h"

typedef struct {
    int x;
    char *y;
} kp_t;

int print_kp (kp_t *v, void *dummy) {
    printf("%d: %s\n", v->x, v->y);
    return 0;
}

void clear_kp (kp_t *v) {
    if (v->y)
        free(v->y);
}

int cmp_kp (kp_t *x, kp_t *y) {
    if (x->x > y->x) return 1;
    if (x->x < y->x) return -1;
    return 0;
}

kp_t copy_kp (kp_t *x) {
    kp_t y;
    y.x = x->x;
    y.y = strdup(x->y);
    return y;
}

DECLARE_ARRAY(keyval,kp_t)
DECLARE_ARRAY_FUN(keyval,kp_t)
void test_array3 (int chunk_size) {
    kp_t kp, *v, t;
    printf("\ntest array, chunk_size = %d\n", chunk_size);
    keyval_t *a = create_keyval(8, chunk_size, CF_REDUCE);
    a->on_free = clear_kp;
    for (int i = 0; i < 10; ++i) {
        kp.x = i;
        if ((v = add_keyval(&a, &kp))) {
            v->y = malloc(8);
            snprintf(v->y, 8, "%d", i);
        }
    }
    t.x = 4;
    if ((v = find_keyval(a, cmp_kp, &t))) {
        printf("Found: %d: %s\n", v->x, v->y);
        del_keyval(&a, v);
    } else
        printf("Not found\n");
    foreach_keyval(a, print_kp, NULL, 0);
    free_keyval(&a);
}

void test_array31 (void) {
    keyval_t *a = create_keyval(8, 8, 0), *b;
    a->on_free = clear_kp;
    a->on_copy = copy_kp;
    for (int i = 0; i < 10; ++i) {
        kp_t kp = { .x = i, .y = strdup("abc") };
        add_keyval(&a, &kp);
    }
    b = copy_keyval(a);
    foreach_keyval(a, print_kp, NULL, 0);
    printf("\n");
    foreach_keyval(b, print_kp, NULL, 0);
    free_keyval(&b);
    free_keyval(&a);
}

DECLARE_SORTED_ARRAY(u_keyval,kp_t)
DECLARE_SORTED_ARRAY_FUN(u_keyval,kp_t)
void test_array4 (int chunk_size) {
    printf("\ntest sorted array, chunk_size = %d\n", chunk_size);
    kp_t v, *vptr;
    u_keyval_t *a = create_u_keyval(8, chunk_size, CF_UNIQUE | CF_REDUCE);
    a->on_free = clear_kp;
    a->on_compare = cmp_kp;
    srand(time(0));
    for (int i = 0; i < 10; ++i) {
        v.x = rand();
        v.y = NULL;
        if ((vptr = add_u_keyval(&a, &v))) {
            vptr->y = malloc(16);
            snprintf(vptr->y, 16, "%d", vptr->x);
        }
    }
    v.x = 1000000000;
    if ((vptr = add_u_keyval(&a, &v))) {
        kp_t *fptr;
        vptr->y = malloc(16);
        snprintf(vptr->y, 16, "%d", vptr->x);
        if ((fptr = find_u_keyval(a, NULL, &v))) {
            printf("Found %d; %s\n", fptr->x, fptr->y);
            del_u_keyval(&a, fptr);
        } else
            printf("Not found\n");
    } else
        printf("Not added\n");
    printf("\n");
    foreach_u_keyval(a, print_kp, NULL, 0);
    free_u_keyval(&a);
}

int main () {
#ifndef __GNUC__
	libex_str_init();
#endif
/*    test_array();
    test_array2();
    test_sorted_array();
    test_sorted_array2();*/
    
/*    test_array3(0);
    test_array3(8);
    test_array4(0);
    test_array4(8);*/
    
    test_array31();
    return 0;
}
