#include "../include/libex/wsnet.h"
#include "linenoise.h"

net_srv_t *srv;

static void sigexit (int sig) {
    if (SIGTERM == sig || SIGINT == sig)
        ws_srv_done(srv);
}

static void setsig () {
    struct sigaction sa;
    sa.sa_handler = sigexit;
    sa.sa_flags = 0;
    sigemptyset(&sa.sa_mask);
    sigaction(SIGTERM, &sa, NULL);
    sigaction(SIGINT, &sa, NULL);
}

static void *on_start (void *dummy) {
    ws_srv_start(srv);
    printf("end thread\n");
    return NULL;
}

static void term (uint64_t fd) {
    char *s;
    do {
        s = linenoise("> ", NULL);
        if ('\0' != *s && 0 != strcmp("quit", s)) {
            int len = strlen(s);
            ws_ev_send(srv, fd, 0, s, len, WS_FIN, WS_TEXT);
        }
    } while (0 != strcmp("quit", s));
    ws_ev_disconnect(srv, fd);
    ws_srv_done(srv);
}

static int on_handle (ev_buf_t *ev) {
    char *s = strndup((const char*)ev->data.ws.ptr, ev->data.ws.len);
    printf("%s\n", s);
    free(s);
    return NET_OK;
}

int main (int argc, const char *argv[]) {
    int is_client = 1;
    pthread_t th;
    if (2 != argc) return 1;
    if (0 == strcmp("server", argv[1]))
        is_client = 0;
    else
    if (0 != strcmp("client", argv[1]))
        return 1;
    printf("[%d]\n", getpid());
    if (is_client) {
        ev_fd_t fd;
        srv = ws_srv_init(NULL);
        srv->on_handle = on_handle;
        srv->tm_connect = 8;
        printf("Ok\n");
        pthread_create(&th, NULL, on_start, NULL);
        sleep(1);
        if (0 == (fd = net_ev_connect(srv, CONST_STR_LEN("ws://127.0.0.1:7878/")))) {
            printf("(%d) %s\n", errno, strerror(errno));
            ws_srv_done(srv);
        } else
            term(fd);
    } else {
        setsig();
        srv = ws_srv_init("7878");
        srv->on_handle = on_handle;
        printf("Ok\n");
        ws_srv_start(srv);
    }
    return 0;
}
