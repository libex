#include "../include/libex/rpc.h"

int on_params1 (strbuf_t *buf, void *dummy) {
    rpc_add_str(buf, CONST_STR_LEN("good luck"));
    rpc_add_int(buf, 5);
    rpc_add_double(buf, 7);
    rpc_add_true(buf);
    rpc_add_false(buf);
    rpc_add_null(buf);
    return 0;
}

int on_response1 (strbuf_t *buf, void *dummy) {
    msg_addi64(buf, 8);
    msg_addstr(buf, CONST_STR_LEN("Good luck"));
    return 0;
}

int main () {
    int64_t id;
    strbuf_t buf;
    rpc_t rpc;
    id = 1;
    memset(&buf, 0, sizeof(strbuf_t));
    
    rpc_create_request(&buf, 1, on_params1, RPC_ID_INT(id), NULL);
    rpc_parse(&buf, 0, &rpc);
    rpc_print(&rpc);
    rpc_clear(&rpc);
    printf("\n");
    
    rpc_create_response(&buf, RPC_OK, on_response1, RPC_ID_INT(id), NULL);
    rpc_parse(&buf, 0, &rpc);
    rpc_print(&rpc);
    rpc_clear(&rpc);
    free(buf.ptr);
    memset(&buf, 0, sizeof buf);
    
    return 0;
}
