#include "../include/libex/wsnet.h"
#include "linenoise.h"

net_srv_t *srv;
rpc_list_t *methods;

static void sigexit (int sig) {
    if (SIGTERM == sig || SIGINT == sig) {
        ws_srv_done(srv);
        free(methods);
    }
}

static void setsig () {
    struct sigaction sa;
    sa.sa_handler = sigexit;
    sa.sa_flags = 0;
    sigemptyset(&sa.sa_mask);
    sigaction(SIGTERM, &sa, NULL);
    sigaction(SIGINT, &sa, NULL);
}

static int on_handle (ev_buf_t *ev) {
    int rc = NET_ERROR;
    rpc_t *rpc = &ev->data.rpc.rpc;
    if ((RPC_REQUEST & rpc->flags)) {
        if ((rc = rpc_execute_parsed(&ev->buf, methods, rpc, ev)) < 0)
            ws_ev_send(srv, ev->fd, 0, ev->buf.ptr, ev->buf.len, WS_FIN, WS_BIN);
    } else {
        if ((rpc->flags & RPC_IDSTR)) {
            if (0 == cmpstr(rpc->id.id_str.ptr, rpc->id.id_str.len, CONST_STR_LEN("2"))) {
                strptr_t s;
                msg_getstrcpy(&ev->buf, &s);
                printf("code: %d; %s\r\n", rpc->data.response.errcode, s.ptr);
                free(s.ptr);
            }
        }
        rc = NET_OK;
    }
    return 0 != rc ? NET_OK : NET_ERROR;
}

static void *on_start (void *param) {
    ws_srv_start(srv);
    return NULL;
}

static int cb_login (strbuf_t *buf, void *userdata) {
    strptr_t *s = userdata;
    msg_addstr(buf, s->ptr, s->len);
    return 0;
}

static int login (strbuf_t *buf, rpc_t *rpc, void *userdata) {
    strptr_t *s = &rpc->data.request.params[0].data.s;
    ev_buf_t *ev = userdata;
    ws_rpc_response_send(srv, ev->fd, buf, RPC_ID_INT(rpc->id.id_int), cb_login, s);
    return 0;
}

static int cb_getprops (strbuf_t *buf, void *userdata) {
    msg_addstr(buf, (char*)userdata, strlen((char*)userdata));
    return 0;
}

static int getprops (strbuf_t *buf, rpc_t *rpc, void *userdata) {
    ev_buf_t *ev = userdata;
    ws_rpc_response_send(srv, ev->fd, buf, (intptr_t)rpc->id.id_str.ptr, rpc->id.id_str.len, cb_getprops, "binary protocol");
    return 0;
}

static int notify (strbuf_t *buf, rpc_t *rpc, void *dummy) {
    strptr_t *str = &rpc->data.request.params[0].data.s;
    char *s = strndup(str->ptr, str->len);
    printf("NOTIFY: %s\n", s);
    free(s);
    return 0;
}

static int on_login (strbuf_t *buf, void *dummy) {
    char *s;
    s = linenoise("login: ", NULL);
    rpc_add_str(buf, s, strlen(s));
    s = linenoise("pass: ", NULL);
    rpc_add_str(buf, s, strlen(s));
    return 0;
}

static int on_notify (strbuf_t *buf, void *dummy) {
    char *s = linenoise("notify: ", NULL);
    int l = strlen(s);
    rpc_add_str(buf, s, l);
    return 0;
}

static void term (ev_fd_t fd) {
    char *method_str;
    strbuf_t buf;
    rpc_t rpc;
    memset(&buf, 0, sizeof buf);
    memset(&rpc, 0, sizeof rpc);
    do {
        method_str = linenoise("> ", NULL);
        if (0 == strcmp(method_str, "login")) {
            strptr_t s;
            ws_rpc_request_send(srv, fd, 1, NF_SYNC, &buf, on_login, &rpc, NULL);
            msg_getstrcpy(&buf, &s);
            printf("%s\n", s.ptr);
            free(s.ptr);
        } else
        if (0 == strcmp(method_str, "getProps")) {
            ws_rpc_request_send(srv, fd, 2, 0, &buf, NULL, &rpc, NULL);
        } else
        if (0 == strcmp(method_str, "notify")) {
            ws_rpc_request_send(srv, fd, 3, NF_NOTIFY, &buf, on_notify, NULL, NULL);
            printf("\r\n");
        } else
        if (0 == strcmp(method_str, "none"))
            ws_rpc_request_send(srv, fd, 99, NF_SYNC, &buf, NULL, &rpc, NULL);
        else
            continue;
        if (buf.ptr) {
            free(buf.ptr);
            buf.ptr = NULL;
        }
        rpc_clear(&rpc);
    } while (0 != strcmp(method_str, "quit"));
    rpc_clear(&rpc);
    if (buf.ptr)
        free(buf.ptr);
    ws_ev_disconnect(srv, fd);
    ws_srv_done(srv);
}

rpc_type_t login_types [] = { RPC_STRING, RPC_STRING };
rpc_type_t notify_types [] = { JSON_STRING };

int main (int argc, const char *argv[]) {
    int is_client = 1;
    if (2 != argc) return 1;
    if (0 == strcmp(argv[1], "server"))
        is_client = 0;
    else
    if (0 != strcmp(argv[1], "client"))
        return 1;
    printf("[%d]\n", getpid());
    methods = rpc_init_methods();
    rpc_add_method(&methods, 1, login, RPC_TYPES_LEN(login_types));
    rpc_add_method(&methods, 2, getprops, NULL, 0);
    rpc_add_method(&methods, 3, notify, RPC_TYPES_LEN(notify_types));
    if (is_client) {
        pthread_t th;
        ev_fd_t fd;
        srv = ws_rpc_srv_init(NULL);
        srv->on_handle = on_handle;
        srv->tm_connect = 8;
        pthread_create(&th, NULL, on_start, NULL);
        sleep(1);
        if (0 == (fd = net_ev_connect(srv, CONST_STR_LEN("ws://127.0.0.1:7878/api/v2")))) {
            printf("(%d) %s\n", errno, strerror(errno));
            ws_srv_done(srv);
            free(methods);
        } else
            term(fd);
    } else {
        setsig();
        srv = ws_rpc_srv_init("7878");
        srv->on_handle = on_handle;
        srv->on_check_url = check_api_ver;
        srv->tm_ping = 60;
        printf("Ok\n");
        ws_srv_start(srv);
    }
    return 0;
}