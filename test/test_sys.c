#include "../include/libex/sys.h"
#include "../include/libex/str.h"
#include <unistd.h>
#include <pthread.h>

#define MAX_THREADS 4
#define MAX_COUNT 1000
#define USEC 2500
int thread_cnt = MAX_THREADS;
int n = 0, m = 0;
int result [MAX_COUNT];
int is_ok = 1;
pthread_t threads [MAX_THREADS];
pthread_barrier_t barrier;
locker_t locker = 0;

void test1 () {
    int r [32], x = 0;
    for (int i = 0; i < 32; ++i) {
        __atomic_inc_int(&x);
        r[i] = x;
    }
    for (int i = 0; i < 32; ++i)
        printf("%d\n", r[i]);
}

void *_test2 (void *arg) {
    pthread_barrier_wait(&barrier);
    while (1) {
        __lock(locker);
        if (n >= MAX_COUNT) {
            __unlock(locker);
            break;
        }
        ++n;
        usleep(USEC);
        ++m;
        result[n] = m;
        __unlock(locker);
    }
    return NULL;
}
void test2 () {
    printf("atomic\n");
    pthread_barrier_init(&barrier, NULL, thread_cnt+1);
    for (int i = 0; i < thread_cnt; ++i)
        pthread_create(&threads[i], NULL, _test2, NULL);
    pthread_barrier_wait(&barrier);
    for (int i = 0; i < thread_cnt; ++i)
        pthread_join(threads[i], NULL);
    pthread_barrier_destroy(&barrier);
    for (int i = 0; i < MAX_COUNT-1; ++i) {
        if (i != result[i]) {
            printf("%d: %d\n", i, result[i]);
            is_ok = 0;
        }
    }
    if (is_ok)
        printf("Ok\n");
}

int main () {
    test1();
//    test2();
    return 0;
}

