#include <stdlib.h>
#include "../include/libex/http.h"

#define HTTP1 "POST /news.html HTTP/1.0\r\n" \
    "Host: www.site.ru\r\n" \
    "Referer: http://www.site.ru/index.html\r\n" \
    "Cookie: income=1\r\n" \
    "Content-Type: application/x-www-form-urlencoded\r\n" \
    "Content-Length: 35\r\n" \
    "\r\n" \
    "login=Petya&password=qq"

void print_url (http_url_t *x) {
    char *p;
    if (x->prot.ptr) {
        p = strndup(x->prot.ptr, x->prot.len);
        printf("%s://", p);
        free(p);
    }
    if (x->domain.ptr) {
        p = strndup(x->domain.ptr, x->domain.len);
        printf("%s", p);
        free(p);
    }
    if (x->port.ptr) {
        p = strndup(x->port.ptr, x->port.len);
        printf(":%s", p);
        free(p);
    }
    if (x->path.ptr) {
        char *p = strndup(x->path.ptr, x->path.len);
        printf("%s", p);
    }
    printf("\n");
}

void test_url () {
    http_url_t x;
    http_parse_url(CONST_STR_LEN("ws://domain.com"), &x);
    print_url(&x);
    http_free_url(&x);
    http_parse_url(CONST_STR_LEN("ws://domain.com/api"), &x);
    print_url(&x);
    http_free_url(&x);
    http_parse_url(CONST_STR_LEN("ws://domain.com:4545"), &x);
    print_url(&x);
    http_free_url(&x);
    http_parse_url(CONST_STR_LEN("ws://domain.com:4545/"), &x);
    print_url(&x);
    http_free_url(&x);
    http_parse_url(CONST_STR_LEN("ws://domain.com:4545/api"), &x);
    print_url(&x);
    http_free_url(&x);
}

int main () {
//    test_http(CONST_STR_LEN(HTTP1));
    test_url();
    return 0;
}
