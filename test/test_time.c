#include <stdio.h>
#include "../include/libex/time.h"

static void tmprintf (const char *s, struct tm *tm) {
    char buf [48];
    snprintf(buf, sizeof buf, "%s %d/%02d/%02d %02d:%02d:%02d", s,
             tm->tm_year, tm->tm_mon, tm->tm_mday, tm->tm_hour, tm->tm_min, tm->tm_sec);
    printf("%s\n", buf);
}

static void test (struct tm *dt, struct tm *dtd) {
    struct tm tm;
    tmdiff_t dif;
    printf("-----------------------\n");
    tmprintf("> time:", dt);
    tmprintf("> diff:", dtd);
    tm_t ts = tm_enc(dt);
    if (-1 == ts && ERANGE == errno)
        exit(1);
    tm_dec(ts, &tm);
    tmprintf("< decode time:", &tm);
    
    dif = tm_encdiff(dtd);
    if (-1 == (ts = tm_add(ts, &dif)) && ERANGE == errno)
        exit(1);
    tm_dec(ts, &tm);
    tmprintf("< time+diff:", &tm);
}

static void test_now () {
    struct tm tm;
    tm_t ts = tm_now();
    tm_dec(ts, &tm);
    tmprintf("now:", &tm);
}

int main () {
    test(DATETIME_ARG(20210208,143000), TM_ARG(0,1,0,0,0,0));
    test(DATETIME_ARG(19800228,122000), TM_ARG(0,0,2,0,0,0));
    test(DATETIME_ARG(19800228,235000), TM_ARG(0,0,0,0,20,0));
    test(DATETIME_ARG(19800228,235500), TM_ARG(0,0,0,0,0,600));
    test(DATETIME_ARG(19800228,235000), TM_ARG(0,0,0,30,20,0));
    test(DATETIME_ARG(19210606,235123), TM_ARG(0,0,0,30,20,21));
    printf("=================================\n");
    test(DATETIME_ARG(20210723,123421), TM_ARG(1,0,0,0,0,0));
    test(DATETIME_ARG(20210723,123421), TM_ARG(0,1,0,0,0,0));
    test(DATETIME_ARG(20210723,123421), TM_ARG(0,0,1,0,0,0));
    test(DATETIME_ARG(20210723,123421), TM_ARG(0,0,0,1,0,0));
    test(DATETIME_ARG(20210723,123421), TM_ARG(0,0,0,0,1,0));
    test(DATETIME_ARG(20210723,123421), TM_ARG(0,0,0,0,0,1));
    printf("=================================\n");
    test(DATETIME_ARG(20210723,123421), TM_ARG(1,0,0,0,0,0));
    test(DATETIME_ARG(20210723,123421), TM_ARG(0,12,0,0,0,0));
    test(DATETIME_ARG(20210723,123421), TM_ARG(0,0,365,0,0,0));
    printf("=================================\n");
    test_now();
    return 0;
}
