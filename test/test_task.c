#include <stdio.h>
#include "../include/libex/task.h"

int i, t;
int timed_wait = 0;

static int on_create_slot (pool_slot_t *slot, void *x) {
    printf("create slot\n");
    return 0;
}

static void on_destroy_slot (pool_slot_t *x) {
    printf("destroy slot\n");
}

static int on_msg (void *d, pool_msg_t *msg) {
    printf("%s\n", (char*)msg->data);
    free(msg->data);
    sleep(t);
    return MSG_DONE;
}

static int on_info (void *d, pool_msg_t *x) {
    printf("%d: %lu\n", i++, time(0));
    return MSG_DONE;
}

static void test_1 () {
    printf(" \n-= test 1 =-\n");
    pool_t *pool = pool_create();
#ifdef __GNUC__
    pool_setopt(pool, POOL_MAXSLOTS, 8);
    pool_setopt(pool, POOL_CREATESLOT, on_create_slot);
    pool_setopt(pool, POOL_DESTROYSLOT, on_destroy_slot);
#else
    pool_setopt_int(pool, POOL_MAXSLOTS, 24);
    pool_setopt_create(pool, POOL_CREATESLOT, on_create_slot);
    pool_setopt_destroy(pool, POOL_DESTROYSLOT, on_destroy_slot);
#endif
    pool_start(pool);
    for (int i = 0; i < 16; ++i) {
        char *str = malloc(16);
        usleep(1000);
        snprintf(str, 16, "i:%d", i);
        pool_call(pool, on_msg, str, NULL, 0);
    }
    sleep(2);
    pool_destroy(pool, timed_wait);
}

static void test_2 () {
    printf(" \n-= test 2 =-\n");
    pool_t *pool = pool_create();
#ifdef __GNUC__
    pool_setopt(pool, POOL_MSG, on_info);
    pool_setopt(pool, POOL_TIMEOUT, 1);
    pool_setopt(pool, POOL_MAXSLOTS, 1);
#else
    pool_setopt_msg(pool, POOL_MSG, on_info);
    pool_setopt_int(pool, POOL_TIMEOUT, 1);
    pool_setopt_int(pool, POOL_MAXSLOTS, 1);
#endif
    i = 0;
    pool_start(pool);
    sleep(7);
    pool_destroy(pool, timed_wait);
}

static int on_msg1 (void *d, pool_msg_t *msg) {
    int n = (intptr_t)msg->data;
    printf("%d\n", n);
    msg->data = (void*)(intptr_t)msg->data - 1;
    if ((intptr_t)msg->data == 0)
        return MSG_DONE;
    return MSG_WAIT;
}

static void test_3 () {
    printf(" \n-= test 3 =-\n");
    pool_t *pool = pool_create();
#ifdef __GNUC__
    pool_setopt(pool, POOL_MAXSLOTS, POOL_DEFAULT_SLOTS);
#else
    pool_setopt_int(pool, POOL_MAXSLOTS, POOL_DEFAULT_SLOTS);
#endif
    pool_start(pool);
    pool_call(pool, on_msg1, (void*)(intptr_t)3, NULL, 0);
    sleep(2);
    pool_destroy(pool, timed_wait);
}

int main (int argc, const char *argv[]) {
    if (argc > 1) {
        char *tail;
        timed_wait = strtol(argv[1], &tail, 0);
        if ('\0' != *tail || ERANGE == errno)
            timed_wait = 0;
    }
    test_1();
    test_2();
    test_3();
    return 0;
}
