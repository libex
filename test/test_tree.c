#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>
#include "../include/libex/str.h"
#include "../include/libex/tree.h"

typedef struct {
    int x;
    char s [16];
} item_t;

DECLARE_LIST(test_list,item_t)
DECLARE_LIST_FUN(test_list,item_t)

typedef struct {
    int key;
    test_list_t list;
} bucket_t;

int on_cmp_bucket (const bucket_t *x, bucket_t *y) {
    if (x->key > y->key) return 1;
    if (x->key < y->key) return -1;
    return 0;
}

void on_free_bucket (bucket_t *x) {
    clear_test_list(&x->list);
}

DECLARE_RBTREE(test_tree,bucket_t)
DECLARE_RBTREE_FUN(test_tree,bucket_t)

int on_calc_item_len (bucket_t *bucket, void *dummy) {
    printf("key: %d, %u items\n", bucket->key, bucket->list.len);
    return 0;
}

int on_enum_items (item_t *item, void *dummy) {
    printf("x: %d; s: %s\n", item->x, item->s);
    return 0;
}

void test_01 (void) {
    test_tree_t *tree = create_test_tree();
    tree->on_compare = on_cmp_bucket;
    tree->on_free = on_free_bucket;
    int input [] = {1,6,5,3,4,8,9,6,4,3,4,6,7,9,3,5,1};
    bucket_t *bucket, input_bucket;
    item_t *item, input_item;
    printf("[%d] ", sizeof(input)/sizeof(int));
    for (int i = 0; i < sizeof(input)/sizeof(int); ++i)
        printf("%d ", input[i]);
    printf("\n");
    for (int i = 0; i < sizeof(input)/sizeof(int); ++i) {
        input_bucket.key = input[i];
        memset(&input_bucket.list, 0, sizeof(test_list_t));
        bucket = add_test_tree(tree, &input_bucket);
        input_item.x = input[i];
        snprintf(input_item.s, sizeof(input_item.s), "%d", i+1);
        add_tail_test_list(&bucket->list, &input_item, 0);
    }
    printf("Count:\n");
    foreach_test_tree(tree, on_calc_item_len, NULL, 0);
    input_bucket.key = 5;
    printf("Find %d\n", input_bucket.key);
    if ((bucket = find_test_tree(tree, &input_bucket)))
        foreach_test_list(&bucket->list, on_enum_items, NULL, 0);
    free_test_tree(tree);
}

int main () {
#ifndef __GNUC__
    libex_str_init();
#endif
    test_01();
    return 0;
}
