#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include "../include/libex/file.h"
#include "../include/libex/json.h"

json_t *load_json (const char *fname, char **buf) {
    json_t *json = NULL;
    int fd = open(fname, O_RDONLY);
    if (-1 != fd) {
        struct stat st;
        if (-1 != fstat(fd, &st) && st.st_size > 0) {
            *buf = malloc(st.st_size+1);
            if (st.st_size == read(fd, *buf, st.st_size))
                json = json_parse_len(*buf, st.st_size);
        }
        close(fd);
    }
    return json;
}

void test_json1 (void) {
    char *buf = NULL;
    json_t *json = load_json("./json_2.txt", &buf);
    if (json) {
        json_item_t *j = json_find(json->data.o, CONST_STR_LEN("method"), JSON_STRING);
        if (j) {
            char *s = alloca(j->data.s.len+1);
            strncpy(s, j->data.s.ptr, j->data.s.len);
            s[j->data.s.len] = '\0';
            printf("method: %s\n", s);
        }
        json_free(json);
    }
    if (buf) free(buf);
}

void add_assets (strbuf_t *buf) {
    json_open_array(buf, CONST_STR_LEN("assets"));
    json_begin_object(buf);
    json_add_str(buf, CONST_STR_LEN("assetref"), CONST_STR_LEN("60-265-38372"));
    json_add_str(buf, CONST_STR_LEN("name"), CONST_STR_LEN("xyz"));
    json_add_double(buf, CONST_STR_LEN("qty"), -16);
    json_end_object(buf);
    json_close_array(buf);
}

void add_myaddresses (strbuf_t *buf) {
    json_open_array(buf, CONST_STR_LEN("myaddresses"));
    json_add_item_str(buf, CONST_STR_LEN("1N5wY46K42bTfMrZNJ6jyyWU9tDsxafjMn5zqR"));
    json_close_array(buf);
}

void add_balance (strbuf_t *buf) {
    json_open_object(buf, CONST_STR_LEN("balance"));
    json_add_double(buf, CONST_STR_LEN("amount"), 0);
    add_assets(buf);
    json_close_object(buf);
}

void add_addresses (strbuf_t *buf) {
    json_open_array(buf, CONST_STR_LEN("addresses"));
    json_add_item_str(buf, CONST_STR_LEN("1NSxMbY2GXSFLN6n8bJN7nSWnPDkU2qnTsPiCm"));
    json_end_array(buf);
}

void add_result (strbuf_t *buf) {
    json_open_object(buf, CONST_STR_LEN("result"));
    add_addresses (buf);
    add_balance(buf);
    json_add_str(buf, CONST_STR_LEN("comment"), CONST_STR_LEN("test comment"));
    json_add_int(buf, CONST_STR_LEN("confirmations"), 0);
    json_empty_array(buf, CONST_STR_LEN("data"));
    json_empty_array(buf, CONST_STR_LEN("items"));
    add_myaddresses(buf);
    json_empty_array(buf, CONST_STR_LEN("permissions"));
    json_add_int(buf, CONST_STR_LEN("time"), 1500386499);
    json_add_int(buf, CONST_STR_LEN("timereceived"), 1500386499);
    json_add_str(buf, CONST_STR_LEN("txid"), CONST_STR_LEN("86c0132b4bc930936fd2b1364a66373e98463cf98c462d54860b7b2320e02385"));
    json_add_true(buf, CONST_STR_LEN("valid"));
    json_close_object(buf);
}

void test_json2 (void) {
    strbuf_t buf;
    strbufalloc(&buf, 64, 64);
    json_begin_object(&buf);
    json_add_null(&buf, CONST_STR_LEN("error"));
    json_add_str(&buf, CONST_STR_LEN("id"), CONST_STR_LEN("curltest"));
    add_result(&buf);
    json_end_object(&buf);
    buf.ptr[buf.len] = '\0';
    printf(buf.ptr);
    free(buf.ptr);
}

int main (int argc, const char *argv[]) {
    test_json1();
//    test_json2();
    return 0;
}
