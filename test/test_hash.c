#include "../include/libex/hash.h"

typedef struct {
    char k [16];
    char v [16];
} item_t;
DECLARE_HASH(test_hash,item_t)
DECLARE_HASH_FUN(test_hash,item_t)
int on_item_compare (const item_t *x, item_t *y) {
    return strcmp(x->k, y->k);
}
hsize_t on_item_hash (test_hash_t *hash, const item_t *item) {
    return hash_mmur((const char*)item->k, sizeof(item->k)) % hash->hsize;
}

int main () {
#ifndef __GNUC__
    libex_str_init();
    libex_hash_init();
#endif
    test_hash_t *hash = create_test_hash(256, CF_UNIQUE);
    set_test_hash_compare(hash, on_item_compare);
    set_test_hash_calc(hash, on_item_hash);
    item_t x, *z, n;
    for (int i = 0; i < 512; ++i) {
        memset(x.k, 0, sizeof(x.k));
        snprintf(x.k, sizeof(x.k), "%d", i);
        memcpy(x.v, x.k, sizeof(x.v));
        z = add_test_hash(hash, &x, 0);
    }
    printf("count: " ULONG_FMT "\n", hash->len);

    memset(&x, 0, sizeof(x));
    snprintf(x.k, sizeof(x.k), "%d", 100);
    memcpy(x.v, x.k, sizeof(x.v));
    z = add_test_hash(hash, &x, 0);
    printf("errno: %d, %s; count " ULONG_FMT "\n", errno, strerror(errno), hash->len);

    memset(&x, 0, sizeof(x));
    snprintf(x.k, sizeof(x.k), "%d", 200);
    memcpy(x.v, x.k, sizeof(x.v));
    if ((z = get_test_hash(hash, &x)))
        printf("found\n");
    else
        printf("not found\n");

    memset(&x, 0, sizeof(x));
    snprintf(x.k, sizeof(x.k), "%d", 1000);
    memcpy(x.v, x.k, sizeof(x.k));
    z = get_test_hash(hash, &x);
    if ((z = get_test_hash(hash, &x)))
        printf("found\n");
    else
        printf("not found\n");

    free_test_hash(hash);
    return 0;
}
