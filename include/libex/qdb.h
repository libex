/*Copyright (c) Brian B.

Permission to use, copy, modify, and/or distribute this software for any purpose
with or without fee is hereby granted.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*/
#ifndef __QDB_H__
#define __QDB_H__

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <stdint.h>
#include <errno.h>
#include <sys/file.h>
#include "str.h"

#define QDB_PEEK 1

typedef struct {
    uint64_t head;
    uint64_t tail;
    uint32_t size;
} __attribute__ ((__packed__)) qdb_stack_t;

int qdb_stat (const char *fname, qdb_stack_t *hdr);

int qdb_open (const char *fname);
int qdb_put (int fd, const void *data, size_t size);
int qdb_get (int fd, strbuf_t *buf);
int qdb_copen (const char *fname, size_t default_size);
int qdb_cput (int fd, const void *data);
int qdb_cget (int fd, void *data, int flags);

#endif // __QDB_H__
