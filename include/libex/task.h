/*Copyright (c) Brian B.

Permission to use, copy, modify, and/or distribute this software for any purpose
with or without fee is hereby granted.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*/
#ifndef __LIBEX_TASK_H__
#define __LIBEX_TASK_H__

#include <stdlib.h>
#include <unistd.h>
#include <stdint.h>
#include <signal.h>
#include <pthread.h>
#include <sys/types.h>
#include <sys/time.h>
#include <sys/wait.h>
#include <signal.h>
#include "str.h"
#include "list.h"

typedef enum { EP_FORK, EP_EXITED, EP_STOPPED, EP_SIGNALED } exit_status_t;
typedef struct {
    exit_status_t status;
    int code;
} exit_process_t;
int runf (exit_process_t *ep, const char *cmd, ...);
int run  (exit_process_t *ep, const char *cmd);

#define POOL_DEFAULT_SLOTS -1

#define MSG_DONE 0
#define MSG_WAIT 1

typedef struct pool pool_t;
typedef struct pool_slot pool_slot_t;
typedef struct pool_msg pool_msg_t;
typedef int (*pool_msg_h) (void *slot_data, pool_msg_t *data);
typedef int (*pool_create_h) (pool_slot_t*, void*);
typedef void (*pool_destroy_h) (pool_slot_t*);

typedef enum {
    POOL_MSG,
    POOL_FREEMSG,
    POOL_MAXSLOTS,
    POOL_TIMEOUT,
    POOL_LIVINGTIME,
    POOL_CREATESLOT,
    POOL_DESTROYSLOT
} pool_opt_t;

#define TASK_WAIT 0x00000002

struct pool_msg {
    void *data;
    pool_msg_h on_msg;
    unsigned int flags;
    pthread_mutex_t *mutex;
    pthread_cond_t *cond;
};
DECLARE_LIST(pool_msg_list,pool_msg_t)

struct pool_slot {
    int is_alive;
    pool_t *pool;
    pthread_t th;
    void *data;
    void *init_data;
};
DECLARE_LIST(pool_slot_list,pool_slot_t)

struct pool {
    int is_alive;
    void *linked_obj;
    pthread_mutex_t locker;
    pthread_condattr_t cond_attr;
    pthread_cond_t cond;
    pool_msg_list_t *queue;
    long max_slots;
    pool_slot_list_t *slots;
    pool_msg_h on_msg;
    pool_destroy_h on_freemsg;
    pool_create_h on_create_slot;
    pool_destroy_h on_destroy_slot;
    pthread_barrier_t barrier;
    long timeout;
    long livingtime;
    int is_started;
};

int pool_setopt_int (pool_t *pool, pool_opt_t opt, long arg);
int pool_setopt_msg (pool_t *pool, pool_opt_t opt, pool_msg_h arg);
int pool_setopt_create (pool_t *pool, pool_opt_t opt, pool_create_h arg);
int pool_setopt_destroy (pool_t *pool, pool_opt_t opt, pool_destroy_h arg);

#define pool_setopt(pool,opt,arg) \
    _Generic((arg), \
    pool_msg_h: pool_setopt_msg, \
    pool_create_h: pool_setopt_create, \
    pool_destroy_h: pool_setopt_destroy, \
    default: pool_setopt_int \
)(pool,opt,arg)

pool_msg_t pool_createmsg (pool_msg_h on_msg,
                       void *data,
                       pthread_mutex_t *mutex, pthread_cond_t *cond,
                       unsigned int flags);

pool_t *pool_create (void);

void pool_start (pool_t *pool);
int pool_callmsg (pool_t *pool, pool_msg_t *msg, void *init_data);
int pool_call (pool_t *pool, pool_msg_h on_msg, void *data, void *init_data, unsigned int flags);
void pool_destroy (pool_t *pool, int toueout);

#endif // __LIBEX_TASK_H__
