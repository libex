/*Copyright (c) Brian B.

Permission to use, copy, modify, and/or distribute this software for any purpose
with or without fee is hereby granted.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE
OF THIS SOFTWARE.
*/
#ifndef __LIBEX_ENDIANS_H__
#define __LIBEX_ENDIANS_H__

#if defined(_AIX)
#include <sys/types.h>
#include <arpa/nameser_compat.h>
#endif

#if BYTE_ORDER == LITTLE_ENDIAN
# include <endian.h>
#else
# define htobe16(x) (x)
# define htobe32(x) (x)
# define htobe64(x) (x)
# define be16toh(x) (x)
# define be32toh(x) (x)
# define be64toh(x) (x)
#endif

#endif
