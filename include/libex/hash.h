/*Copyright (c) Brian B.

Permission to use, copy, modify, and/or distribute this software for any purpose
with or without fee is hereby granted.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF
THIS SOFTWARE.
*/
#ifndef __LIBEX_HASH_H__
#define __LIBEX_HASH_H__

#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <errno.h>
#include <limits.h>
#include <string.h>
#include <pthread.h>
#include "list.h"
#include "str.h"
#include "sys.h"

#ifndef __GNUC__
void libex_hash_init (void);
#endif

#if __WORDSIZE == 64
#define MAX_HASH_KEY LONG_MAX
typedef uint64_t hsize_t;
#define HASH_FMT "%lu"
#else
#define MAX_HASH_KEY UINT_MAX
typedef uint32_t hsize_t;
#define HASH_FMT "%u"
#endif

hsize_t hash_nstr (const void *s, size_t len);
hsize_t hash_mmur (const void *s, size_t len);

#define DECLARE_HASH(NAME,TYPE) \
    DECLARE_LIST(NAME##_hash_list,TYPE) \
    typedef struct NAME NAME##_t; \
    typedef struct { \
        pthread_mutex_t locker; \
        NAME##_hash_list_t list; \
    } NAME##_slot_t; \
    struct NAME { \
        hsize_t hsize; \
        unsigned long long len; \
        unsigned int flags; \
        void(*on_free)(TYPE*); \
        hsize_t(*on_hash)(NAME##_t *hash, const TYPE *data); \
        int(*on_compare)(const TYPE*, TYPE*); \
        NAME##_slot_t ptr [0]; \
    }; \
    extern NAME##_t *create_##NAME (hsize_t hsize, unsigned int flags); \
    extern void set_##NAME##_calc (NAME##_t *hash, hsize_t(*fn)(NAME##_t*,const TYPE*)); \
    extern void set_##NAME##_free (NAME##_t *hash, void(*fn)(TYPE*)); \
    void set_##NAME##_compare (NAME##_t *hasn, int(*fn)(const TYPE*,TYPE*)); \
    extern void free_##NAME (NAME##_t *hash); \
    extern TYPE *add_##NAME (NAME##_t *hash, TYPE *data, size_t); \
    extern void del_##NAME (NAME##_t *hash, TYPE *data); \
    extern TYPE *get_##NAME (NAME##_t *hash, TYPE *data); \
    extern void foreach_##NAME (NAME##_t *hash, int(*fn)(TYPE*,void*), void *userdata, int flags); \
    extern void unlock_##NAME (NAME##_t *hash, TYPE *data);

#define DECLARE_HASH_FUN(NAME,TYPE) \
    DECLARE_LIST_FUN(NAME##_hash_list,TYPE) \
    NAME##_t *create_##NAME (hsize_t hsize, unsigned int flags) { \
        NAME##_t *hash = calloc(1, sizeof(NAME##_t) + sizeof(NAME##_slot_t) * hsize); \
        hash->hsize = hsize; \
        hash->flags = flags; \
        return hash; \
    } \
    void set_##NAME##_calc (NAME##_t *hash, hsize_t(*fn)(NAME##_t*,const TYPE*)) { \
        hash->on_hash = fn; \
    } \
    void set_##NAME##_free (NAME##_t *hash, void(*fn)(TYPE*)) { \
        hash->on_free = fn; \
        for (hsize_t i = 0; i < hash->hsize; ++i) \
            hash->ptr[i].list.on_free = fn; \
    } \
    void set_##NAME##_compare (NAME##_t *hash, int(*fn)(const TYPE*,TYPE*)) { \
        hash->on_compare = fn; \
    } \
    void free_##NAME (NAME##_t *hash) { \
        if ((hash->flags & CF_LOCK)) { \
            for (size_t i = 0; i < hash->hsize; ++i) { \
                pthread_mutex_lock(&hash->ptr[i].locker); \
                clear_##NAME##_hash_list(&hash->ptr[i].list); \
                pthread_mutex_unlock(&hash->ptr[i].locker); \
            } \
        } else \
        for (size_t i = 0; i < hash->hsize; ++i) \
            clear_##NAME##_hash_list(&hash->ptr[i].list); \
        free(hash); \
    } \
    TYPE *add_##NAME (NAME##_t *hash, TYPE *data, size_t size) { \
        TYPE *obj = NULL; \
        hsize_t id = hash->on_hash(hash, data); \
        NAME##_slot_t *slot = &hash->ptr[id]; \
        NAME##_hash_list_t *list = &slot->list; \
        errno = 0; \
        if ((hash->flags & CF_LOCK)) \
            pthread_mutex_lock(&slot->locker); \
        if ((hash->flags & CF_UNIQUE) && (obj = find_##NAME##_hash_list(&slot->list, hash->on_compare, data))) { \
            errno = EEXIST; \
            return obj; \
        } \
        if (!(obj = add_tail_##NAME##_hash_list(list, data, size))) { \
            if ((hash->flags & CF_LOCK)) \
                pthread_mutex_unlock(&slot->locker); \
        } else \
            __atomic_inc_ulong(&hash->len); \
        return obj; \
    } \
    TYPE *get_##NAME (NAME##_t *hash, TYPE *data) { \
        hsize_t id = hash->on_hash(hash, data); \
        NAME##_slot_t *slot = &hash->ptr[id]; \
        TYPE *obj; \
        if ((hash->flags & CF_LOCK)) \
            pthread_mutex_lock(&slot->locker); \
        if (!(obj = find_##NAME##_hash_list(&slot->list, hash->on_compare, data)) && \
            (hash->flags & CF_LOCK)) \
            pthread_mutex_unlock(&slot->locker); \
        return obj; \
    } \
    void del_##NAME (NAME##_t *hash, TYPE *data) { \
        hsize_t id = hash->on_hash(hash, data); \
        NAME##_slot_t *slot = &hash->ptr[id]; \
        del_##NAME##_hash_list(data, CF_FREE); \
        __atomic_dec_ulong(&hash->len); \
    } \
    void foreach_##NAME (NAME##_t *hash, int(*fn)(TYPE*,void*), void *userdata, int flags) { \
        if ((hash->flags & CF_LOCK)) { \
            for (hsize_t i = 0; i < hash->hsize; ++i) { \
                pthread_mutex_lock(&hash->ptr[i].locker); \
                foreach_##NAME##_hash_list(&hash->ptr[i].list, fn, userdata, 0); \
                pthread_mutex_unlock(&hash->ptr[i].locker); \
            } \
        } else \
        for (hsize_t i = 0; i < hash->hsize; ++i) \
            foreach_##NAME##_hash_list(&hash->ptr[i].list, fn, userdata, 0); \
    } \
    void unlock_##NAME (NAME##_t *hash, TYPE *data) { \
        if (!((hash->flags & CF_LOCK)) || !data) return; \
        hsize_t id = hash->on_hash(hash, data); \
        pthread_mutex_unlock(&hash->ptr[id].locker); \
    }

#endif // __LIBEX_HASH_H__
