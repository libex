/*Copyright (c) Brian B.

Permission to use, copy, modify, and/or distribute this software for any purpose
with or without fee is hereby granted.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF
THIS SOFTWARE.
*/
#ifndef __LIBEX_NET_H__
#define __LIBEX_NET_H__

#include <stdio.h>
#include <errno.h>
#include <stdint.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/poll.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <sys/select.h>
#include "json.h"
#include "msg.h"
#include "rpc.h"
#include "sys.h"
#include "poll/net_poll.h"

#define ev_lock(srv,i) pthread_mutex_lock(&srv->fd_info[i % FDINFO_MAX].locker)
#define ev_unlock(srv,i) pthread_mutex_unlock(&srv->fd_info[i % FDINFO_MAX].locker)

typedef ssize_t (*fmt_checker_h) (const char*, size_t);

void ignore_pipe (void);
int atoport (const char *service, const char *proto);
int atoaddr (const char *address, struct in_addr *addr);

typedef ssize_t (*net_recv_h) (int, strbuf_t*);

int net_bind (const char *svc);
int net_connect (char *to_addr, char *service, int timeout);
ssize_t net_read (int fd, strbuf_t *buf);
ssize_t net_recv (int fd, strbuf_t *buf);
ssize_t net_write (int fd, const void *buf, size_t size);

typedef struct {
    ws_t ws;
    int64_t id_rpc;
    rpc_t rpc;
} ws_rpc_data_t;

typedef struct {
    ws_t ws;
    int64_t id_rpc;
    jsonrpc_t jsonrpc;
} ws_jsonrpc_data_t;

struct ev_buf {
    strbuf_t buf;
    uint64_t fd;
    uint32_t nf_flags;
    net_srv_t *srv;
    struct in_addr addr;
    union {
        ws_t ws;
        ws_rpc_data_t rpc;
        ws_jsonrpc_data_t json;
    } data;
    void *userdata;
};

#define NF_IS_INIT(x) (x & NF_INIT)
#define NF_IS_WAIT(x) (x & NF_WAIT)
#define NF_IS_DONE(x) !(x & NF_STATE)
#define NF_SET_STATE(x,v) x |= (x & ~NF_STATE) | (v)

#define NF_FD 0x00000001
#define NF_SYNC 0x00000002
#define NF_NOTIFY 0x00000004
typedef struct {
    uint64_t id;
    int fd;
} net_id_info_t;
DECLARE_ARRAY(ev_array,ev_fd_t)

ev_array_t *get_all_id_info (net_srv_t *srv);
int get_id_info (net_srv_t *srv, uint64_t *id);

static inline int get_ev_fd (net_srv_t *srv, ev_fd_t fd) {
    net_ev_t *ev = srv->get_ev(srv, fd);
    if (ev->fd == fd)
        return NET_FD(fd);
    return 0;
};

int ev_send (int fd, net_srv_t *srv);
void ev_reset (net_ev_t *ev);
int net_srv_event (void *userdata, pool_msg_t *msg);

net_ev_t *net_ev_lock (net_srv_t *srv, ev_fd_t fd);
int ev_free_ev (int fd, net_srv_t *srv);

typedef enum {
    NET_EV_HANDLE,
    NET_EV_CHECK_URL,
    NET_SRV_TM_CONNECT,
    NET_SRV_TM_WAIT,
    NET_SRV_TM_COUNT,
    NET_SRV_TM_PING,
    NET_SRV_TM_RECV,
    NET_SRV_TM_CLOSE,
    NET_SRV_POOL_SIZE,
    NET_SRV_PING,
    NET_SRV_START,
    NET_SRV_STOP,
    NET_SRV_OPT,
    NET_EV_CLOSE,
    NET_EV_CLOSE_EV,
    NET_EV_FREE_EV,
    NET_EV_OPEN,
    NET_EV_GET,
    NET_EVBUF_FREE,
} net_srv_opt_t;

int net_srv_setopt_hnd (net_srv_t *srv, net_srv_opt_t opt, net_ev_handle_h arg);
int net_srv_setopt_check_url (net_srv_t *srv, net_srv_opt_t opt, check_url_h arg);
int net_srv_setopt_int (net_srv_t *srv, net_srv_opt_t opt, int arg);
int net_srv_setopt_hnd_std (net_srv_t *srv, net_srv_opt_t opt, net_ev_h arg);
int net_srv_setopt_hnd_connect (net_srv_t *srv, net_srv_opt_t opt, net_ev_connect_h arg);
int net_srv_setopt_srv_hnd (net_srv_t *srv, net_srv_opt_t opt, net_srv_h arg);
int net_srv_setopt_evbuf_free (net_srv_t *srv, net_srv_opt_t opt, net_ev_buf_free_h arg);
int net_srv_setopt_get_ev (net_srv_t *srv, net_srv_opt_t opt, get_ev_h arg);

#define net_srv_setopt(srv,opt,arg) \
    _Generic((arg), \
    net_ev_handle_h: net_srv_setopt_hnd, \
    check_url_h: net_srv_setopt_check_url, \
    net_ev_h: net_srv_setopt_hnd_std, \
    net_ev_connect_h: net_srv_setopt_hnd_connect, \
    net_srv_h: net_srv_setopt_srv_hnd, \
    net_ev_buf_free_h: net_srv_setopt_evbuf_free, \
    get_ev_h: net_srv_setopt_get_ev, \
    default: net_srv_setopt_int \
)(srv,opt,arg)

#ifdef USE_SSL
extern __thread int ssl_errno;
cstr_t *ssl_error ();

SSL_CTX *ssl_create (int kind);
int ssl_config (SSL_CTX *ctx, const char *cert_file, const char *key_file);

int ssl_accept (SSL_CTX *ctx, int fd, SSL **ssl);
int ssl_connect (SSL_CTX *ctx, int fd, SSL **ssl);
ssize_t ssl_recv (SSL *ssl, strbuf_t *buf);
ssize_t ssl_send (SSL *ssl, const void *buf, size_t size);

//int sslws_handshake (SSL *ssl, strbuf_t *buf, strptr_t *url);
//int sslws_recv (SSL *ssl, netbuf_t *nbuf, ws_t *result);
#endif

net_srv_t *net_srv_init (const char *svc);
static inline void net_srv_start (net_srv_t *srv) { net_poll(srv); };
int net_ev_send (net_srv_t *srv, ev_fd_t fd, uint32_t flags, const char *buf, size_t len);
ev_fd_t net_ev_connect (net_srv_t *srv, char *url, size_t ulen);
int net_ev_close (net_srv_t *srv, ev_fd_t fd, uint32_t flags);
void net_srv_done (net_srv_t *srv);

#endif // __LIBEX_NET_H__
