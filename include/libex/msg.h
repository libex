/*Copyright (c) Brian B.

Permission to use, copy, modify, and/or distribute this software for any purpose
with or without fee is hereby granted.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*/
#ifndef __LIBEX_MSG_H__
#define __LIBEX_MSG_H__

#include <unistd.h>
#include "str.h"
#include "endian.h"

typedef uint32_t msglen_t;
typedef int (*msg_parse_h) (strbuf_t*, char*, msglen_t);

int msg_alloc (strbuf_t *buf, msglen_t len, msglen_t chunk_size);
void msg_reset (strbuf_t *buf);
int msg_read (int fd, strbuf_t *buf);
int msg_parse (strbuf_t *buf, msglen_t off);
ssize_t msg_checklen (const char *buf, msglen_t len);
static inline char *msg_ptr (strbuf_t *buf) {
    return buf->ptr + sizeof(msglen_t);
};
static inline msglen_t msg_getlen (strbuf_t *buf) {
    return *(msglen_t*)buf->ptr;
};

int msg_addbuf(strbuf_t *buf, void *src, msglen_t len);
int msg_addstr (strbuf_t *buf, const char *str, msglen_t slen);
int msg_addi8 (strbuf_t *buf, int8_t v);
int msg_addui8 (strbuf_t *buf, uint8_t v);
int msg_addi16 (strbuf_t *buf, int16_t v);
int msg_addui16 (strbuf_t *buf, uint16_t v);
int msg_addi32 (strbuf_t *buf, int32_t v);
int msg_addui32 (strbuf_t *buf, uint32_t v);
int msg_addi64 (strbuf_t *buf, int64_t v);
int msg_addui64 (strbuf_t *buf, uint64_t v);
int msg_addf4 (strbuf_t *buf, float v);
int msg_addf8 (strbuf_t *buf, double v);
int msg_addf16 (strbuf_t *buf, long double v);

void *msg_getbuf (strbuf_t *buf, size_t len);
int msg_getstr (strbuf_t *buf, strptr_t *str);
int msg_getstrcpy (strbuf_t *buf, strptr_t *str);
cstr_t *msg_getcstr (strbuf_t *buf);
int msg_geti8 (strbuf_t *buf, int8_t *v);
int msg_getui8 (strbuf_t *buf, uint8_t *v);
int msg_geti16 (strbuf_t *buf, int16_t *v);
int msg_getui16 (strbuf_t *buf, uint16_t *v);
int msg_geti32 (strbuf_t *buf, int32_t *v);
int msg_getui32 (strbuf_t *buf, uint32_t *v);
int msg_geti64 (strbuf_t *buf, int64_t *v);
int msg_getui64 (strbuf_t *buf, uint64_t *v);
int msg_getf4 (strbuf_t *buf, float *v);
int msg_getf8 (strbuf_t *buf, double *v);
int msg_getf16 (strbuf_t *buf, long double *v);

int msg_skip (strbuf_t *buf, msglen_t len);


static inline void msg_clear (strbuf_t *buf, int flags) {
    strbufsize(buf, sizeof(msglen_t), flags);
    *(msglen_t*)buf->ptr = 0;
    buf->len = buf->pos = sizeof(msglen_t);
};
static inline void msg_destroy (strbuf_t *buf) {
    if (buf->ptr)
        free(buf->ptr);
    memset(buf, 0, sizeof(strbuf_t));
};

#endif // __LIBEX_MSG_H__
