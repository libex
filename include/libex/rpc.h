/*Copyright (c) Brian B.

Permission to use, copy, modify, and/or distribute this software for any purpose
with or without fee is hereby granted.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*/
#ifndef __LIBEX_RPC_H__
#define __LIBEX_RPC_H__

#include "array.h"
#include "str.h"
#include "msg.h"
#define SRV_BUF_SIZE 256

#define RPC_NULL    0x4000
#define RPC_STRING  0x0001
#define RPC_INTEGER 0x0002
#define RPC_DOUBLE  0x0004
#define RPC_TRUE    0x0008
#define RPC_FALSE   0x0010
#define RPC_ANY     0x7fff

#define RPC_VARG    0x8000

#define RPC_TYPES_LEN(X) X, sizeof(X)/sizeof(rpc_type_t)

#define RPC_OK 0
#define RPC_PARSE_ERROR -32700
#define RPC_INVALID_REQUEST -32600
#define RPC_METHOD_NOT_FOUND -32601
#define RPC_INVALID_PARAMS -32602
#define RPC_INTERNAL_ERROR -32603
#define RPC_ERROR -32000

typedef uint16_t rpc_type_t;
typedef struct rpc_param rpc_param_t;
struct rpc_param {
    rpc_type_t typ;
    union {
        strptr_t    s;
        int64_t     i;
        double      d;
    } data;
};

typedef struct {
    uint32_t method;
    rpc_param_t *params;
    int16_t params_len;
} rpc_request_t;

typedef struct {
    int32_t errcode;
} rpc_response_t;

#define RPC_ID_INT(X) X, -1
#define RPC_ID_NULL 0, 0
#define RPC_ID_STR(X) CONST_STR_LEN(X)

#define RPC_TYPE 0x03
#define RPC_REQUEST 0x01
#define RPC_RESPONSE 0x02
#define RPC_HASID 0x0c
#define RPC_IDSTR 0x04
#define RPC_IDINT 0x08

typedef union {
    int64_t id_int;
    strptr_t id_str;
} rpc_id_t;

typedef struct {
    uint8_t flags;
    rpc_id_t id;
    union {
        rpc_request_t request;
        rpc_response_t response;
    } data;
} rpc_t;
void rpc_clear (rpc_t *rpc);
int rpc_getid (rpc_t *rpc, int is_copy, intptr_t *id);

typedef int (*rpc_method_h) (strbuf_t*, rpc_t*, void*);
typedef struct {
    uint32_t method;
    rpc_method_h handle;
    int arg_len;
    rpc_type_t *arg_types;
} rpc_method_t;
DECLARE_SORTED_ARRAY(rpc_list,rpc_method_t)

rpc_list_t *rpc_init_methods (void);
int rpc_add_method (rpc_list_t **methods,
                    uint32_t method,
                    rpc_method_h handle,
                    rpc_type_t *arg_types,
                    int arg_len);
int rpc_set_method (rpc_list_t *methods,
                    uint32_t method,
                    rpc_method_h handle,
                    rpc_type_t *arg_types,
                    int arg_len);

int rpc_parse (strbuf_t *buf, msglen_t off, rpc_t *rpc);
int rpc_parse_request (strbuf_t *buf, rpc_request_t *req);
int rpc_parse_response(strbuf_t *buf, rpc_response_t *resp);
void rpc_print (rpc_t *rpc);

typedef int (*rpc_h) (strbuf_t*, void*);
int rpc_add_str (strbuf_t *msg, const char *str, size_t slen);
int rpc_add_int (strbuf_t *msg, int64_t i);
int rpc_add_double (strbuf_t *msg, double d);
int rpc_add_true (strbuf_t *msg);
int rpc_add_false (strbuf_t *msg);
int rpc_add_null (strbuf_t *msg);

int rpc_create_request (strbuf_t *buf, uint32_t method, rpc_h on_params, intptr_t id, int id_len, void *userdata);
int rpc_create_response (strbuf_t *buf, int32_t errcode, rpc_h on_response, intptr_t id, int id_len, void *userdata);
static inline int rpc_create_response_ok (strbuf_t *buf, intptr_t id, int id_len) {
    return rpc_create_response(buf, RPC_OK, NULL, id, id_len, NULL);
}
static inline int rpc_create_error (strbuf_t *buf, int32_t errcode, intptr_t id, int id_len) {
    return rpc_create_response(buf, errcode, NULL, id, id_len, NULL);
}

int rpc_execute_method (strbuf_t *buf, rpc_method_t *method, rpc_t *rpc, void *userdata);
int rpc_execute_parsed (strbuf_t *buf, rpc_list_t *methods, rpc_t *rpc, void *userdata);
int rpc_execute (strbuf_t *buf, size_t off, rpc_list_t *methods, void *userdata);

#endif
