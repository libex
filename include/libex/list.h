/*Copyright (c) Brian B.

Permission to use, copy, modify, and/or distribute this software for any purpose
with or without fee is hereby granted.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF
THIS SOFTWARE.
*/
#ifndef __LIBEX_LIST_H__
#define __LIBEX_LIST_H__

#include <stdlib.h>
#include <string.h>
#include <stdint.h>

#define ENUM_CONTINUE 0x00000000
#define ENUM_BREAK 0x00000001
#define ENUM_STOP_IF_BREAK ENUM_BREAK
#define ENUM_FORWARD 0x00000000
#define ENUM_BACKWARD 0x00000002

#define CF_FOUND 0
#define CF_NOTFOUND -1
#define CF_UNDEFINED 0
#define CF_FREE 1

#define CF_UNIQUE 0x00000001
#define CF_REDUCE 0x00000002
#define CF_LOCK 0x00000004

#define DECLARE_LIST(NAME,TYPE) \
    typedef struct NAME##_item NAME##_item_t; \
    typedef struct NAME##_struct NAME##_t; \
    struct NAME##_item { \
        NAME##_item_t *next, *prev; \
        NAME##_t *list; \
        TYPE ptr; \
    }; \
    struct NAME##_struct { \
        unsigned int len; \
        NAME##_item_t *head; \
        void(*on_free)(TYPE*); \
    }; \
    extern NAME##_t *create_##NAME (void); \
    extern void clear_##NAME (NAME##_t *list); \
    extern void free_##NAME (NAME##_t *list); \
    extern TYPE *get_##NAME##_head (NAME##_t *list); \
    extern TYPE *next_##NAME (TYPE *data); \
    extern TYPE *prev_##NAME (TYPE *data); \
    extern TYPE *dequeue_##NAME (NAME##_t *list); \
    extern TYPE *enqueue_##NAME (NAME##_t *list, TYPE *data, size_t size); \
    extern int is_##NAME##_head (TYPE *data); \
    extern int is_##NAME##_tail (TYPE *data); \
    extern int foreach_##NAME (NAME##_t *list, int(*fn)(TYPE*,void*), void *userdata, int flags); \
    extern TYPE *find_##NAME (NAME##_t *list, int(*fn)(const TYPE*,TYPE*), TYPE *data); \
    extern TYPE *add_tail_##NAME (NAME##_t *list, TYPE *val, size_t size); \
    extern TYPE *add_head_##NAME (NAME##_t *list, TYPE *val, size_t size); \
    extern TYPE *del_##NAME (TYPE *data, unsigned int flags); \
    extern void free_##NAME##_item (NAME##_t *list, TYPE *data);

#define DECLARE_LIST_FUN(NAME,TYPE) \
    NAME##_t *create_##NAME (void) { \
        return calloc(1, sizeof(NAME##_t)); \
    } \
    void clear_##NAME (NAME##_t *list) { \
        NAME##_item_t *x = list->head, *y; \
        if (x) { \
            do { \
                y = x->next; \
                if (list->on_free) \
                    list->on_free(&x->ptr); \
                free(x); \
                x = y; \
            } while (x != list->head); \
            list->len = 0; \
            list->head = NULL; \
        } \
    } \
    void free_##NAME (NAME##_t *list) { \
        if (list) { \
            clear_##NAME(list); \
            free(list); \
        } \
    } \
    TYPE *get_##NAME##_head (NAME##_t *list) { \
        if (!list->head) \
            return NULL; \
        return &list->head->ptr; \
    } \
    TYPE *next_##NAME (TYPE *data) { \
        NAME##_item_t *x = (NAME##_item_t*)((uintptr_t)data - sizeof(void*)*3); \
        return &x->next->ptr; \
    } \
    TYPE *prev_##NAME (TYPE *data) { \
        NAME##_item_t *x = (NAME##_item_t*)((uintptr_t)data - sizeof(void*)*3); \
        return &x->prev->ptr; \
    } \
    TYPE *dequeue_##NAME (NAME##_t *list) { \
        return del_##NAME(&list->head->ptr, 0); \
    } \
    TYPE *enqueue_##NAME (NAME##_t *list, TYPE *data, size_t size) { \
        return add_head_##NAME(list, data, size); \
    } \
    int is_##NAME##_head (TYPE *data) { \
        NAME##_item_t *x = (NAME##_item_t*)((uintptr_t)data - (sizeof(NAME##_item_t) - sizeof(TYPE))); \
        return x->list->head == x; \
    } \
    int is_##NAME##_tail (TYPE *data) { \
        NAME##_item_t *x = (NAME##_item_t*)((uintptr_t)data - (sizeof(NAME##_item_t) - sizeof(TYPE))); \
        return x->list->head->prev == x; \
    } \
    int foreach_##NAME (NAME##_t *list, int(*fn)(TYPE*,void*), void *userdata, int flags) { \
        NAME##_item_t *x = list->head; \
        if ((flags & ENUM_BACKWARD)) { \
            if ((x = x->prev)) { \
                do { \
                    if (ENUM_BREAK == fn(&x->ptr, userdata) && (ENUM_STOP_IF_BREAK & flags)) \
                        return ENUM_BREAK; \
                    x = x->prev; \
                } while (x != list->head->prev); \
            } \
        } else { \
            if (x) { \
                do { \
                    if (ENUM_BREAK == fn(&x->ptr, userdata) && (ENUM_STOP_IF_BREAK & flags)) \
                        return ENUM_BREAK; \
                    x = x->next; \
                } while (x != list->head); \
            } \
        } \
        return ENUM_CONTINUE; \
    } \
    TYPE *find_##NAME (NAME##_t *list, int(*fn)(const TYPE*,TYPE*), TYPE *data) { \
        NAME##_item_t *x = list->head; \
        if (x) { \
            do { \
                if (CF_FOUND == fn((const TYPE*)&x->ptr, data)) \
                    return &x->ptr; \
                x = x->next; \
            } while (x != list->head); \
        } \
        return NULL; \
    } \
    TYPE *add_tail_##NAME (NAME##_t *list, TYPE *val, size_t size) { \
        NAME##_item_t *item; \
        size_t payload_size; \
        if (size <= sizeof(TYPE)) { \
            payload_size = sizeof(TYPE); \
            size = sizeof(NAME##_item_t); \
        } else { \
            payload_size = size; \
            size = sizeof(NAME##_item_t) - sizeof(TYPE) + size; \
        } \
        if (!(item = calloc(1, size))) \
            return NULL; \
        if (val) \
            memcpy(&item->ptr, val, payload_size); \
        else \
            memset(&item->ptr, 0, payload_size); \
        if (!list->head) { \
            item->next = item->prev = item; \
            list->head = item; \
        } else { \
            item->next = list->head; \
            item->prev = list->head->prev; \
            list->head->prev = item; \
            item->prev->next = item; \
        } \
        item->list = list; \
        ++list->len; \
        return &item->ptr; \
    } \
    TYPE *add_head_##NAME (NAME##_t *list, TYPE *val, size_t size) { \
        NAME##_item_t *item; \
        size_t payload_size; \
        if (size <= sizeof(TYPE)) { \
            payload_size = sizeof(TYPE); \
            size = sizeof(NAME##_item_t); \
        } else { \
            payload_size = size; \
            size = sizeof(NAME##_item_t) - sizeof(TYPE) + size; \
        } \
        if (!(item = calloc(1, size))) \
            return NULL; \
        if (val) \
            memcpy(&item->ptr, val, payload_size); \
        else \
            memset(&item->ptr, 0, payload_size); \
        if (!list->head) \
            item->next = item->prev = item; \
        else { \
            item->next = list->head; \
            item->prev = list->head->prev; \
            list->head->prev = item; \
            item->prev->next = item; \
        } \
        list->head = item; \
        item->list = list; \
        ++list->len; \
        return &item->ptr; \
    } \
    TYPE *del_##NAME (TYPE *data, unsigned int flags) { \
        NAME##_item_t *x = (NAME##_item_t*)((uintptr_t)data - sizeof(void*)*3), \
                      *prev = x->prev, \
                      *next = x->next; \
        NAME##_t *list = x->list; \
        TYPE *res = NULL; \
        if (prev) prev->next = next; \
        if (next) next->prev = prev; \
        if (x == list->head) list->head = next; \
        if (CF_FREE == flags) { \
            if (list && list->on_free) \
                list->on_free(&x->ptr); \
            free(x); \
        } else { \
            res = &x->ptr; \
            x->next = NULL; x->prev = NULL; x->list = NULL; \
        } \
        if (list) { \
            --list->len; \
            if (!list->len) next = list->head = NULL; \
        } \
        return res; \
    } \
    void free_##NAME##_item (NAME##_t *list, TYPE *data) { \
        NAME##_item_t *x = (NAME##_item_t*)((uintptr_t)data - sizeof(void*)*3); \
        if (list && list->on_free) \
            list->on_free(data); \
        free(x); \
    }

#endif
