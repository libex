/*Copyright (c) Brian B.

Permission to use, copy, modify, and/or distribute this software for any purpose
with or without fee is hereby granted.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE
OF THIS SOFTWARE.
*/
#ifndef __LIBEX_JSON_H__
#define __LIBEX_JSON_H__

#include <stdlib.h>
#include <stdint.h>
#include <stdarg.h>
#include <errno.h>
#include <stdarg.h>
#include "array.h"
#include "str.h"
#include "list.h"
#include "tree.h"

#define JSON_NULL    0x40000000
#define JSON_STRING  0x00000001
#define JSON_INTEGER 0x00000002
#define JSON_DOUBLE  0x00000004
#define JSON_TRUE    0x00000008
#define JSON_FALSE   0x00000010
#define JSON_OBJECT  0x00000020
#define JSON_ARRAY   0x00000040
#define JSON_ANY     0x7fffffff

#define JSON_VARG    0x80000000

typedef struct json_item {
    strptr_t key;
    uint32_t type;
    strptr_t str;
    union {
        strptr_t s;
        int64_t i;
        long double d;
        void *o;
        void *a;
    } data;
} json_item_t;
DECLARE_RBTREE(json_object,json_item_t)
DECLARE_LIST(json_array,json_item_t)

typedef struct {
    char *text;
    char *text_ptr;
    size_t text_len;
    uint32_t type;
    union {
        json_object_t *o;
        json_array_t *a;
    } data;
} json_t;

#define JSON_ISNAME(j,S) 0 == cmpstr(j->key.ptr, j->key.len, CONST_STR_LEN(S))

typedef int (*json_item_h) (json_item_t*, void*);
json_t *json_parse_len (const char *json_str, size_t json_str_len);
static inline json_t *json_parse (const char *json_str) {
    return json_parse_len(json_str, strlen(json_str));
};
json_item_t *json_find (json_object_t *jo, const char *key, size_t key_len, int type);
int json_enum_array (json_array_t *a, json_item_h fn, void *userdata, int flags);
int json_enum_object (json_object_t *obj, json_item_h fn, void *userdata, int flags);
static inline char *json_str(json_item_t *j) { return strndup(j->data.s.ptr, j->data.s.len); }
cstr_t *json_item_as_str (json_item_t *ji);
void json_free (json_t *j);

#define JSON_NOT_INSERTED 0
#define JSON_INSERTED 1

#define JSON_DOUBLE_PRECISION 6

void json_add_key (strbuf_t *buf, const char *key, size_t key_len);
void json_add_str (strbuf_t *buf, const char *key, size_t key_len,
                   const char *val, size_t val_len);
void json_add_escape_str (strbuf_t *buf, const char *key, size_t key_len,
                          const char *val, size_t val_len);
void json_add_unescape_str (strbuf_t *buf, const char *key, size_t key_len,
                            const char *val, size_t val_len);
void json_add_int (strbuf_t *buf, const char *key, size_t key_len,
                   int64_t val);
void json_add_double_p (strbuf_t *buf, const char *key, size_t key_len,
                        long double val, int prec);
static inline void json_add_double (strbuf_t *buf, const char *key, size_t key_len,
                                    long double val) {
                                    json_add_double_p(buf, key, key_len, val, JSON_DOUBLE_PRECISION); }
void json_add_null (strbuf_t *buf, const char *key, size_t key_len);
void json_add_bool (strbuf_t *buf, const char *key, size_t key_len, int val);
static inline void json_add_true (strbuf_t *buf, const char *key, size_t key_len) {
                   json_add_bool(buf, key, key_len, 1); }
static inline void json_add_false (strbuf_t *buf, const char *key, size_t key_len) {
                   json_add_bool(buf, key, key_len, 0); }
static inline void json_add_item_str (strbuf_t *buf, const char *val, size_t val_len) {
                   json_add_str(buf, CONST_STR_NULL, val, val_len); }
static inline void json_add_item_int (strbuf_t *buf, int64_t val) {
                   json_add_int(buf, CONST_STR_NULL, val); }
static inline void json_add_item_null (strbuf_t *buf) {
                   json_add_null(buf, CONST_STR_NULL); }
static inline void json_add_item_bool (strbuf_t *buf, int val) {
                   json_add_bool(buf, CONST_STR_NULL, val); }
static inline void json_add_item_true (strbuf_t *buf) {
                   json_add_bool(buf, CONST_STR_NULL, 1); }
static inline void json_add_item_false (strbuf_t *buf) {
                   json_add_bool(buf, CONST_STR_NULL, 0); }
void json_open_array (strbuf_t *buf, const char *key, size_t key_len);
void json_close_array (strbuf_t *buf);
static inline void json_empty_array (strbuf_t *buf, const char *key, size_t key_len) {
                   json_open_array(buf, key, key_len); json_close_array(buf); };
void json_open_object (strbuf_t *buf, const char *key, size_t key_len);
void json_close_object (strbuf_t *buf);
static inline void json_empty_object (strbuf_t *buf, const char *key, size_t key_len) {
                   json_open_object(buf, key, key_len); json_close_object(buf); };
static inline void json_begin_object (strbuf_t *buf) { json_open_object(buf, CONST_STR_NULL); }
static inline void json_end_object (strbuf_t *buf) { json_close_object(buf); }
static inline void json_begin_array (strbuf_t *buf) { json_open_array(buf, CONST_STR_NULL); }
static inline void json_end_array (strbuf_t *buf) { json_close_array(buf); }

#define JSONRPC_OK 0
#define JSONRPC_PARSE_ERROR -32700
#define JSONRPC_INVALID_REQUEST -32600
#define JSONRPC_METHOD_NOT_FOUND -32601
#define JSONRPC_INVALID_PARAMS -32602
#define JSONRPC_INTERNAL_ERROR -32603
#define JSONRPC_ERROR_MAX -32000
#define JSONRPC_ERROR_MIN -32099

#define JSONRPC_PARSE_ERROR_STR "Invalid JSON was received by the server."
#define JSONRPC_INVALID_REQUEST_STR "The JSON sent is not a valid Request object."
#define JSONRPC_METHOD_NOT_FOUND_STR "The method does not exist."
#define JSONRPC_INVALID_PARAMS_STR "Invalid method parameters."
#define JSONRPC_INTERNAL_ERROR_STR "Internal JSON-RPC error."

#define JSONRPC_ID_INT(X) X, -1
#define JSONRPC_ID_NULL 0, 0
#define JSONRPC_ID_STR(X) CONST_STR_LEN(X)

#define JSONRPC_TYPES_LEN(X) X, sizeof(X)/sizeof(int)

typedef enum {
    JSONRPC_VNONE,
    JSONRPC_V10,
    JSONRPC_V20
} jsonrpc_ver_t;

typedef struct {
    jsonrpc_ver_t ver;
    json_item_t *id;
    strptr_t method;
    json_item_t *params;
    json_item_t *result;
    int64_t error_code;
    strptr_t error_message;
    json_item_t *error_data;
    json_t *json;
} jsonrpc_t;

typedef struct {
    jsonrpc_t *jsonrpc;
    int errcode;
} jsonrpc_enum_t;

int jsonrpc_parse_request_len (const char *json_str, size_t json_str_len, jsonrpc_t *jsonrpc);
int jsonrpc_parse_request (const char *json_str, jsonrpc_t *jsonrpc);
int jsonrpc_parse_response_len (const char *json_str, size_t json_str_len, jsonrpc_t *jsonrpc);
int jsonrpc_parse_response (const char *json_str, jsonrpc_t *jsonrpc);

int jsonrpc_parse (const char *json_str, size_t json_str_len, jsonrpc_t *jsonrpc);
static inline int jsonrpc_is_request (jsonrpc_t *jsonrpc) {
    if (!jsonrpc->id || (jsonrpc->id->type != JSON_INTEGER && jsonrpc->id->type != JSON_STRING) || jsonrpc->ver == JSONRPC_VNONE || !jsonrpc->method.len)
        return 0;
    return 1;
}
static inline int jsonrpc_is_response (jsonrpc_t *jsonrpc) {
    if (!jsonrpc->id || jsonrpc->ver != JSONRPC_VNONE || (jsonrpc->result && jsonrpc->error_message.ptr) || (!jsonrpc->result && !jsonrpc->error_message.ptr))
        return 0;
    return 1;
}

typedef int (*jsonrpc_h) (strbuf_t*, void *userdata);
void jsonrpc_setver (jsonrpc_ver_t ver);

extern char json_prefix;
extern size_t json_prefix_len;

#define jsonrpc_open_request(buf,method,method_len) \
    buf->len = 0; \
    if (json_prefix_len > 0) \
        strbufset(buf, json_prefix, json_prefix_len); \
    json_begin_object(buf); \
    json_add_str(buf, CONST_STR_LEN("jsonrpc"), jsonrpc_version, 3); \
    json_add_str(buf, CONST_STR_LEN("method"), method, method_len); \
    json_open_array(buf)

#define jsonrpc_close_request(buf,id,id_len) \
    json_close_array(buf); \
    jsonrpc_id(buf, id, id_len); \
    json_end_object(buf)

#define jsonrpc_open_response(buf) \
    buf->len = 0; \
    if (json_prefix_len > 0) \
        strbufset(buf, json_prefix, json_prefix_len); \
    json_begin_object(buf); \
    json_add_key(buf, CONST_STR_LEN("result"))

#define jsonrpc_close_response(buf,id,id_len) \
    strbufadd(buf, CONST_STR_LEN(",")); \
    json_add_null(buf, CONST_STR_LEN("error")); \
    jsonrpc_id(buf, id, id_len); \
    json_end_object(buf)

void jsonrpc_prepare (strbuf_t *buf);
int jsonrpc_request (strbuf_t *buf, const char *method, size_t method_len, intptr_t id, int id_len, jsonrpc_h on_params, void *userdata);
int jsonrpc_requestf (strbuf_t *buf, const char *method, size_t method_len, intptr_t id, int id_len, const char *fmt, ...);
int jsonrpc_response (strbuf_t *buf, jsonrpc_h on_result, void *userdata, intptr_t id, int id_len);
static inline void jsonrpc_response_ok (strbuf_t *buf, intptr_t id, int id_len) {
    jsonrpc_response(buf, ({
        int fn (strbuf_t *buf, void *dummy) {
            json_add_true(buf, CONST_STR_NULL);
            return 0;
        } fn;
    }), NULL, id, id_len);
}

static inline void jsonrpc_response_fail (strbuf_t *buf, intptr_t id, int id_len) {
    jsonrpc_response(buf, ({
        int fn (strbuf_t *buf, void *dummy) {
            json_add_false(buf, CONST_STR_NULL);
            return 0;
        } fn;
    }), NULL, id, id_len);
}

static inline void jsonrpc_response_bool (strbuf_t *buf, int val, intptr_t id, int id_len) {
    jsonrpc_response(buf, ({
        int fn (strbuf_t *buf, void *dummy) {
            if (val)
                json_add_true(buf, CONST_STR_NULL);
            else
                json_add_false(buf, CONST_STR_NULL);
            return 0;
        } fn;
    }), NULL, id, id_len);
}

static inline void jsonrpc_response_str (strbuf_t *buf, const char *str, size_t len, intptr_t id, int id_len) {
    jsonrpc_response(buf, ({
        int fn (strbuf_t *buf, void *dummy) {
            json_add_escape_str(buf, CONST_STR_NULL, str, len);
            return 0;
        } fn;
    }), NULL, id, id_len);
}

static inline void jsonrpc_response_int (strbuf_t *buf, long long int val, intptr_t id, int id_len) {
    jsonrpc_response(buf, ({
        int fn (strbuf_t *buf, void *dummy) {
            json_add_int(buf, CONST_STR_NULL, val);
            return 0;
        } fn;
    }), NULL, id, id_len);
}

static inline void jsonrpc_response_raw (strbuf_t *buf, const char *str, size_t slen, intptr_t id, int id_len) {
    jsonrpc_response(buf, ({
        int fn (strbuf_t *buf, void *dummy) {
            strbufadd(buf, str, slen);
            return 0;
        } fn;
    }), NULL, id, id_len);
}

static inline void jsonrpc_response_null (strbuf_t *buf, intptr_t id, int id_len) {
    jsonrpc_response(buf, ({
        int fn (strbuf_t *buf, void *dummy) {
            json_add_null(buf, CONST_STR_NULL);
            return 0;
        } fn;
    }), NULL, id, id_len);
}

int jsonrpc_error (strbuf_t *buf, int code, const char *message, size_t message_len, intptr_t id, int id_len);
int jsonrpc_stderror (strbuf_t *buf, int code, intptr_t id, int id_len);

typedef int (*jsonrpc_method_h) (strbuf_t *buf, json_item_t **params, size_t params_len, intptr_t id, int id_len, void*);

typedef struct {
    int id;
    strptr_t method;
    jsonrpc_method_h handle;
    int param_lens;
    int *param_types;
} jsonrpc_method_t;
DECLARE_SORTED_ARRAY(jsonrpc_method_list, jsonrpc_method_t)

jsonrpc_method_list_t *jsonrpc_init (void);
void jsonrpc_done (jsonrpc_method_list_t **methods);

int jsonrpc_add_method (jsonrpc_method_list_t **methods,
                        int id,
                        const char *method, size_t len,
                        jsonrpc_method_h handle,
                        int *param_types, size_t param_len);
int jsonrpc_getid (jsonrpc_t *jsonrpc, intptr_t *id, int *id_len);
jsonrpc_method_t *jsonrpc_find_method (jsonrpc_method_list_t *methods, jsonrpc_t *jsonrpc);
int jsonrpc_execute_method (strbuf_t *buf, jsonrpc_method_t *method, void *userdata, jsonrpc_t *jsonrpc);
int jsonrpc_execute_parsed (jsonrpc_method_list_t *mehods, strbuf_t *buf, size_t off,
                            void *userdata, jsonrpc_t *jsonrpc);
int jsonrpc_execute (jsonrpc_method_list_t *methods, strbuf_t *buf, size_t off, void *userdata);

#endif // __LIBEX_JSON_H__
