/*Copyright (c) Brian B.
  Copyright (c) Chris Venter : chris.venter@gmail.com

Permission to use, copy, modify, and/or distribute this software for any purpose
with or without fee is hereby granted.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF
THIS SOFTWARE.
*/
#ifndef __LIBEX_STR_H__
#define __LIBEX_STR_H__

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <errno.h>
#include <stdarg.h>
#include <time.h>
#include <ctype.h>
#include <locale.h>
#include <wchar.h>
#include <wctype.h>
#include "sys.h"

#ifdef _AIX
#include <alloca.h>
#endif

#ifndef __GNUC__
void libex_str_init (void);
#endif

#ifdef __m64__
#define LONG_FMT "%ld"
#define ULONG_FMT "%lu"
#define TIME_FMT "%lu"
#define SIZE_FMT "%lu"
#define LONG_HEXFMT "%02lX"
#else
#define LONG_FMT "%lld"
#define ULONG_FMT "%llu"
#ifdef _AIX
#define TIME_FMT "%d"
#else
#define TIME_FMT "%lu"
#endif
#define SIZE_FMT "%lu"
#define LONG_HEXFMT "%02llX"
#endif

#define STR(x) #x
#define STR_SPACES " \f\n\r\t\v"
#ifndef CONST_STR_LEN
#define CONST_STR_LEN(x) x, x ? sizeof(x) - 1 : 0
#define WCONST_STR_LEN(x) x, x ? (sizeof(x) / sizeof(wchar_t) - 1) : 0
#endif
#define CONST_STR_NULL NULL,0
#define STR_REDUCE 0x0001
#define STR_KEEPLEN 0x0002
#define STR_ADD_NULL(x) (x)->ptr[(x)->len] = '\0'
#define WSTR_ADD_NULL(x) (x)->ptr[(x)->len] = L'\0'
#define CONST_STR_INIT(s) { .len = sizeof(s)-1, .ptr = s }
#define CONST_STR_INIT_NULL { .len = 0, .ptr = NULL }
#define CONST_STR_SET(x,s) (x).ptr = s; (x).len = sizeof(s)-1

#define STR_LEFT 0x0001
#define STR_CENTER 0x0002
#define STR_RIGHT 0x0004
#define STR_MAYBE_UTF 0x8000

#define RAND_ALPHA 0x0001
#define RAND_ALNUM 0x0002
#define RAND_UPPER 0x0010
#define RAND_LOWER 0x0020

#define DEFAULT_STRLEN 64

typedef struct {
    size_t len;
    char ptr [0];
} cstr_t;

typedef struct {
    size_t len;
    size_t bufsize;
    size_t chunk_size;
    char ptr [0];
} str_t;

typedef struct {
    size_t len;
    size_t bufsize;
    size_t chunk_size;
    wchar_t ptr [0];
} wstr_t;

typedef struct {
    size_t len;
    char *ptr;
} strptr_t;

typedef struct {
    size_t len;
    wchar_t *ptr;
} wstrptr_t;

typedef struct {
    size_t len;
    size_t bufsize;
    size_t chunk_size;
    char *ptr;
    size_t pos;
} strbuf_t;

#define isunicode(c) (((c)&0xc0)==0xc0)

cstr_t *mkcstr (const char *str, size_t len);
str_t *stralloc (size_t len, size_t chunk_size);
wstr_t *wstralloc (size_t len, size_t chunk_size);
wstr_t *str2wstr (const char *str, size_t str_len, size_t chunk_size);
str_t *wstr2str (const wchar_t *str, size_t str_len, size_t chunk_size);
str_t *mkstr (const char *str, size_t len, size_t chunk_size);
int mkstrptr (const char *str, size_t len, strptr_t *result);
wstr_t *wmkstr (const wchar_t *str, size_t len, size_t chunk_size);
cstr_t *mkcstrfill (size_t len, int c);
cstr_t *mkcstrpad (const char *str, size_t len, int c, size_t max_len);
int strsize (str_t **str, size_t nlen, int flags);
size_t strwlen (const char *str, size_t str_len);
void strwupper (char *str, size_t str_len, locale_t locale);
void strwlower (char *str, size_t str_len, locale_t locale);
int strpad (str_t **str, size_t nlen, char filler, int flags);
int strput (str_t **str, const char *src, size_t src_len, int flags);
int strfill (str_t **str, int c, int len);
str_t *strput2 (str_t *str, const char *src, size_t src_len, int flags);
char *strputc (char *old_str, const char *new_str);
void strdel (str_t **str, char *pos, size_t len, int flags);
int strnadd (str_t **str, const char *src, size_t src_len);
int wstrnadd (wstr_t **str, const wchar_t *src, size_t src_len);
str_t *strconcat (str_t *res, size_t chunk_size, const char *arg, size_t arg_len, ...);
str_t *vstrfmt (const char *fmt, va_list args);
str_t *vstrfmtadd (str_t *res, const char *fmt, va_list args);
__attribute__ ((format (printf, 1, 2)))
str_t *strfmt (const char *fmt, ...);
str_t *strfmtadd (str_t *res, const char *fmt, ...);
cstr_t *vcstrfmt (const char *fmt, va_list args);
__attribute__ ((format (printf, 1, 2)))
cstr_t *cstrfmt (const char *fmt, ...);
str_t *strev (const char *str, size_t len, size_t chunk_size);
strptr_t strntrim (char *str, size_t str_len);
int strntok (char **str, size_t *str_len, const char *sep, size_t sep_len, strptr_t *ret);
int strepl (str_t **str, char *dst_pos, size_t dst_len, const char *src_pos, size_t src_len);
str_t *strsplit (const char *s, size_t len, char delim);
int strnext (str_t *str, strptr_t *entry);
int strcount (str_t *str);
char *strnchr (const char *s, int c, size_t str_len);
char *strnrchr (const char *s, int c, size_t str_len);
char *strnstr (const char *str, size_t str_len, const char *needle, size_t n_len);
str_t *strhex (const char *prefix, const char *str, size_t str_len, size_t chunk_size);
cstr_t *cstrhex (const char *prefix, const char *str, size_t str_len);
str_t *hexstr (const char *str, size_t str_len, size_t chunk_size);
int cmpstr (const char *x, size_t x_len, const char *y, size_t y_len);
int wcmpstr (const wchar_t *x, size_t x_len, const wchar_t *y, size_t y_len);
int cmpcasestr (const char *x, size_t x_len, const char *y, size_t y_len);
int wcmpcasestr (const wchar_t *x, size_t x_len, const wchar_t *y, size_t y_len);
static inline str_t *strcopy (str_t *str) { return mkstr(str->ptr, str->len, str->chunk_size); };
static inline wstr_t *wstrcopy (wstr_t *str) { return wmkstr(str->ptr, str->len, str->chunk_size); };
static inline cstr_t *cstrcopy (cstr_t *str) { return mkcstr(str->ptr, str->len); };

int strbufalloc (strbuf_t *strbuf, size_t len, size_t chunk_size);
int strbufsize (strbuf_t *strbuf, size_t nlen, int flags);
int strbufput (strbuf_t *strbuf, const char *src, size_t src_len, int flags);
int strbufadd (strbuf_t *strbuf, const char *src, size_t src_len);
int strbufset (strbuf_t *buf, const char c, size_t len);
void vstrbufmt (strbuf_t *buf, const char *fmt, va_list args);
void strbufmt (strbuf_t *buf, const char *fmt, ...);

void b64_encode(const unsigned char *in, size_t len, char *out, size_t elen);
int b64_decode(const char *in, size_t len, unsigned char *out, size_t outlen);

size_t b64_encoded_len (size_t inlen);
size_t b64_decoded_len (const char *in, size_t len);
void b64_encode(const unsigned char *in, size_t len, char *out, size_t elen);
int b64_decode(const char *in, size_t len, unsigned char *out, size_t outlen);
str_t *str_b64encode (const char *buf, size_t bufsize, size_t chunk_size);
str_t *str_b64decode (const char *buf, size_t bufsize, size_t chunk_size);
cstr_t *cstr_b64encode (const char *buf, size_t bufsize);
cstr_t *cstr_b64decode (const char *buf, size_t bufsize);
int strbuf_b64encode (strbuf_t *encoded, const char *input, size_t input_len, size_t chunk_size);
int strbuf_b64decode (strbuf_t *decoded, const char *input, size_t input_len, size_t chunk_size);

str_t *str_url_encode (const char *str, size_t str_len, size_t chunk_size);
str_t *str_url_decode (const char *str, size_t str_len, size_t chunk_size);

str_t *str_unescape (const char *src, size_t src_len, size_t chunk_size);
int strbuf_unescape (strbuf_t *strbuf, const char *src, size_t src_len);
str_t *str_escape (const char *src, size_t src_len, size_t chunk_size);
int strbuf_escape (strbuf_t *dst, const char *src, size_t src_len);

static inline size_t strset_count (void *strset) { return *((size_t*)(strset+sizeof(size_t))); }
static inline size_t strset_size (void *strset) { return *((size_t*)strset); }
void *mkstrset (size_t count, const char **strs);
void *mkstrset2 (const char **strs, size_t count, size_t *idxs);
void *strset_start (void *strset, size_t *len);
strptr_t strset_fetch (void **strset_ptr);
strptr_t strset_get (void *strset, size_t index);
int strand (char *outbuf, size_t outlen, int flags);

typedef struct
{
    uint32_t total[2];
    uint32_t state[5];
    uint8_t buffer[64];
} libex_sha1_t;

void libex_sha1_init (libex_sha1_t *ctx);
void libex_sha1_update (libex_sha1_t *ctx, uint8_t *input, uint32_t length);
void libex_sha1_done (libex_sha1_t *ctx, uint8_t digest[20]);

#endif // __LIBEX_STR_H__
