/*Copyright (c) Brian B.

Permission to use, copy, modify, and/or distribute this software for any purpose
with or without fee is hereby granted.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*/
#ifndef __LIBEX_UNET_H__
#define __LIBEX_UNET_H__

#include <stdint.h>
#include <unistd.h>
#include <sys/socket.h>
#include <sys/un.h>
#include "str.h"
#include "msg.h"
#include "net.h"
#if 0
int unet_netbuf_alloc (netbuf_t *nbuf, size_t start_len, size_t chunk_size);
void unet_netbuf_free (netbuf_t *nbuf);

typedef int (*on_parse_msg) (strbuf_t*);

int unet_bind (const char *sock_file);
int unet_connect (const char *sock_file);
int unet_read (int fd, strbuf_t *msg, on_parse_msg on_msg);
static inline int unet_read_request (int fd, strbuf_t *msg) { return unet_read(fd, msg, msg_load_request); }
static inline int unet_read_response (int fd, strbuf_t *msg) { return unet_read(fd, msg, msg_load_response); }
ssize_t unet_write (int fd, char *buf, size_t size);
int unet_recv (int fd, netbuf_t *nbuf, strbuf_t *result, msg_parse_h on_parse);
int unet_recv_request(int fd, netbuf_t *buf, strbuf_t *result);
int unet_recv_response (int fd, netbuf_t *buf, strbuf_t *result);
void unet_reset (netbuf_t *nbuf);
#endif
#endif // __LIBEX_UNET_H__
