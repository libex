/*Copyright (c) Brian B.

Permission to use, copy, modify, and/or distribute this software for any purpose
with or without fee is hereby granted.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*/
#ifndef __LIBEX_NETPOLL_H__
#define __LIBEX_NETPOLL_H__

#include <stdlib.h>
#include <stdint.h>
#include <time.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <resolv.h>
#ifdef USE_SSL
#include <openssl/ssl.h>
#include <openssl/err.h>
#endif
#include <limits.h>
#include "../str.h"
#include "../sys.h"
#include "../ws.h"
#include "../list.h"
#include "../task.h"
#include "../http.h"
#include "../tree.h"

#define NET_OK 0
#define NET_ERROR -1
#define NET_WAIT 1
#define NET_EMPTY NET_WAIT
#define NET_CLOSE NET_ERROR

#define NF_READ 1

#define NF_CONNECTED 0x00000001
#define NF_CLOSE 0x00000002
#define NF_CLIENT 0x00000004
#define NF_WSCONNECTED 0x00000008
#define NF_SENDBUF 0x00000010

#define NF_STATE 0x00000060
#define NF_INIT 0x00000020
#define NF_WAIT 0x00000040
#define NF_DONE 0x00000000

#ifdef USE_SSL
#define NF_SSLACCEPTED 0x80000000

#define SSL_CLIENT 0
#define SSL_SERVER 1
#endif

typedef struct {
    strbuf_t buf;
    strbuf_t tail;
} netbuf_t;

typedef uint32_t net_flags_t;

typedef struct net_ev net_ev_t;
typedef struct ev_buf ev_buf_t;
typedef struct net_srv net_srv_t;
typedef struct net_recon net_recon_t;

typedef int (*net_srv_h) (net_srv_t*);
typedef int (*net_ev_h) (int, net_srv_t*);
typedef int (*net_ev_recv_h) (int, int, net_srv_t*);
typedef int (*net_ev_handle_h) (ev_buf_t*);
typedef int (*net_ev_buf_set_h) (ev_buf_t*, net_ev_t*);
typedef int (*net_ev_connect_h) (int, net_srv_t*, uint32_t*, http_url_t*);
typedef void (*net_ev_buf_free_h) (ev_buf_t*);
typedef int (*net_ev_buf_h) (net_srv_t*, ev_buf_t*);
typedef int (*check_url_h) (int fd, net_srv_t*, strptr_t*);
typedef struct net_ev *(*get_ev_h) (net_srv_t*, int);

typedef uint64_t ev_fd_t;
DECLARE_LIST(ev_list,ev_fd_t)
typedef union {
    ev_fd_t fd;
    struct {
        int fd;
        unsigned int id;
    } key;
} fd_t;
#define NET_FD(x) ((fd_t)x).key.fd

#define FDINFO_MAX 8192
#define NF_RECONNECT 0x00000001
struct net_ev {
    ev_fd_t fd;
    struct in_addr addr;
    ev_fd_t *ptr;
    time_t tm;
    net_flags_t nf_flags;
    netbuf_t rd_buf;
    strbuf_t wr_buf;
    size_t wrote;
    cstr_t *url;
    ws_t ws;
    uint8_t ver;
#ifdef USE_SSL
    SSL *ssl;
#endif
} __attribute__ ((aligned(64)));
DECLARE_LIST(net_ev_list,net_ev_t)

typedef struct {
    pthread_mutex_t locker;
    net_ev_list_t events;
    net_flags_t flags;
} fdinfo_t;

typedef struct ev_locker ev_locker_t;

struct ev_locker {
    int64_t id_rpc;
    pthread_mutex_t *locker;
    pthread_cond_t *cond;
    pthread_condattr_t *condattr;
    strbuf_t buf;
    ws_t ws;
};
DECLARE_RBTREE(msginfo, ev_locker_t*)

struct net_recon {
    net_srv_t *srv;
    cstr_t *url;
    pthread_t th;
    pthread_mutex_t locker;
    pthread_cond_t cond;
    pthread_condattr_t condattr;
    int counter;
};
DECLARE_LIST(recon_list,net_recon_t)

#define SF_RECONNECT 0x00000001
struct net_srv {
    int efd;
    int sfd;
    unsigned int id;
    int is_active;
    int tm_connect;
    int tm_wait;
    int tm_count;
    net_flags_t flags;
    size_t ev_bufsize;
    pool_t *pool;
    pthread_mutex_t locker;
    fdinfo_t fd_info [FDINFO_MAX];
    ev_list_t *id_info;
    recon_list_t *recon_list;
    pthread_mutex_t recon_locker;
    pthread_barrier_t *recon_barrier;
    net_ev_connect_h on_connect;
    net_ev_h on_close;
    net_ev_h on_close_ev;
    net_ev_h on_free_ev;
    net_ev_recv_h on_recv;
    net_ev_buf_h on_recvd;
    net_ev_h on_send;
    pool_msg_h on_event;
    net_ev_handle_h on_handle;
    net_ev_buf_set_h on_evbuf_set;
    net_ev_buf_free_h on_evbuf_free;
    net_srv_h on_start;
    net_srv_h on_stop;
    net_srv_h on_ping;
    get_ev_h get_ev;
    int (*on_poll_prepare) (net_srv_t*);
    void (*on_poll_ev_close) (int, net_srv_t*);
    int (*on_poll_ev_create) (int, net_srv_t*, int*);
    int (*on_poll_in) (int, net_srv_t*);
    int (*on_poll_out) (int, net_srv_t*);
    int (*om_poll_check_out) (int, net_srv_t*);
    void (*on_poll_close) (net_srv_t*);
    pthread_cond_t cond;
    pthread_rwlock_t rw_locker;
    check_url_h on_check_url;
    pthread_t th_ping;
    pthread_mutex_t ping_locker;
    pthread_condattr_t ping_condattr;
    pthread_cond_t ping_cond;
    int tm_ping;
    uint64_t id_rpc;
    msginfo_t *msg_info;
    int tm_recv;
    int tm_close;
    void *userdata;
#ifdef USE_SSL
    SSL_CTX *ctx_cli;
    SSL_CTX *ctx_srv;
#endif
};

int net_poll_create (void);
int net_poll_destroy (int efd, int sfd);
int net_poll_ctl_listener (int efd, int sfd);
int net_poll_ctl_add (int efd, int fd);
int net_poll_ctl_out (int efd, int fd);
void net_poll (net_srv_t *srv);

#endif
