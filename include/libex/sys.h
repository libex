/*Copyright (c) Brian B.

Permission to use, copy, modify, and/or distribute this software for any purpose
with or without fee is hereby granted.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF
THIS SOFTWARE.
*/
#ifndef __LIBEX_SYS_H__
#define __LIBEX_SYS_H__

#include <limits.h>

typedef int locker_t;

#if defined(__GNUC__) || (defined(__IBMC__) && __IBMC__ >= 1210)
#define __ATOMICS__
#endif

#ifdef __ATOMICS__
#define __atomic_inc_int(P) __sync_add_and_fetch((P), 1)
#define __atomic_inc_uint(P) __sync_add_and_fetch((P), 1)
#define __atomic_dec_int(P) __sync_sub_and_fetch((P), 1)
#define __atomic_dec_uint(P) __sync_sub_and_fetch((P), 1)
#define __atomic_inc_long(P) __sync_add_and_fetch((P), 1)
#define __atomic_inc_ulong(P) __sync_add_and_fetch((P), 1)
#define __atomic_dec_long(P) __sync_sub_and_fetch((P), 1)
#define __atomic_dec_ulong(P) __sync_sub_and_fetch((P), 1)

#define add_and_fetch_int(P,V) __sync_add_and_fetch((P),V)
#define sub_and_fetch_int(P,V) __sync_sub_and_fetch((P),V)
#define add_and_fetch_uint(P,V) __sync_add_and_fetch((P),V)
#define sub_and_fetch_uint(P,V) __sync_add_and_fetch((P),V)
#define add_and_fetch_long(P,V) __sync_add_and_fetch((P),V)
#define sub_and_fetch_long(P,V) __sync_sub_and_fetch((P),V)
#define add_and_fetch_ulong(P,V) __sync_add_and_fetch((P),V)
#define sub_and_fetch_ulong(P,V) __sync_sub_and_fetch((P),V)

#define __lock(__locker__) while (__sync_lock_test_and_set(&__locker__, 1))
#define __unlock(__locker__) __sync_lock_release(&__locker__)
#elif defined(__powerpc) || defined(__PPC)
#undef _NO_PROTO
#include <sys/atomic_op.h>
boolean_t test_and_set(atomic_p, int);

static inline long long __lw (volatile int *p) {
    int v;
    __asm__ __volatile__ ("lwarx %0, 0, %2" : "=r"(v) : "m"(*p), "r"(p));
    return v;
}

static inline long long __ll (volatile long long *p) {
    long long v;
    __asm__ __volatile__ ("ldarx %0, 0, %2" : "=r"(v) : "m"(*p), "r"(p));
    return v;
}

static inline int __sw (volatile int *p, int v) {
    int r;
    __asm__ __volatile__ ("stwcx. %2, 0, %3 ; mfcr %0"
                          : "=r"(r), "=m"(*p) : "r"(v), "r"(p) : "memory", "cc");
    return r & 0x20000000;
}

static inline int __sl (volatile long long *p, long long v) {
    int r;
    __asm__ __volatile__ ("stdcx. %2, 0, %3 ; mfcr %0"
                          : "=r"(r), "=m"(*p) : "r"(v), "r"(p) : "memory", "cc");
    return r & 0x20000000;
}

#define def_op_and_fetch(FS,ST,TS,NOP,OP,T) \
static inline T NOP##_and_fetch_##TS (T *P, T V) { \
    T r; \
    __asm__ __volatile__ ("sync" : : : "memory"); \
    do r = (T)__l##FS((ST*)P); \
    while (!__s##FS((ST*)P, r OP V)); \
    r = *P; \
    __asm__ __volatile__ ("isync" : : : "memory"); \
    return r; \
}

def_op_and_fetch(w,int,int,add,+,int)
def_op_and_fetch(w,int,int,sub,-,int)
def_op_and_fetch(w,int,uint,add,+,unsigned int)
def_op_and_fetch(w,int,uint,sub,-,unsigned int)
def_op_and_fetch(l,long long,long,add,+,long long)
def_op_and_fetch(l,long long,long,sub,-,long long)
def_op_and_fetch(l,long long,ulong,add,+,unsigned long long)
def_op_and_fetch(l,long long,ulong,sub,-,unsigned long long)


int add_and_fetch_int (int*, int);
int sub_and_fetch_int (int*, int);
unsigned int add_and_fetch_uint (unsigned int*, unsigned int);
unsigned int sub_and_fetch_uint (unsigned int*, unsigned int);
long long int add_and_fetch_long (long long int*, long long int);
long long int sub_and_fetch_long (long long int*, long long int);
unsigned long long int add_and_fetch_ulong (unsigned long long int*, unsigned long long int);
unsigned long long int sub_and_fetch_ulong (unsigned long long int*, unsigned long long int);

// long and ulong means 64-bit values in macros
#define __atomic_inc_int(P) add_and_fetch_int((P), 1)
#define __atomic_inc_uint(P) add_and_fetch_uint((P), 1)
#define __atomic_dec_int(P) sub_and_fetch_int((P), 1)
#define __atomic_dec_uint(P) sub_and_fetch_uint((P), 1)
#define __atomic_inc_long(P) add_add_and_fetch_long((P), 1LL)
#define __atomic_inc_ulong(P) add_and_fetch_ulong((P), 1LL)
#define __atomic_dec_long(P) sub_and_fetch_long((P), 1LL)
#define __atomic_dec_ulong(P) sub_and_fetch_ulong((P), 1LL)

#define __lock(__locker__) while (!test_and_set(&__locker__, 1))
#define __unlock(__locker__) _clear_lock(&__locker__, 0)
#endif

#if defined(__X86_64__) || defined(__i386__)
#define __breakpoint __asm__ __volatile__ ("int3" : : : "memory")
#else
#define __breakpoint __asm__ __volatile__ (".long 0x7d821008" : : : "memory")
#endif

#endif // __LIBEX_SYS_H__
