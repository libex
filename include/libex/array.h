/*Copyright (c) Brian B.

Permission to use, copy, modify, and/or distribute this software for any purpose
with or without fee is hereby granted.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF
THIS SOFTWARE.
*/
#ifndef __LIBEX_ARRAY_H__
#define __LIBEX_ARRAY_H__

#include "list.h"

#define DECLARE_ARRAY(NAME,TYPE) \
    typedef struct { \
        int len; \
        unsigned int flags; \
        int maxsize; \
        int bufsize; \
        int datasize; \
        int chunk_size; \
        void(*on_free)(TYPE*); \
        TYPE(*on_copy)(TYPE*); \
        TYPE ptr [0]; \
    } NAME##_t; \
    extern NAME##_t *create_##NAME (int len, int chunk_size, unsigned int flags); \
    extern NAME##_t *copy_##NAME (NAME##_t *src); \
    extern void clear_##NAME (NAME##_t **array); \
    extern void free_##NAME (NAME##_t **array); \
    extern TYPE *next_##NAME (NAME##_t *array, TYPE *data); \
    extern TYPE *prev_##NAME (NAME##_t *array, TYPE *data); \
    extern int foreach_##NAME (NAME##_t *array, int(*fn)(TYPE*,void*), void *userdata, int flags); \
    extern TYPE *find_##NAME (NAME##_t *array, int(*fn)(TYPE*,TYPE*), TYPE *data); \
    extern TYPE *add_##NAME (NAME##_t **array, TYPE *val); \
    extern void del_##NAME (NAME##_t **array, TYPE *item);

#define DECLARE_ARRAY_FUN(NAME,TYPE) \
    NAME##_t *create_##NAME (int len, int chunk_size, unsigned int flags) { \
        NAME##_t *res; \
        if (chunk_size > 0) { \
            int bufsize, \
                maxsize; \
            if (!(len = len < chunk_size ? chunk_size : len)) { \
                errno = ENOMEM; \
                return NULL; \
            } \
            maxsize = (len / chunk_size) * chunk_size; \
            bufsize = maxsize * sizeof(TYPE); \
            if ((res = calloc(1, sizeof(NAME##_t) + bufsize))) { \
                res->bufsize = bufsize; \
                res->maxsize = maxsize; \
                res->datasize = sizeof(TYPE); \
                res->chunk_size = chunk_size; \
            } \
        } else \
        if (len > 0 && (res = calloc(1, sizeof(NAME##_t) + len * sizeof(TYPE)))) { \
            res->bufsize = len * sizeof(TYPE); \
            res->maxsize = len; \
            res->datasize = sizeof(TYPE); \
        } else { \
            errno = ENOMEM; \
            return NULL; \
        } \
        res->flags = flags; \
        return res; \
    }; \
    NAME##_t *copy_##NAME (NAME##_t *src) { \
        NAME##_t *dst = create_##NAME(src->len, src->chunk_size, src->flags); \
        dst->on_copy = src->on_copy; \
        dst->on_free = src->on_free; \
        for (int i = 0; i < src->len; ++i) { \
            TYPE tmp = src->on_copy(&src->ptr[i]); \
            add_##NAME(&dst, &tmp); \
        } \
        return dst; \
    } \
    void clear_##NAME (NAME##_t **array) { \
        NAME##_t *a = *array; \
        if (a) { \
            if (a->on_free) \
                for (int i = 0; i < a->len; ++i) \
                    a->on_free(&a->ptr[i]); \
            memset(a, 0, sizeof(NAME##_t) + a->bufsize); \
            *array = NULL; \
        } \
    } \
    void free_##NAME (NAME##_t **array) { \
        NAME##_t *a = *array; \
        if (a) { \
            if (a->on_free) \
                for (int i = 0; i < a->len; ++i) \
                    a->on_free(&a->ptr[i]); \
            free(a); \
            *array = NULL; \
        } \
    } \
    TYPE *next_##NAME (NAME##_t *array, TYPE *data) { \
        TYPE *end = array->ptr + array->len * sizeof(TYPE), \
             *next = array->ptr + ((uintptr_t)data - (uintptr_t)array) + sizeof(TYPE); \
        if (next >= end) return NULL; \
        return next; \
    } \
    TYPE *prev_##NAME (NAME##_t *array, TYPE *data) { \
        TYPE *prev = array->ptr + ((uintptr_t)data - (uintptr_t)array) - sizeof(TYPE); \
        if (prev < array->ptr) return NULL; \
        return prev; \
    } \
    int foreach_##NAME (NAME##_t *array, int(*fn)(TYPE*,void*), void *userdata, int flags) { \
        if (fn) \
            for (int i = 0; i < array->len; ++i) \
                fn(&array->ptr[i], userdata); \
        return 0; \
    } \
    TYPE *find_##NAME (NAME##_t *array, int(*fn)(TYPE*,TYPE*), TYPE *data) { \
        if (fn) \
            for (int i = 0; i < array->len; ++i) \
                if (CF_FOUND == fn(&array->ptr[i], data)) \
                    return &array->ptr[i]; \
        return NULL; \
    } \
    TYPE *add_##NAME (NAME##_t **array, TYPE *val) { \
        NAME##_t *a = *array; \
        TYPE *item = NULL; \
        if (a->chunk_size > 0) { \
            if (a->len == a->maxsize) { \
                int maxsize = a->maxsize + a->chunk_size, \
                    bufsize = maxsize * sizeof(TYPE); \
                a = realloc(*array, sizeof(NAME##_t) + bufsize); \
                if (!a) return NULL; \
                *array = a; \
                a->maxsize = maxsize; \
                a->bufsize = bufsize; \
            } \
        } else \
        if (a->len == a->maxsize) { \
            errno = ERANGE; \
            return NULL; \
        } \
        if (val) \
            a->ptr[a->len] = *val; \
        item = (TYPE*)((void*)(&a->ptr[a->len]) - (uintptr_t)(void*)a); \
        a->len++; \
        return item; \
    } \
    void del_##NAME (NAME##_t **array, TYPE *item) { \
        NAME##_t *a = *array; \
        void *x = (void*)a + (uintptr_t)item, \
             *e = (void*)a->ptr + a->bufsize, \
             *n = x + sizeof(TYPE); \
        if (a->on_free) \
            a->on_free(x); \
        if (n < e) \
            memmove(x, n, (intptr_t)e - (intptr_t)n); \
        --a->len; \
        if ((CF_REDUCE & a->flags) && a->chunk_size > 0 && a->maxsize > a->chunk_size && a->maxsize - a->len > a->chunk_size) { \
            int maxsize = a->maxsize - a->chunk_size, \
                bufsize = maxsize * sizeof(TYPE); \
            a = realloc(*array, sizeof(NAME##_t) + bufsize); \
            a->maxsize = maxsize; \
            a->bufsize = bufsize; \
            *array = a; \
        } \
    }

#define DECLARE_SORTED_ARRAY(NAME,TYPE) \
    typedef struct { \
        int len; \
        unsigned int flags; \
        int maxsize; \
        int bufsize; \
        int datasize; \
        int chunk_size; \
        void(*on_free)(TYPE*); \
        int(*on_compare)(TYPE*,TYPE*); \
        TYPE(*on_copy)(TYPE*); \
        TYPE ptr [0]; \
    } NAME##_t; \
    extern NAME##_t *create_##NAME (int len, int chunk_size, unsigned int flags); \
    extern NAME##_t *copy_##NAME (NAME##_t *src); \
    extern void clear_##NAME (NAME##_t **array); \
    extern void free_##NAME (NAME##_t **array); \
    extern int foreach_##NAME (NAME##_t *array, int(*fn)(TYPE*,void*), void *userdata, int flags); \
    extern TYPE *find_##NAME (NAME##_t *array, int(*fn)(TYPE*,TYPE*), TYPE *data); \
    extern TYPE *add_##NAME (NAME##_t **array, TYPE *val); \
    extern void del_##NAME (NAME##_t **array, TYPE *item);

#define DECLARE_SORTED_ARRAY_FUN(NAME,TYPE) \
    NAME##_t *create_##NAME (int len, int chunk_size, unsigned int flags) { \
        NAME##_t *res; \
        if (chunk_size > 0) { \
            int bufsize, \
                maxsize; \
            if ((len = len < chunk_size ? chunk_size : len) <= 0) { \
                errno = ERANGE; \
                return NULL; \
            } \
            maxsize = (len / chunk_size) * chunk_size; \
            bufsize = maxsize * sizeof(TYPE); \
            if ((res = calloc(1, sizeof(NAME##_t) + bufsize))) { \
                res->bufsize = bufsize; \
                res->maxsize = maxsize; \
                res->datasize = sizeof(TYPE); \
                res->chunk_size = chunk_size; \
            } \
        } else \
        if (len <= 0) { \
            errno = ERANGE; \
            return NULL; \
        } else \
        if ((res = calloc(1, sizeof(NAME##_t) * len * sizeof(TYPE)))) { \
            res->maxsize = len; \
            res->datasize = sizeof(TYPE); \
        } else \
            return NULL; \
        res->flags = flags; \
        return res; \
    }; \
    NAME##_t *copy_##NAME (NAME##_t *src) { \
        NAME##_t *dst = create_##NAME(src->len, src->chunk_size, src->flags); \
        dst->on_copy = src->on_copy; \
        dst->on_free = src->on_free; \
        dst->on_compare = src->on_compare; \
        for (int i = 0; i < src->len; ++i) { \
            TYPE tmp = src->on_copy(&src->ptr[i]); \
            add_##NAME(&dst, &tmp); \
        } \
        return dst; \
    } \
    void clear_##NAME (NAME##_t **array) { \
        NAME##_t *a = *array; \
        if (a) { \
            if (a->on_free) \
                for (int i = 0; i < a->len; ++i) \
                    a->on_free(&a->ptr[i]); \
            memset(a, 0, sizeof(NAME##_t) + a->bufsize); \
            *array = NULL; \
        } \
    } \
    void free_##NAME (NAME##_t **array) { \
        NAME##_t *a = *array; \
        if (a) { \
            if (a->on_free) \
                for (int i = 0; i < a->len; ++i) \
                    a->on_free(&a->ptr[i]); \
            free(a); \
            *array = NULL; \
        } \
    } \
    int foreach_##NAME (NAME##_t *array, int(*fn)(TYPE*,void*), void *userdata, int flags) { \
        if (fn) \
            for (int i = 0; i < array->len; ++i) \
                fn(&array->ptr[i], userdata); \
        return 0; \
    } \
    TYPE *find_##NAME (NAME##_t *array, int(*fn)(TYPE*,TYPE*), TYPE *data) { \
        int is_found = -1; \
        long long int l = 0, r = array->len - 1, found_idx; \
        if (!fn) fn = array->on_compare; \
        while (l <= r) { \
            found_idx = (l + r) / 2LL; \
            is_found = array->on_compare(data, &array->ptr[found_idx]); \
            if (is_found > 0) l = found_idx + 1; \
            else if (is_found < 0) r = found_idx - 1; \
            else \
                return &array->ptr[found_idx]; \
        } \
        return NULL; \
    } \
    TYPE *add_##NAME (NAME##_t **array, TYPE *val) { \
        NAME##_t *a = *array; \
        TYPE *item = NULL; \
        long long int found_idx = 0; \
        int l = 0, r = a->len - 1; \
        int is_found = 0; \
        if (a->chunk_size <= 0 && a->maxsize == a->len) { \
            errno = ERANGE; \
            return NULL; \
        } \
        if (!a->on_compare) { \
            errno = EFAULT; \
            return NULL; \
        } \
        errno = 0; \
        while (l <= r) { \
            found_idx = (l + r) / 2; \
            is_found = a->on_compare(val, &a->ptr[found_idx]); \
            if (is_found > 0) l = found_idx + 1; \
            else if (is_found < 0) r = found_idx - 1; \
            else { \
                errno = EEXIST; \
                if ((a->flags & CF_UNIQUE)) \
                    return 0; \
                break; \
            } \
        } \
        if (a->len > 0 && is_found > 0) ++found_idx; \
        if (found_idx < 0) found_idx = 0; \
        if (a->maxsize == a->len) { \
            int maxsize, bufsize; \
            if (a->chunk_size <= 0) { \
                errno = ERANGE; \
                return NULL; \
            } \
            maxsize = a->maxsize + a->chunk_size; \
            bufsize = maxsize * sizeof(TYPE); \
            if (!(a = realloc(*array, sizeof(NAME##_t) + bufsize))) \
                return 0; \
            a->maxsize = maxsize; \
            a->bufsize = bufsize; \
            *array = a; \
        } \
        if (found_idx < a->len) { \
            memmove((void*)a->ptr + (found_idx + 1) * sizeof(TYPE), \
                    (void*)a->ptr + found_idx * sizeof(TYPE), \
                    (a->len - found_idx) * sizeof(TYPE)); \
            a->ptr[found_idx] = *val; \
            item = (TYPE*)((void*)&a->ptr[found_idx] - (uintptr_t)(void*)a); \
        } else { \
            a->ptr[a->len] = *val; \
            item = (TYPE*)((void*)&a->ptr[a->len] - (uintptr_t)(void*)a); \
        } \
        ++a->len; \
        return item; \
    } \
    void del_##NAME (NAME##_t **array, TYPE *item) { \
        NAME##_t *a = *array; \
        void *x = (void*)a + (uintptr_t)item, \
             *e = (void*)a->ptr + a->bufsize, \
             *n = x + sizeof(TYPE); \
        if (a->on_free) \
            a->on_free(x); \
        if (n < e) \
            memmove(x, n, (intptr_t)e - (intptr_t)n); \
        --a->len; \
        if ((CF_REDUCE & a->flags) && a->chunk_size > 0 && \
            a->maxsize > a->chunk_size && a->maxsize - a->len > a->chunk_size) { \
            int maxsize = a->maxsize - a->chunk_size, \
                bufsize = maxsize * sizeof(TYPE); \
            a = realloc(*array, sizeof(NAME##_t) + bufsize); \
            a->maxsize = maxsize; \
            a->bufsize = bufsize; \
            *array = a; \
        } \
    }

#endif // __LIBEX_ARRAY_H__
