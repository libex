/*Copyright (c) Brian B.

Permission to use, copy, modify, and/or distribute this software for any purpose
with or without fee is hereby granted.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*/
#ifndef __LIBEX_HTML_H__
#define __LIBEX_HTML_H__

#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include "str.h"
#include "list.h"

#define HTML_NOT_FOUND -1
#define HTML_FOUND 0
#define HTML_FOUND_FIN 1

typedef struct html_tag html_tag_t;
struct html_tag {
    str_t *tag_s;
    str_t *tag_t;
    str_t *tag_e;
    void *tags;
    struct html_tag *parent;
};
DECLARE_LIST(html_tags,html_tag_t)

typedef struct {
    html_tag_t *head;
} html_t;

void html_clear (html_t *html);
void html_clear_tags (html_tags_t *tags);
int html_parse (char *content, size_t content_length, html_t *html);
int html_add_tag (html_tag_t *parent,
                  const char *tag_s, size_t tag_s_len,
                  const char *tag_t, size_t tag_t_len,
                  const char *tag_e, size_t tag_e_len,
                  int add_to_head);
int html_get_tag_attr (str_t *tag, int idx, strptr_t *val);
str_t *html_mkcontent (html_t *html, size_t start_len, size_t chunk_size);

#endif // __LIBEX_HTML_H__
