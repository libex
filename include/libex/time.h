/*Copyright (c) Brian B.
  Copyright (c) PostgreSQL Global Development Group

Permission to use, copy, modify, and/or distribute this software for any purpose
with or without fee is hereby granted.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE
OF THIS SOFTWARE.
*/
#ifndef __LIBEX_TIME_H__
#define __LIBEX_TIME_H__

#include <stdlib.h>
#include <time.h>
#include <limits.h>
#include <errno.h>
#include <string.h>
#include <stdio.h>

typedef long long int tm_t;
typedef struct tm_diff {
    tm_t time;
    long mon;
} tmdiff_t;

#define TM_ARG(Y,M,D,HH,MM,SS) \
    &(struct tm){ .tm_year = Y, .tm_mon = M, .tm_mday = D, .tm_hour = HH, .tm_min = MM, .tm_sec = SS }
#define DATETIME_ARG(DATE,TIME) \
    &(struct tm){ .tm_year = DATE/10000, .tm_mon = DATE%10000/100, .tm_mday = DATE%10000%100, \
                  .tm_hour = TIME/10000, .tm_min = TIME%10000/100, .tm_sec = TIME%10000%100 }
#define DATE_RESULT(TM) \
    (TM)->tm_year*10000 + (TM)->tm_mon*100 + (TM)->tm_mday
#define TIME_RESULT(TM) \
    (TM)->tm_hour*10000 + (TM)->tm_min*100 + (TM)->tm_sec

const char *tm_moname (int mon);

tm_t tm_enc (struct tm *tm);
struct tm *tm_dec (tm_t t, struct tm *tm);
tm_t tm_add (tm_t t, tmdiff_t *dif);
tm_t tm_sub (tm_t t, tmdiff_t *dif);
tmdiff_t tm_encdiff (struct tm *tm);
struct tm *tm_decdiff (tmdiff_t dif, struct tm *tm);
tmdiff_t tm_diff (tm_t begin, tm_t end);
tm_t tm_now (void);
const char *tm_moname (int mon);

#endif
