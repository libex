/*Copyright (c) Brian B.

Permission to use, copy, modify, and/or distribute this software for any purpose
with or without fee is hereby granted.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF
THIS SOFTWARE.
*/
#ifndef __LIBEX_TREE_H__
#define __LIBEX_TREE_H__

#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include "list.h"

#define DECLARE_RBTREE(NAME,TYPE) \
    typedef struct NAME##_item NAME##_item_t; \
    typedef struct NAME##_struct NAME##_t; \
    struct NAME##_item { \
        NAME##_item_t *left, *right, *parent; \
        int red; \
        unsigned int rsv; \
        NAME##_t *tree; \
        TYPE ptr; \
    }; \
    struct NAME##_struct { \
        unsigned int len; \
        int (*on_compare)(const TYPE*,TYPE*); \
        void (*on_free)(TYPE*); \
        struct NAME##_item nil, root; \
    }; \
    extern NAME##_t *create_##NAME (void); \
    extern void clear_##NAME (NAME##_t *tree); \
    extern void free_##NAME (NAME##_t *tree); \
    extern int foreach_##NAME (NAME##_t *tree, int(*fn)(TYPE*,void*), void *userdata, int flags); \
    extern TYPE *find_##NAME (NAME##_t *tree, TYPE *data); \
    extern TYPE *add_##NAME (NAME##_t *tree, TYPE *val); \
    extern void del_##NAME (TYPE *data);

#define DECLARE_RBTREE_FUN(NAME,TYPE) \
    NAME##_t *create_##NAME (void) { \
        NAME##_t *tree; \
        NAME##_item_t *tmp; \
        tree = calloc(1, sizeof(NAME##_t)); \
        tmp = &tree->nil; \
        tmp->parent = tmp->left = tmp->right = tmp; \
        tmp = &tree->root; \
        tmp->parent = tmp->left = tmp->right = &tree->nil; \
        return tree; \
    } \
    void clear_##NAME (NAME##_t *tree) { \
        NAME##_item_t *nil, *tmp; \
        void fh (NAME##_item_t *x) { \
            if (x != nil) { \
                fh(x->left); \
                fh(x->right); \
                free(x); \
            } \
        } \
        void fhf (NAME##_item_t* x) { \
            if (x != nil) { \
                fhf(x->left); \
                fhf(x->right); \
                tree->on_free(&x->ptr); \
                free(x); \
            } \
        } \
        nil = &tree->nil; \
        if (tree->on_free) \
            fhf(tree->root.left); \
        else \
            fh(tree->root.left); \
        tmp = &tree->nil; \
        tmp->parent = tmp->left = tmp->right = tmp; \
        tmp->red = 0; \
        memset(&tmp->ptr, 0, sizeof(TYPE)); \
        tmp = &tree->root; \
        tmp->parent = tmp->left = tmp->right = &tree->nil; \
        memset(&tmp->ptr, 0, sizeof(TYPE)); \
        tmp->red = 0; \
    } \
    void free_##NAME (NAME##_t *tree) { \
        clear_##NAME(tree); \
        free(tree); \
    } \
    int foreach_##NAME (NAME##_t *tree, int(*fn)(TYPE*,void*), void *userdata, int flags) { \
        NAME##_item_t *nil = &tree->nil, *root = &tree->root; \
        int enum_nodes (NAME##_item_t *x) { \
            if (x->left != nil) { \
                if (ENUM_BREAK == enum_nodes(x->left) && ENUM_STOP_IF_BREAK == flags) \
                    return ENUM_BREAK; \
            } \
            if (ENUM_BREAK == fn(&x->ptr, userdata) && ENUM_STOP_IF_BREAK == flags) \
                return ENUM_BREAK; \
            if (x->right != nil) \
                return enum_nodes(x->right); \
            return ENUM_CONTINUE; \
        } \
        if (nil != root && root->left != nil) \
            return enum_nodes(root->left); \
        return ENUM_CONTINUE; \
    } \
    TYPE *find_##NAME (NAME##_t *tree, TYPE *data) { \
        if (!tree->len) return NULL; \
        NAME##_item_t *root = &tree->root, *x = root->left, *nil = &tree->nil; \
        int n = tree->on_compare((const TYPE*)&x->ptr, data); \
        while (n) { \
            if (0 < n) \
                x = x->left; \
            else \
                x = x->right; \
            if (x == nil) return NULL; \
            n = tree->on_compare((const TYPE*)&x->ptr, data); \
        } \
        return &x->ptr; \
    } \
    static void NAME##_left_rotate (NAME##_t *tree, NAME##_item_t *x) { \
        NAME##_item_t *y = x->right, *nil = &tree->nil; \
        x->right = y->left; \
        if (y->left != nil) \
            y->left->parent = x; \
        y->parent = x->parent; \
        if (x == x->parent->left) \
            x->parent->left = y; \
        else \
            x->parent->right = y; \
        y->left = x; \
        x->parent = y; \
    } \
    static void NAME##_right_rotate (NAME##_t *tree, NAME##_item_t *y) { \
        NAME##_item_t *x = y->left, *nil = &tree->nil; \
        y->left = x->right; \
        if (x->right != nil) x->right->parent = y; \
        x->parent = y->parent; \
        if (y == y->parent->left) \
            y->parent->left = x; \
        else \
            y->parent->right = x; \
        x->right = y; \
        y->parent = x; \
    } \
    static NAME##_item_t *NAME##_insert_helper (NAME##_t *tree, TYPE *val) { \
        NAME##_item_t *y = &tree->root, *x = y->left, *nil = &tree->nil, *z; \
        errno = 0; \
        while (x != nil) { \
            y = x; \
            int r = tree->on_compare((const TYPE*)&x->ptr, val); \
            errno = 0; \
            if (r > 0) \
                x = x->left; \
            else if (r < 0) \
                x = x->right; \
            else { \
                errno = EEXIST; \
                return x; \
            } \
        } \
        z = calloc(1, sizeof(NAME##_item_t)); \
        z->left = z->right = nil; \
        z->parent = y; \
        z->red = 1; \
        z->tree = tree; \
        if ((y == &tree->root) || (0 < tree->on_compare((const TYPE*)&y->ptr, val))) \
            y->left = z; \
        else \
            y->right = z; \
        memcpy(&z->ptr, val, sizeof(TYPE)); \
        return z; \
    } \
    TYPE *add_##NAME (NAME##_t *tree, TYPE *val) { \
        NAME##_item_t *x = NAME##_insert_helper(tree, val), *y, *new_node; \
        if (!x || EEXIST == errno) \
            return &x->ptr; \
        new_node = x; \
        x->red = 1; \
        while(x->parent->red) { \
            if (x->parent == x->parent->parent->left) { \
                y = x->parent->parent->right; \
                if (y->red) { \
                    x->parent->red = 0; \
                    y->red = 0; \
                    x->parent->parent->red = 1; \
                    x = x->parent->parent; \
                } else { \
                    if (x == x->parent->right) { \
                        x = x->parent; \
                        NAME##_left_rotate(tree, x); \
                    } \
                    x->parent->red = 0; \
                    x->parent->parent->red = 1; \
                    NAME##_right_rotate(tree, x->parent->parent); \
                } \
            } else { \
                y = x->parent->parent->left; \
                if (y->red) { \
                    x->parent->red = 0; \
                    y->red=0; \
                    x->parent->parent->red = 1; \
                    x = x->parent->parent; \
                } else { \
                    if (x == x->parent->left) { \
                        x = x->parent; \
                        NAME##_right_rotate(tree,x); \
                    } \
                    x->parent->red = 0; \
                    x->parent->parent->red = 1; \
                    NAME##_left_rotate(tree, x->parent->parent); \
                } \
            } \
        } \
        tree->root.left->red = 0; \
        ++tree->len; \
        x->tree = tree; \
        return &new_node->ptr; \
    } \
    static void NAME##_del_fixup (NAME##_t *tree, NAME##_item_t *x) { \
        NAME##_item_t *root = &tree->root, *w; \
        root = root->left; \
        while((!x->red) && (root != x)) { \
            if (x == x->parent->left) { \
                w = x->parent->right; \
                if (w->red) { \
                    w->red = 0; \
                    x->parent->red=1; \
                    NAME##_left_rotate(tree, x->parent); \
                    w = x->parent->right; \
                } \
                if ((!w->right->red) && (!w->left->red)) {  \
                    w->red = 1; \
                    x = x->parent; \
                } else { \
                    if (!w->right->red) { \
                        w->left->red = 0; \
                        w->red = 1; \
                        NAME##_right_rotate(tree, w); \
                        w = x->parent->right; \
                    } \
                    w->red = x->parent->red; \
                    x->parent->red = 0; \
                    w->right->red = 0; \
                    NAME##_left_rotate(tree, x->parent); \
                    x = root; \
                } \
            } else { \
                w = x->parent->left; \
                if (w->red) { \
                    w->red = 0; \
                    x->parent->red = 1; \
                    NAME##_right_rotate(tree, x->parent); \
                    w = x->parent->left; \
                } \
                if ((!w->right->red) && (!w->left->red)) {  \
                    w->red = 1; \
                    x = x->parent; \
                } else { \
                    if (!w->left->red) { \
                        w->right->red = 0; \
                        w->red = 1; \
                        NAME##_left_rotate(tree, w); \
                        w = x->parent->left; \
                    } \
                    w->red = x->parent->red; \
                    x->parent->red = 0; \
                    w->left->red = 0; \
                    NAME##_right_rotate(tree, x->parent); \
                    x = root; \
                } \
            } \
        } \
        x->red = 0; \
    } \
    static NAME##_item_t *NAME##_succ (NAME##_t *tree, NAME##_item_t *x) { \
        NAME##_item_t *y, *nil = &tree->nil, *root = &tree->root; \
        if (nil != (y = x->right)) { \
            while (y->left != nil) y=y->left; \
            return y; \
        } else { \
            y = x->parent; \
            while(x == y->right) { \
                x = y; \
                y = y->parent; \
            } \
            if (y == root) return(nil); \
            return y; \
        } \
    } \
    void del_##NAME (TYPE *data) { \
        NAME##_item_t *z = (NAME##_item_t*)((uintptr_t)data - sizeof(void*)*4 - sizeof(int)*2), \
                      *x, *y, *nil, *root; \
        NAME##_t *t = z->tree; \
        nil = &t->nil; \
        root = &t->root; \
        y = ((z->left == nil) || (z->right == nil)) ? z : NAME##_succ(t, z); \
        x = (y->left == nil) ? y->right : y->left; \
        if (root == (x->parent = y->parent)) \
            root->left = x; \
        else { \
            if (y == y->parent->left) \
                y->parent->left = x; \
            else \
                y->parent->right = x; \
        } \
        if (y != z) { \
            if (!(y->red)) NAME##_del_fixup(t, x); \
            y->left = z->left; \
            y->right = z->right; \
            y->parent = z->parent; \
            y->red = z->red; \
            z->left->parent = z->right->parent = y; \
            if (z == z->parent->left) \
                z->parent->left = y;  \
            else \
                z->parent->right = y; \
            if (t->on_free) \
                t->on_free(data); \
            free(z); \
            --t->len; \
        } else { \
            if (!(y->red)) NAME##_del_fixup(t, x); \
            if (t->on_free) \
                t->on_free(&y->ptr); \
            free(y); \
            --t->len; \
        } \
    }

#endif //  __LIBEX_TREE_H__
