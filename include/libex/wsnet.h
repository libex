/*Copyright (c) Brian B.

Permission to use, copy, modify, and/or distribute this software for any purpose
with or without fee is hereby granted.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*/
#ifndef __LIBEX_WSNET_H__
#define __LIBEX_WSNET_H__

#include <sys/time.h>
#include "list.h"
#include "net.h"
#include "ws.h"
#include "rpc.h"
#include "json.h"

#define WST_PING_LEN 7
#define WST_PONG_LEN 11
#define WST_CLOSE_LEN 6

#ifdef USE_SSL
#define FL_WSLOADEDHDR 0x00000002
#define FL_WSLOADEDLEN 0x00000004
#define FL_WSLOADEDMASK 0x00000008
#define FL_WSLOADED 0x00000010
#endif

extern char wst_ping [];
extern char wst_pong [];
extern char wst_close [];

void wsnet_save_tail (netbuf_t *nbuf, ssize_t nbytes);

int ws_netbuf_alloc (netbuf_t *nbuf, size_t start_len, size_t chunk_size);
void ws_netbuf_free (netbuf_t *nbuf);

str_t *create_ws_request (http_url_t *h_url);
int ws_ev_connect (int fd, net_srv_t *srv, uint32_t *flags, http_url_t *h_url);
int wsnet_handshake (int fd, strbuf_t *buf, strptr_t *url);
int wsnet_recv (int fd, netbuf_t *nbuf, ws_t *ws);
int wsnet_parse (netbuf_t *nbuf, ws_t *ws);
void wsnet_reset (netbuf_t *nbuf);

int check_api_ver (int fd, net_srv_t *srv, strptr_t *url);
int ev_getver (net_srv_t *srv, ev_fd_t fd);

net_srv_t *ws_srv_init (const char *svc);
void ws_srv_start (net_srv_t *srv);
int ws_ev_disconnect (net_srv_t *srv, ev_fd_t fd);
void ws_srv_done (net_srv_t *srv);
int ws_ev_send (net_srv_t *srv, ev_fd_t fd, uint32_t flags, const char *buf, size_t len, uint32_t ws_flags, uint8_t ws_opcode);
int ws_ev_recvd (net_srv_t *srv, ev_buf_t *ev);
void *ws_ping_cb (void *param);
int ws_evbuf_set (ev_buf_t *n_ev, net_ev_t *ev);
int ws_close_ev (int fd, net_srv_t *srv);
#define ws_ev_close_send(srv,fd,flags) net_ev_send(srv,fd,flags,wst_close,WST_CLOSE_LEN)

/*typedef struct ev_locker ev_locker_t;

struct ev_locker {
    int64_t id_rpc;
    pthread_mutex_t *locker;
    pthread_cond_t *cond;
    pthread_condattr_t *condattr;
    rbtree_item_t *ti;
    strbuf_t buf;
    ws_t ws;
};*/

net_srv_t *ws_rpc_srv_init (const char *svc);
//rbtree_t *ws_create_msg_info (void);
msginfo_t *ws_create_msg_info (void);
void ws_rpc_evbuf_free (ev_buf_t *n_ev);
int ws_rpc_srv_event (void *dummy, pool_msg_t *msg);
int ws_rpc_ev_send (net_srv_t *srv,
                    ev_fd_t id,
                    uint32_t flags,
                    const char *buf, size_t len,
                    uint32_t ws_flags, uint8_t ws_opcode,
                    int64_t *id_rpc,
                    ws_t *ws,
                    strbuf_t *res);
int ws_rpc_request_send (net_srv_t *srv,
                         ev_fd_t id,
                         uint32_t method,
                         uint32_t flags,
                         strbuf_t *buf,
                         rpc_h cb,
                         rpc_t *rpc,
                         void *userdata);
int ws_rpc_response_send (net_srv_t *srv,
                          ev_fd_t id,
                          strbuf_t *buf,
                          intptr_t id_rpc,
                          int id_rpc_len,
                          rpc_h cb,
                          void *userdata);
int ws_rpc_error_send (net_srv_t *srv,
                       ev_fd_t id,
                       strbuf_t *buf,
                       int code,
                       intptr_t id_rpc, int id_rpc_len);

typedef struct ws_jsonrpc_buf ws_jsonrpc_buf_t;

net_srv_t *ws_jsonrpc_srv_init (const char *svc);
json_t *ws_jsonrpc_request_send (net_srv_t *srv,
                                 ev_fd_t id,
                                 const char *method,
                                 size_t method_len,
                                 uint32_t flags,
                                 strbuf_t *buf,
                                 jsonrpc_h cb,
                                 void *userdata);
int ws_jsonrpc_response_send (net_srv_t *srv,
                              ev_fd_t id,
                              strbuf_t *buf,
                              intptr_t id_rpc, int id_rpc_len,
                              jsonrpc_h cb,
                              void *userdata);
int ws_jsonrpc_error_send (net_srv_t *srv,
                           ev_fd_t id,
                           strbuf_t *buf,
                           int code,
                           const char *msg, size_t lmsg,
                           intptr_t id_rpc, int id_rpc_len);
int ws_jsonrpc_stderror_send (net_srv_t *srv,
                              ev_fd_t id,
                              strbuf_t *buf,
                              int code,
                              intptr_t id_rpc, int id_rpc_len);

#define WS_EV_OFFSET(ev) (uintptr_t)ev->data.ws.ptr - (uintptr_t)ev->buf.ptr

void ws_jsonrpc_evbuf_free (ev_buf_t *n_ev);
int ws_jsonrpc_srv_event (void *dummy, pool_msg_t *msg);

#ifdef USE_SSL
net_srv_t *sslws_srv_init (const char *svc, const char *cert_file, const char *key_file);
void sslws_srv_done (net_srv_t *srv);
net_srv_t *sslws_jsonrpc_srv_init (const char *svc, const char *cert_file, const char *key_file);
void sslws_jsonrpc_srv_done (net_srv_t *srv);
#endif

#endif // __LIBEX_WSNET_H__
