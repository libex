#!/bin/sh

VER=`cat libex.pc.in | grep Version | awk '{print $2}'`
DIR=libex-$VER
(rm libex-$VER.tar.bz2 2>/dev/null || true)
mkdir -p $DIR $DIR/include/libex $DIR/include/libex/poll $DIR/src $DIR/src/epoll $DIR/src/pollset $DIR/test
cp -r include/libex/* $DIR/include/libex
cp -r src/* $DIR/src
cp test/* $DIR/test
cp Jamfile $DIR/
cp Jamrules.configure $DIR/
cp LICENSE $DIR/
cp README.md $DIR/
cp libex.pc.in $DIR/
tar cvf libex-$VER.tar $DIR/
bzip2 libex-$VER.tar
rm -fr $DIR/