/*Copyright (c) Brian B.

Permission to use, copy, modify, and/or distribute this software for any purpose
with or without fee is hereby granted.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*/
#include "../include/libex/str.h"

str_t *str_url_decode (const char *str, size_t str_len, size_t chunk_size) {
    str_t *ret = stralloc(str_len * 1.5, chunk_size);
    char num [] = "0x0__";
    for (size_t i = 0; i < str_len; ++i) {
        if (str[i] == '%') {
            char c, *tail;
            num[3] = str[++i];
            num[4] = str[++i];
            c = strtol(num, &tail, 16);
            if (*tail || errno == ERANGE) goto err;
            if (-1 == strnadd(&ret, &c, sizeof(char))) goto err;
        } else {
            if (-1 == strnadd(&ret, &str[i], sizeof(char))) goto err;
        }
    }
    return ret;
    err:
    free(ret);
    return NULL;
}
