/*Copyright (c) Brian B.

Permission to use, copy, modify, and/or distribute this software for any purpose
with or without fee is hereby granted.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*/
#include "../include/libex/net.h"

DECLARE_ARRAY_FUN(ev_array,ev_fd_t)
DECLARE_LIST_FUN(ev_list,ev_fd_t)
DECLARE_LIST_FUN(recon_list,net_recon_t)
DECLARE_RBTREE_FUN(msginfo, ev_locker_t*)
DECLARE_LIST_FUN(net_ev_list,net_ev_t)

#ifdef USE_SSL
static __thread str_t *errmsg = NULL;
__thread int ssl_errno = 0;

__attribute__ ((destructor))
static void sslw_free_err () {
    if (errmsg)
        free(errmsg);
}

static int adderr (const char *str, size_t len, void *userdata) {
    if (errmsg)
        strnadd(&errmsg, str, len);
    else
        errmsg = mkstr(str, len, 64);
    return 0;
}

cstr_t *ssl_error () {
    if (errmsg)
        return mkcstr(errmsg->ptr, errmsg->len);
    return NULL;
}

SSL_CTX *ssl_create (int kind) {
    const SSL_METHOD *m;
    switch (kind) {
        case SSL_CLIENT:
            m = SSLv23_client_method();
            break;
        case SSL_SERVER:
            m = SSLv23_server_method();
            break;
        default:
            m = SSLv23_method();
            break;
    }
    SSL_CTX *ctx = SSL_CTX_new(m);
    if (!ctx) {
        if (errmsg) {
            free(errmsg);
            errmsg = NULL;
        }
        ERR_print_errors_cb(adderr, NULL);
    }
    return ctx;
}

int ssl_config (SSL_CTX *ctx, const char *cert_file, const char *key_file) {
    int rc;
    SSL_CTX_set_ecdh_auto(ctx, 1);
    if ((rc = SSL_CTX_use_certificate_chain_file(ctx, cert_file)) > 0)
        rc = SSL_CTX_use_PrivateKey_file(ctx, key_file, SSL_FILETYPE_PEM);
    if (rc <= 0) {
        if (errmsg) {
            free(errmsg);
            errmsg = NULL;
        }
        ERR_print_errors_cb(adderr, NULL);
    }
    return rc > 0 ? 0 : -1;
}

int ssl_accept (SSL_CTX *ctx, int fd, SSL **ssl) {
    int rc;
    if (!*ssl) {
        *ssl = SSL_new(ctx);
        SSL_set_fd(*ssl, fd);
    } else
    if (SSL_is_init_finished(*ssl))
        return NET_WAIT;
    rc = SSL_accept(*ssl);
    if (1 == rc)
        return 1;
    switch (SSL_get_error(*ssl, rc)) {
        case SSL_ERROR_NONE:
            return 1;
        case SSL_ERROR_WANT_WRITE:
        case SSL_ERROR_WANT_READ:
            return 0;
        default:
            if (errmsg) {
                free(errmsg);
                errmsg = NULL;
            }
            ERR_print_errors_cb(adderr, NULL);
            SSL_shutdown(*ssl);
            SSL_free(*ssl);
            *ssl = NULL;
    }
    return -1;
}

int ssl_connect (SSL_CTX *ctx, int fd, SSL **ssl) {
    int rc;
    if (!*ssl) {
        *ssl = SSL_new(ctx);
        SSL_set_fd(*ssl, fd);
    }
    if (SSL_is_init_finished(*ssl))
        return 0;
    rc = SSL_connect(*ssl);
    if (1 == rc)
        return 1;
    switch (SSL_get_error(*ssl, rc)) {
        case SSL_ERROR_NONE:
            return 1;
        case SSL_ERROR_WANT_WRITE:
        case SSL_ERROR_WANT_READ:
            return 0;
        default:
            if (errmsg) {
                free(errmsg);
                errmsg = NULL;
            }
            ERR_print_errors_cb(adderr, NULL);
            SSL_shutdown(*ssl);
            SSL_free(*ssl);
            *ssl = NULL;
    }
    return -1;
}

ssize_t ssl_recv (SSL *ssl, strbuf_t *buf) {
    ssize_t readed = -1;
    int rc;
    ssl_errno = 0;
    if (-1 == strbufsize(buf, buf->len + buf->chunk_size, STR_KEEPLEN | STR_REDUCE))
        return -1;
    rc = SSL_read_ex(ssl, buf->ptr + buf->len, buf->chunk_size, (size_t*)&readed);
    if (rc > 0) {
        buf->len += readed;
        return readed;
    }
    switch ((ssl_errno = SSL_get_error(ssl, rc))) {
        case SSL_ERROR_WANT_READ:
        case SSL_ERROR_NONE:
            if (0 == rc)
                ssl_errno = 0;
            return 0;
        case SSL_ERROR_ZERO_RETURN:
        case SSL_ERROR_SYSCALL:
            SSL_shutdown(ssl);
            return -1;
        default:
            if (errmsg) {
                free(errmsg);
                errmsg = NULL;
            }
            ERR_print_errors_cb(adderr, NULL);
            break;
    }
    return -1;
}

ssize_t ssl_send (SSL *ssl, const void *buf, size_t size) {
    ssize_t sent = 0, wrote = 0;
    int rc;
    ssl_errno = 0;
    while (sent < size) {
        rc = SSL_write_ex(ssl, buf, size - sent, (size_t*)&wrote);
        if (rc > 0) {
            sent += wrote;
            buf += wrote;
            continue;
        }
        if (0 == rc)
            continue;
        if (errmsg) {
            free(errmsg);
            errmsg = NULL;
        }
        ssl_errno = SSL_get_error(ssl, rc);
        if (SSL_ERROR_WANT_WRITE == ssl_errno)
            continue;
        if (SSL_ERROR_ZERO_RETURN == ssl_errno)
            SSL_shutdown(ssl);
        ERR_print_errors_cb(adderr, NULL);
        sent = -1;
    }
    return sent;
}
#endif

static inline ev_fd_t create_id (net_srv_t *srv, int fd) {
    fd_t res = { .key = { .fd = fd, .id = __atomic_inc_uint(&srv->id) } };
    return res.fd;
}

void ignore_pipe () {
    struct sigaction sig;
    sig.sa_handler = SIG_IGN;
    sig.sa_flags = SA_RESTART;
    sigemptyset(&sig.sa_mask);
    sigaction(SIGPIPE, &sig, NULL);
}

int atoport (const char *service, const char *proto) {
    struct servent *servent;
    int port;
    char *tail;
    if ((servent = getservbyname(service, proto)))
        return servent->s_port;
    port = strtol(service, &tail, 0);
    if ('\0' != *tail || port < 1 || port > 65535)
        return -1;
    return htons(port);
}

int atoaddr (const char *address, struct in_addr *addr) {
    int rc;
    struct addrinfo hints = { .ai_family = PF_UNSPEC, .ai_socktype = SOCK_STREAM, .ai_flags = AI_CANONNAME },
                    *res, *result;
    struct sockaddr_in *in_addr = NULL;
    if (-1 != (addr->s_addr = inet_addr(address)))
        return 0;
    if ((rc = getaddrinfo(address, NULL, &hints, &result)))
        return rc;
    res = result;
    while (res) {
        if (AF_INET == res->ai_family) {
            in_addr = (struct sockaddr_in*)res->ai_addr;
            break;
        }
        res = res->ai_next;
    }
    if (in_addr)
        memcpy(addr, &in_addr->sin_addr, sizeof(struct in_addr));
    else
        rc = -1;
    freeaddrinfo(result);
    return rc;
}

int net_bind (const char *svc) {
    struct sockaddr_in6 inaddr;
    int fd = socket(AF_INET6, SOCK_STREAM, 0);
    setsockopt(fd, SOL_SOCKET, SO_REUSEADDR, (intptr_t*)&(int){1}, sizeof(int));
    memset(&inaddr, 0, sizeof(inaddr));
    inaddr.sin6_family = AF_INET6;
    inaddr.sin6_port = atoport(svc, "tcp");
    inaddr.sin6_addr = in6addr_any;
    if (bind(fd, (struct sockaddr*)&inaddr, sizeof(inaddr)) < 0 ||
        listen(fd, SOMAXCONN) < 0) {
        close(fd);
        return -1;
    }
    return fd;
}

ssize_t net_recv (int fd, strbuf_t *buf) {
    int done = 0, rc = -1;
    ssize_t total = 0;
    errno = 0;
    while (!done) {
        while (1) {
            ssize_t readed;
            if (-1 == strbufsize(buf, buf->len + buf->chunk_size, 0)) {
                done = 1;
                break;
            }
            readed = recv(fd, buf->ptr + buf->len, buf->chunk_size, 0);
            if (-1 == readed) {
                if (EAGAIN == errno)
                    errno = 0;
                done = 1;
                rc = 1;
                break;
            }
            if (0 == readed) {
                long nbytes = -1;
                if (0 == ioctl(fd, FIONREAD, &nbytes) && nbytes > 0)
                    continue;
                rc = -1;
                done = 1;
                break;
            }
            buf->len += readed;
            total += readed;
        }
    }
    return rc > 0 ? total : rc;
}

ssize_t net_read (int fd, strbuf_t *buf) {
    ssize_t readed;
    if (-1 == strbufsize(buf, buf->len + buf->chunk_size, 0))
        return -1;
    if ((readed = recv(fd, buf->ptr + buf->len, buf->chunk_size, 0)) > 0)
        buf->len += readed;
    return readed;
}

ssize_t net_write (int fd, const void *buf, size_t size) {
    ssize_t sent = 0, wrote = 0;
    while (sent < size) {
        do
            wrote = send(fd, buf, size - sent, MSG_NOSIGNAL);
        while (wrote < 0 && errno == EINTR);
        if (wrote <= 0) {
            if (errno != EAGAIN) sent = -1;
            break;
        }
        sent += wrote;
        buf += wrote;
    }
    return sent;
}

int net_connect (char *to_addr, char *service, int timeout) {
    int port, sock, rc = -1, flags, err;
    struct in_addr addr;
    struct sockaddr_in in_addr;
    fd_set rd;
    memset(&addr, 0, sizeof(struct in_addr));
    if (-1 == (port = atoport(service, "tcp")) || (-1 == atoaddr(to_addr, &addr)))
        return -1;
    memset(&in_addr, 0, sizeof in_addr);
    in_addr.sin_family = AF_INET;
    in_addr.sin_port = port;
    in_addr.sin_addr.s_addr = addr.s_addr;
    sock = socket(AF_INET, SOCK_STREAM, 0);
    if (timeout > 0) {
        if (0 > (flags = fcntl(sock, F_GETFL, 0)) ||
            0 > fcntl(sock, F_SETFL, flags | O_NONBLOCK) ||
            ((rc = connect(sock, (struct sockaddr*)&in_addr, sizeof in_addr)) && EINPROGRESS != errno))
            goto err;
    } else
    if (0 == (rc = connect(sock, (struct sockaddr*)&in_addr, sizeof in_addr))) {
        if (0 > (flags = fcntl(sock, F_GETFL, 0)) || 0 > fcntl(sock, F_SETFL, O_NONBLOCK | flags))
            goto err;
        return sock;
    } else
        goto err;
    FD_ZERO(&rd);
    FD_SET(sock, &rd);
    long ns = (timeout % 1000) * 1000000;
    struct timeval tv = { .tv_sec = timeout / 1000 + (ns / 1000000000), .tv_usec = ns % 1000000000 };
    if (0 > (rc = select(sock+1, &rd, NULL, NULL, &tv)))
        goto err;
    if (0 == rc)
        goto err;
    errno = 0;
    if (!FD_ISSET(sock, &rd)) return 0;
    if (getsockopt(sock, SOL_SOCKET, SO_ERROR, &err, &(socklen_t){sizeof(int)}) < 0) return 0;
    errno = err;
    if (0 == err) return sock;
    return sock;
err:
    close(sock);
    return -1;
}

// Srv

int net_srv_setopt_hnd (net_srv_t *srv, net_srv_opt_t opt, net_ev_handle_h arg) {
    srv->on_handle = arg;
    return 0;
}

int net_srv_setopt_check_url (net_srv_t *srv, net_srv_opt_t opt, check_url_h arg) {
    srv->on_check_url = arg;
    return 0;
}

int net_srv_setopt_int (net_srv_t *srv, net_srv_opt_t opt, int arg) {
    if (NET_SRV_TM_CONNECT == opt)
        srv->tm_connect = arg;
    else if (NET_SRV_TM_WAIT == opt)
        srv->tm_wait = arg;
    else if (NET_SRV_TM_COUNT == opt)
        srv->tm_count = arg;
    else if (NET_SRV_TM_PING == opt)
        srv->tm_ping = arg;
    else if (NET_SRV_TM_RECV == opt)
        srv->tm_recv = arg;
    else if (NET_SRV_POOL_SIZE == opt)
        pool_setopt_int(srv->pool, POOL_MAXSLOTS, arg);
    else if (NET_SRV_TM_CLOSE == opt)
        srv->tm_close = arg;
    else if (NET_SRV_OPT == opt)
        srv->flags = (net_flags_t)arg;
    else
        return -1;
    return 0;
}

int net_srv_setopt_hnd_std (net_srv_t *srv, net_srv_opt_t opt, net_ev_h arg) {
    switch (opt) {
        case NET_EV_CLOSE:
            srv->on_close = arg;
            return 0;
        case NET_EV_CLOSE_EV:
            srv->on_close_ev = arg;
            return 0;
        case NET_EV_FREE_EV:
            srv->on_free_ev = arg;
            return 0;
        default: break;
    }
    return -1;
}

int net_srv_setopt_hnd_connect (net_srv_t *srv, net_srv_opt_t opt, net_ev_connect_h arg) {
    srv->on_connect = arg;
    return 0;
}

int net_srv_setopt_srv_hnd (net_srv_t *srv, net_srv_opt_t opt, net_srv_h arg) {
    switch (opt) {
        case NET_SRV_START:
            srv->on_start = arg;
            return 0;
        case NET_SRV_STOP:
            srv->on_stop = arg;
            return 0;
        case NET_SRV_PING:
            srv->on_ping = arg;
            return 0;
        default: break;
    }
    return -1;
}

int net_srv_setopt_evbuf_free (net_srv_t *srv, net_srv_opt_t opt, net_ev_buf_free_h arg) {
    srv->on_evbuf_free = arg;
    return 0;
}

int net_srv_setopt_get_ev (net_srv_t *srv, net_srv_opt_t opt, get_ev_h arg) {
    srv->get_ev = arg;
    return 0;
}

static int add_id_info (net_srv_t *srv, ev_fd_t *fd) {
    int rc = NET_ERROR;
    net_ev_t *ev = srv->get_ev(srv, *fd);
    fd_t *f_id = (fd_t*)fd;
    ev_unlock(srv, *fd);
    pthread_rwlock_wrlock(&srv->rw_locker);
    if ((ev->ptr = add_tail_ev_list(srv->id_info, &f_id->fd, 0)))
        rc = NET_OK;
    pthread_rwlock_unlock(&srv->rw_locker);
    return rc;
}

static int on_add_evfd (ev_fd_t *x, void *userdata) {
    ev_array_t **res = userdata;
    add_ev_array(res, x);
    return ENUM_CONTINUE;
}

ev_array_t *get_all_id_info (net_srv_t *srv) {
    ev_array_t *res = create_ev_array(srv->id_info->len, 8, 0);
//    clist_item_t *x;
    pthread_rwlock_rdlock(&srv->rw_locker);
    foreach_ev_list(srv->id_info, on_add_evfd, &res, 0);
/*    if ((x = srv->id_info->head)) {
        do {
            ev_fd_t id = *(ev_fd_t*)x->ptr;
            add_ev_array(&res, &id, 0);
        } while (x != srv->id_info->head);
    }*/
    pthread_rwlock_unlock(&srv->rw_locker);
    return res;
}

static void rm_id_info (net_srv_t *srv, ev_fd_t *ptr) {
    pthread_rwlock_wrlock(&srv->rw_locker);
    if (ptr)
        del_ev_list(ptr, CF_FREE);
    pthread_rwlock_unlock(&srv->rw_locker);
}

static int ev_recv (int fd, int flags, net_srv_t *srv) {
    int rc = NET_OK;
    strbuf_t buf;
    net_ev_t *ev = srv->get_ev(srv, fd);
    buf = ev->rd_buf.buf;
    ev_unlock(srv, fd);
    if (-1 == net_recv(fd, &buf))
        rc = NET_ERROR;
    ev_lock(srv, fd);
    ev->rd_buf.buf = buf;
    ev_unlock(srv, fd);
    return rc;
}

void ev_reset (net_ev_t *ev) {
    netbuf_t *nbuf = &ev->rd_buf;
    strbuf_t tmp = nbuf->buf;
    nbuf->buf = nbuf->tail;
    nbuf->tail.ptr = tmp.ptr;
    nbuf->tail.len = 0;
}

int ev_send (int fd, net_srv_t *srv) {
    ssize_t rc;
    cstr_t *buf;
    net_ev_t *ev = srv->get_ev(srv, fd);
    buf = mkcstr(ev->wr_buf.ptr + ev->wrote, ev->wr_buf.len - ev->wrote);
    ev_unlock(srv, fd);
    if ((rc = net_write(fd, buf->ptr, buf->len)) > 0) {
        ev_lock(srv, fd);
        ev->wrote += rc;
        ev_unlock(srv, fd);
    }
    free(buf);
    return rc > 0 ? NET_OK : NET_ERROR;
}

static void net_ev_buf_free (ev_buf_t *n_ev) {
    if (n_ev->buf.ptr)
        free(n_ev->buf.ptr);
    free(n_ev);
}

int net_srv_event (void *userdata, pool_msg_t *msg) {
    int rc;
    ev_buf_t *n_ev = msg->data;
    net_srv_t *srv = n_ev->srv;
    if (NET_ERROR == (rc = srv->on_handle(n_ev))) {
        ev_fd_t fd = n_ev->fd;
        srv->on_evbuf_free(n_ev);
        net_ev_close(srv, fd, 0);
        return MSG_DONE;
    }
    if (NF_IS_DONE(n_ev->nf_flags)) {
        srv->on_evbuf_free(n_ev);
        return MSG_DONE;
    }
    return MSG_WAIT;
}

static int cmp_ev (const net_ev_t *x, net_ev_t *y) {
    if (NET_FD(x->fd) == NET_FD(y->fd))
        return 0;
    return -1;
}

static net_ev_t *get_ev (net_srv_t *srv, int i) {
    fdinfo_t *fdinfo = &srv->fd_info[i % FDINFO_MAX];
    net_ev_t *ev, ev_t = { .fd = i };
    pthread_mutex_lock(&fdinfo->locker);
    if ((ev = find_net_ev_list(&fdinfo->events, cmp_ev, &ev_t)))
        return ev;
    pthread_mutex_unlock(&fdinfo->locker);
    return NULL;
}

static void ev_check_out (net_srv_t *srv, net_ev_t *ev) {
    if (ev->wrote == ev->wr_buf.len) {
        free(ev->wr_buf.ptr);
        ev->wr_buf.ptr = NULL;
        ev->wr_buf.len = ev->wrote = 0;
    }
}

static int ev_rd_alloc (int fd, net_srv_t *srv) {
    int rc = NET_WAIT;
    net_ev_t *ev = srv->get_ev(srv, fd);
    if (!ev->rd_buf.buf.ptr && -1 == strbufalloc(&ev->rd_buf.buf, SRV_BUF_SIZE, SRV_BUF_SIZE))
        rc = NET_ERROR;
    if (!ev->rd_buf.tail.ptr && -1 == strbufalloc(&ev->rd_buf.tail, SRV_BUF_SIZE, SRV_BUF_SIZE))
        rc = NET_ERROR;
    ev_unlock(srv, fd);
    return rc;
}

static ev_buf_t *ev_get_in (int fd, net_srv_t *srv) {
    net_ev_t *ev = srv->get_ev(srv, fd);
    ev_buf_t *n_ev = calloc(1, srv->ev_bufsize);
    n_ev->nf_flags = ev->nf_flags;
    n_ev->srv = srv;
    n_ev->addr = ev->addr;
    n_ev->fd = ev->fd;
    n_ev->buf = ev->rd_buf.buf;
    ev->rd_buf.buf = ev->rd_buf.tail;
    strbufalloc(&ev->rd_buf.tail, SRV_BUF_SIZE, SRV_BUF_SIZE);
    if (srv->on_evbuf_set && NET_ERROR == srv->on_evbuf_set(n_ev, ev)) {
        srv->on_evbuf_free(n_ev);
        n_ev = NULL;
    }
    ev_unlock(srv, fd);
    return n_ev;
}

static void *on_reconnect (void *arg) {
    net_recon_t *recon = arg, *nrecon;
    net_srv_t *srv = recon->srv;
    pthread_mutex_lock(&srv->recon_locker);
    nrecon = add_tail_recon_list(srv->recon_list, recon, 0);
    pthread_mutex_unlock(&srv->recon_locker);
    pthread_mutex_lock(&recon->locker);
    while (recon->counter > 0) {
        struct timespec totime;
        if (net_ev_connect(srv, recon->url->ptr, recon->url->len) > 0)
            break;
        clock_gettime(CLOCK_MONOTONIC, &totime);
        totime.tv_sec += srv->tm_wait;
        if (ETIMEDOUT == pthread_cond_timedwait(&recon->cond, &recon->locker, &totime))
            break;
    }
    pthread_mutex_unlock(&recon->locker);
    free(recon->url);
    pthread_condattr_destroy(&recon->condattr);
    pthread_cond_destroy(&recon->cond);
    pthread_mutex_destroy(&recon->locker);
    pthread_mutex_lock(&srv->recon_locker);
    del_recon_list(nrecon, CF_FREE);
    pthread_mutex_unlock(&srv->recon_locker);
    free(recon);
    if (srv->recon_barrier)
        pthread_barrier_wait(srv->recon_barrier);
    return NULL;
}

static void ev_recon (net_srv_t *srv, cstr_t *url) {
    net_recon_t *recon = calloc(1, sizeof(net_recon_t));
    recon->srv = srv;
    recon->url = url;
    pthread_mutex_init(&recon->locker, NULL);
    pthread_condattr_setclock(&recon->condattr, CLOCK_MONOTONIC);
    pthread_cond_init(&recon->cond, &recon->condattr);
    recon->counter = srv->tm_count;
    pthread_create(&recon->th, NULL, on_reconnect, recon);
    pthread_detach(recon->th);
}

static void ev_close (int fd, net_srv_t *srv) {
    net_ev_t *ev = srv->get_ev(srv, fd);
    ev_fd_t *ptr;
    cstr_t *url = NULL;
    if ((srv->fd_info[fd % FDINFO_MAX].flags & NF_RECONNECT) && ev->url)
        url = mkcstr(ev->url->ptr, ev->url->len);
    ev_unlock(srv, fd);
    if (srv->on_close)
        srv->on_close(fd, srv);
    if (srv->on_close_ev)
        srv->on_close_ev(fd, srv);
    ev_lock(srv, fd);
    if (ev->rd_buf.buf.ptr) {
        free(ev->rd_buf.buf.ptr);
    }
    if (ev->rd_buf.tail.ptr)
        free(ev->rd_buf.tail.ptr);
    if (ev->wr_buf.ptr)
        free(ev->wr_buf.ptr);
    ptr = ev->ptr;
    ev->ptr = 0;
    del_net_ev_list(ev, CF_FREE);
    ev_unlock(srv, fd);
    rm_id_info(srv, ptr);
    if (url) ev_recon(srv, url);
}

static int poll_prepare (net_srv_t *srv) {
    if (!srv->on_handle)
        return NET_ERROR;
    if (srv->on_start && NET_ERROR == srv->on_start(srv))
        return NET_ERROR;
    if (srv->pool->max_slots <= 0)
        pool_setopt_int(srv->pool, POOL_MAXSLOTS, POOL_DEFAULT_SLOTS);
    pool_start(srv->pool);
    if (srv->ev_bufsize < sizeof(ev_buf_t))
        srv->ev_bufsize = sizeof(ev_buf_t);
    srv->is_active = 1;
    return NET_OK;
}

static int ev_accept (net_srv_t *srv) {
    struct sockaddr_in in_addr;
    int fd, flags;
    if (-1 == (fd = accept(srv->sfd, (struct sockaddr*)&in_addr, &(socklen_t){sizeof in_addr})))
        return -1;
    if (-1 != (flags = fcntl(fd, F_GETFL, 0)) && -1 != fcntl(fd, F_SETFL, flags | O_NONBLOCK))
        return fd;
    close(fd);
    return -1;
}

static int poll_ev_create (int sfd, net_srv_t *srv, int *fd) {
    net_ev_t *ev, ev_t;
    if (-1 == (*fd = ev_accept(srv))) {
        if (EAGAIN == errno)
            return NET_WAIT;
        return NET_ERROR;
    }
    ev_lock(srv, *fd);
    memset(&ev_t, 0, sizeof ev_t);
    ev = add_tail_net_ev_list(&srv->fd_info[*fd % FDINFO_MAX].events, &ev_t, 0);
    ev->fd = create_id(srv, *fd);
    ev->ptr = add_tail_ev_list(srv->id_info, &ev->fd, 0);
    ev_unlock(srv, *fd);
    return NET_OK;
}

static int poll_in (int fd, net_srv_t *srv) {
    int rc;
    net_ev_t *ev = srv->get_ev(srv, fd);
    uint32_t nf_flags = ev->nf_flags;
    ev->tm = time(0);
    ev_unlock(srv, fd);
    if (NET_ERROR == (rc = ev_rd_alloc(fd, srv)) ||
        NET_ERROR == (rc = srv->on_recv(fd, NF_READ, srv)))
        return NET_ERROR;
    ev_lock(srv, fd);
    if (NET_OK != rc || !ev->rd_buf.buf.len) {
        ev_unlock(srv, fd);
        return NET_WAIT;
    }
    ev_unlock(srv, fd);
    while (1) {
        ev_buf_t *n_ev = ev_get_in(fd, srv);
        if (!n_ev)
            return NET_ERROR;
        if (srv->on_recvd) {
            switch (srv->on_recvd(srv, n_ev)) {
                case NET_OK:
                    srv->on_evbuf_free(n_ev);
                    if ((nf_flags & NF_CLIENT)) {
                        ev_lock(srv, fd);
                        srv->fd_info[fd % FDINFO_MAX].flags &= ~NF_RECONNECT;
                        ev_unlock(srv, fd);
                    }
                    return NET_OK;
                case NET_ERROR:
                    srv->on_evbuf_free(n_ev);
                    return NET_ERROR;
            }
        }
        if (-1 == pool_call(srv->pool, srv->on_event, n_ev, NULL, 0))
            return NET_ERROR;
        if (NET_OK != (rc = srv->on_recv(fd, 0, srv)))
            return rc;
    }
    return NET_WAIT;
}

int poll_out (int fd, net_srv_t *srv) {
    net_ev_t *ev = srv->get_ev(srv, fd);
    int ev_flags = ev->nf_flags;
    ev_unlock(srv, fd);
    if ((ev_flags & NF_CLOSE)) {
        if ((ev_flags & NF_SENDBUF))
            srv->on_send(fd, srv);
        return NET_OK;
    }
    if (NET_ERROR == srv->on_send(fd, srv))
        return NET_ERROR;
    ev_lock(srv, fd);
    ev_check_out(srv, ev);
    ev_unlock(srv, fd);
    return NET_WAIT;
}

static void poll_close (net_srv_t *srv) {
    pthread_mutex_lock(&srv->locker);
    pool_destroy(srv->pool, srv->tm_close);
    if (srv->on_stop)
        srv->on_stop(srv);
    if (srv->fd_info) {
        for (int i = 1; i < FDINFO_MAX; ++i) {
            fdinfo_t *fdinfo = &srv->fd_info[i];
            foreach_net_ev_list(&fdinfo->events, ({
                int fn (net_ev_t *dummy1, void *dummy2) {
                    srv->on_free_ev(i, srv);
                    return ENUM_CONTINUE;
                } fn;
            }), NULL, 0);
            clear_net_ev_list(&fdinfo->events);
        }
    }
    if (srv->id_info)
        free_ev_list(srv->id_info);
    pthread_cond_destroy(&srv->cond);
    pthread_mutex_unlock(&srv->locker);
    free(srv);
}

#define DEFAULT_TM_COUNT 16
#define DEFAULT_TM_WAIT 10
net_srv_t *net_srv_init (const char *svc) {
    net_srv_t *srv;
    int efd = -1, sfd = -1, flags;
    if ((efd = net_poll_create()) < 0)
        goto done;
    if (svc) {
        struct sockaddr_in in_addr;
        memset(&in_addr, 0, sizeof in_addr);
        in_addr.sin_family = AF_INET;
        in_addr.sin_addr.s_addr = INADDR_ANY;
        sfd = socket(AF_INET, SOCK_STREAM, 0);
        setsockopt(sfd, SOL_SOCKET, SO_REUSEADDR, &(int){1}, sizeof(int));
        if (-1 == (in_addr.sin_port = atoport(svc, "tcp")))
            goto done;
        if (bind(sfd, (struct sockaddr*)&in_addr, sizeof in_addr) < 0 ||
            listen(sfd, SOMAXCONN) < 0 ||
            (flags = fcntl(sfd, F_GETFL, 0)) < 0 || fcntl(sfd, F_SETFL, flags | O_NONBLOCK))
                goto done;
        net_poll_ctl_listener(efd, sfd);
    }
    srv = calloc(1, sizeof(net_srv_t));
    srv->tm_close = 1;
    srv->tm_count = DEFAULT_TM_COUNT;
    srv->tm_wait = DEFAULT_TM_WAIT;
    srv->efd = efd;
    srv->sfd = sfd;
    srv->pool = pool_create();
    srv->pool->linked_obj = srv;
    srv->on_recv = ev_recv;
    srv->on_send = ev_send;
    srv->on_free_ev = ev_free_ev;
    srv->on_evbuf_free = net_ev_buf_free;
    srv->on_event = net_srv_event;
    srv->on_poll_prepare = poll_prepare;
    srv->on_poll_ev_create = poll_ev_create;
    srv->on_poll_ev_close = ev_close;
    srv->on_poll_in = poll_in;
    srv->on_poll_out= poll_out;
    srv->on_poll_close = poll_close;
    srv->get_ev = get_ev;
    srv->id_info = create_ev_list();
    srv->recon_list = create_recon_list();
    pthread_mutex_init(&srv->locker, NULL);
    pthread_cond_init(&srv->cond, NULL);
    pthread_rwlock_init(&srv->rw_locker, NULL);
    pthread_mutex_init(&srv->recon_locker, NULL);
    return srv;
done:
    net_poll_destroy(sfd, efd);
    return NULL;
}

int ev_free_ev (int fd, net_srv_t *srv) {
    net_ev_t *ev = srv->get_ev(srv, fd);
    ev_fd_t *ptr;
    if (ev->url)
        free(ev->url);
    if (ev->rd_buf.buf.ptr)
        free(ev->rd_buf.buf.ptr);
    if (ev->rd_buf.tail.ptr)
        free(ev->rd_buf.tail.ptr);
    if (ev->wr_buf.ptr)
        free(ev->wr_buf.ptr);
    ptr = ev->ptr;
    ev_unlock(srv, fd);
    if (ptr)
        rm_id_info(srv, ptr);
    close(fd);
    return NET_OK;
}

ev_fd_t net_ev_connect (net_srv_t *srv, char *url, size_t ulen) {
    struct sockaddr_in in_addr;
    struct in_addr addr;
    int flags, fd;
    uint32_t nf_flags;
    ev_fd_t evfd;
    net_ev_t *ev, ev_t;
    http_url_t h_url;
    http_parse_url(url, ulen, &h_url);
    if (!h_url.domain.ptr || !h_url.port.ptr) {
        http_free_url(&h_url);
        return 0;
    }
    if (!h_url.path.ptr) {
        h_url.path.ptr = strndup("/", 1);
        h_url.path.len = 1;
    }
    memset(&in_addr, 0, sizeof in_addr);
    in_addr.sin_family = AF_INET;
    if (-1 == (in_addr.sin_port = atoport(h_url.port.ptr, "tcp")) || -1 == atoaddr(h_url.domain.ptr, &addr)) {
        http_free_url(&h_url);
        return 0;
    }
    in_addr.sin_addr.s_addr = addr.s_addr;
    fd = socket(AF_INET, SOCK_STREAM, 0);
    setsockopt(fd, SOL_SOCKET, SO_REUSEADDR, (intptr_t*)&(int){1}, sizeof(int));
    if (srv->tm_connect > 0) {
        int err;
        struct pollfd fds [2] = {
            { .fd = fd, .events = POLLIN, .revents = 0 },
            { .fd = fd, .events = POLLOUT, .revents = 0 }
        };
        if ((flags = fcntl(fd, F_GETFL, 0) < 0) || fcntl(fd, F_SETFL, flags | O_NONBLOCK) < 0 ||
            (-1 == connect(fd, (struct sockaddr*)&in_addr, sizeof in_addr) && EINPROGRESS != errno) ||
            poll(fds, 2, srv->tm_connect * 1000) <= 0 ||
            (fds[0].revents & (POLLERR | POLLHUP)) > 0 ||
            (fds[1].revents & (POLLERR | POLLHUP)) > 0 ||
            getsockopt(fd, SOL_SOCKET, SO_ERROR, &err, &(socklen_t){sizeof err}) < 0 || 0 != err) {
            http_free_url(&h_url);
            close(fd);
            return 0;
        }
        fcntl(fd, F_SETFL, flags & ~O_NONBLOCK);
    } else {
        if (-1 == connect(fd, (struct sockaddr*)&in_addr, sizeof in_addr)) {
            http_free_url(&h_url);
            close(fd);
            return 0;
        }
    }
    nf_flags = NF_CLIENT;
    if (srv->on_connect && NET_ERROR == srv->on_connect(fd, srv, &nf_flags, &h_url)) {
        http_free_url(&h_url);
        close(fd);
        return 0;
    }
    if (-1 == (flags = fcntl(fd, F_GETFL, 0)) || -1 == fcntl(fd, F_SETFL, flags | O_NONBLOCK)) {
        http_free_url(&h_url);
        close(fd);
        return 0;
    }
    nf_flags |= NF_CONNECTED;
    ev_t.url = mkcstr(url, ulen);
    ev_t.nf_flags = nf_flags;
    ev_t.fd = evfd = create_id(srv, fd);
    ev_lock(srv, fd);
//    if ((srv->flags & SF_RECONNECT))
//        srv->fd_info[fd % FDINFO_MAX].flags = NF_RECONNECT;
    ev = add_tail_net_ev_list(&srv->fd_info[fd % FDINFO_MAX].events, &ev_t, 0);
    ev_unlock(srv, fd);
    net_poll_ctl_add(srv->efd, fd);
    if (NET_ERROR == add_id_info(srv, &evfd)) {
        net_ev_close(srv, evfd, NF_FD);
        return 0;
    }
    http_free_url(&h_url);
    return ev->fd;
}

int net_ev_send (net_srv_t *srv, ev_fd_t fd, uint32_t flags, const char *buf, size_t len) {
    net_ev_t *ev = NULL;
    int i_fd = 0;
    errno = 0;
    if (!buf || !(ev = srv->get_ev(srv, NET_FD(fd))))
        return NET_ERROR;
    if (!(NF_FD & flags))
        i_fd = NET_FD(ev->fd);
    else
        i_fd = fd;
    if (0 == i_fd) {
        ev_unlock(srv, NET_FD(ev->fd));
        errno = EBADF;
        return NET_ERROR;
    }
    if (!ev->wr_buf.ptr && -1 == strbufalloc(&ev->wr_buf, len, SRV_BUF_SIZE)) {
        ev_unlock(srv, NET_FD(ev->fd));
        return NET_ERROR;
    }
    strbufadd(&ev->wr_buf, buf, len);
    ev_unlock(srv, NET_FD(ev->fd));
    net_poll_ctl_out(srv->efd, i_fd);
    return NET_OK;
}

int net_ev_close (net_srv_t *srv, ev_fd_t fd, uint32_t flags) {
    net_ev_t *ev = srv->get_ev(srv, NET_FD(fd));
    int nfd = fd;
    if (!(NF_FD & flags) && 0 == (nfd = NET_FD(ev->fd))) {
        ev_unlock(srv, NET_FD(ev->fd));
        return NET_ERROR;
    }
    ev->nf_flags |= NF_CLOSE;
    ev_unlock(srv, NET_FD(ev->fd));
    net_poll_ctl_out(srv->efd, nfd);
    return NET_OK;
}

net_ev_t *net_ev_lock (net_srv_t *srv, ev_fd_t fd) {
    net_ev_t *ev = srv->get_ev(srv, NET_FD(fd));
    if (ev && ev->fd == fd)
        return ev;
    ev_unlock(srv, NET_FD(fd));
    return NULL;

}

static int on_close_recon (net_recon_t *recon, void *userdata) {
    pthread_mutex_lock(&recon->locker);
    recon->counter = 0;
    pthread_cond_signal(&recon->cond);
    pthread_mutex_unlock(&recon->locker);
    return ENUM_CONTINUE;
}

static void net_destroy_recon (net_srv_t *srv) {
    pthread_mutex_lock(&srv->recon_locker);
    srv->recon_barrier = malloc(sizeof(pthread_barrier_t));
    pthread_barrier_init(srv->recon_barrier, NULL, srv->recon_list->len+1);
    pthread_mutex_unlock(&srv->recon_locker);
    foreach_recon_list(srv->recon_list, on_close_recon, NULL, 0);
    pthread_mutex_unlock(&srv->recon_locker);
    pthread_barrier_wait(srv->recon_barrier);
    pthread_barrier_destroy(srv->recon_barrier);
    free(srv->recon_barrier);
    free_recon_list(srv->recon_list);
}

static int on_send_fin (ev_fd_t *fd, void *userdata) {
    net_srv_t *srv = userdata;
    srv->on_close_ev(NET_FD(*fd), srv);
    return ENUM_CONTINUE;
}

void net_srv_done (net_srv_t *srv) {
    srv->is_active = 0;
    shutdown(srv->sfd, SHUT_RD);
    net_destroy_recon(srv);
    if (srv->on_close_ev)
        foreach_ev_list(srv->id_info, on_send_fin, NULL, 0);
    net_poll_destroy(srv->efd, srv->sfd);
}
