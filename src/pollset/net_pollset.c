/*Copyright (c) Brian B.

Permission to use, copy, modify, and/or distribute this software for any purpose
with or without fee is hereby granted.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE
OF THIS SOFTWARE.
*/
#include "../include/libex/poll/net_poll.h"

#include <sys/pollset.h>

int net_poll_create () {
    return (int)pollset_create(-1);
}

int net_poll_destroy (int efd, int sfd) {
    if (sfd > 0) {
        struct poll_ctl pc = { .cmd = PS_DELETE, .fd = sfd };
        if (pollset_ctl(efd, &pc, 1))
            return NET_ERROR;
        close(sfd);
    }
    if (efd > 0 && !pollset_destroy(efd))
        return NET_OK;
    return NET_ERROR;
}

int net_poll_ctl_listener (int efd, int fd) {
    struct poll_ctl pc = { .cmd = PS_ADD, .events = POLLIN, .fd = fd };
    return pollset_ctl(efd, &pc, 1);
}

int net_poll_ctl_add (int efd, int fd) {
    struct poll_ctl pc = { .cmd = PS_ADD, .events = POLLIN, .fd = fd };
    return pollset_ctl(efd, &pc, 1);
}

int net_poll_ctl_out (int efd, int fd) {
    struct poll_ctl pc = { .cmd = PS_MOD, .events = POLLIN | POLLOUT, .fd = fd };
    return pollset_ctl(efd, &pc, 1);
}

#define MAX_POLLFD 1024
void net_poll (net_srv_t *srv) {
    struct pollfd events [MAX_POLLFD];
    if (NET_ERROR == srv->on_poll_prepare(srv))
        return;
    while (srv->is_active) {
        int n = pollset_poll(srv->efd, events, MAX_POLLFD, -1);
        for (int i = 0; i < n; ++i) {
            if ((events[i].revents & POLLERR) || (events[i].revents & POLLHUP)) {
                struct poll_ctl pc = { .cmd = PS_DELETE, .fd = events[i].fd };
                pollset_ctl(srv->efd, &pc, 1);
                srv->on_poll_ev_close(events[i].fd, srv);
                close(events[i].fd);
            } else if ((events[i].revents & POLLIN)) {
                if (events[i].fd == srv->sfd) {
                    while (1) {
                        struct poll_ctl pc = { .cmd = PS_ADD, .events = POLLIN };
                        int fd, rc = srv->on_poll_ev_create(events[i].fd, srv, &fd);
                        if (NET_ERROR == rc)
                            break;
                        pc.fd = fd;
                        if (NET_WAIT == rc) {
                            if (pc.fd > 0) {
                                pc.cmd = PS_DELETE;
                                pollset_ctl(srv->efd, &pc, 1);
                                srv->on_poll_ev_close(pc.fd, srv);
                                close(pc.fd);
                            }
                            continue;
                        }
                        pollset_ctl(srv->efd, &pc, 1);
                    }
                } else if (NET_ERROR == srv->on_poll_in(events[i].fd, srv)) {
                    struct poll_ctl pc = { .cmd = PS_DELETE, .fd = events[i].fd };
                    pollset_ctl(srv->efd, &pc, 1);
                    srv->on_poll_ev_close(events[i].fd, srv);
                    close(events[i].fd);
                }
            }
            if ((events[i].revents & POLLOUT)) {
                struct poll_ctl pc = { .cmd = PS_MOD, .events = POLLIN, .fd = events[i].fd };
                if (NET_WAIT != srv->on_poll_out(events[i].fd, srv)) {
                    pc.cmd = PS_DELETE;
                    pollset_ctl(srv->efd, &pc, 1);
                    srv->on_poll_ev_close(events[i].fd, srv);
                    close(events[i].fd);
                }
                pollset_ctl(srv->efd, &pc, 1);
            }
        }
    }
    srv->on_poll_close(srv);
}
