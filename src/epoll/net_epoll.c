/*Copyright (c) Brian B.

Permission to use, copy, modify, and/or distribute this software for any purpose
with or without fee is hereby granted.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*/
#include "../include/libex/poll/net_poll.h"

#include <sys/epoll.h>

int net_poll_create () {
    return epoll_create1(0);
}

int net_poll_destroy (int efd, int sfd) {
    if (sfd > 0)
        close(sfd);
    if (efd > 0)
        close(efd);
    return NET_OK;
}

int net_poll_ctl_listener (int efd, int sfd) {
    struct epoll_event event = { .data.fd = sfd, .events = EPOLLIN };
    return epoll_ctl(efd, EPOLL_CTL_ADD, sfd, &event);
}

int net_poll_ctl_add (int efd, int fd) {
    struct epoll_event event = { .data.fd = fd, .events = EPOLLIN | EPOLLET };
    return epoll_ctl(efd, EPOLL_CTL_ADD, fd, &event);
}

int net_poll_ctl_out (int efd, int fd) {
    struct epoll_event event = { .data.fd = fd, .events = EPOLLIN | EPOLLOUT | EPOLLET };
    return epoll_ctl(efd, EPOLL_CTL_MOD, fd, &event);
}

void net_poll (net_srv_t *srv) {
    struct epoll_event events [SOMAXCONN];
    if (NET_ERROR == srv->on_poll_prepare(srv))
        return;
    while (srv->is_active) {
        int n = epoll_wait(srv->efd, events, SOMAXCONN, -1);
        for (int i = 0; i < n; ++i) {
            struct epoll_event event;
            if ((events[i].events & EPOLLERR) || (events[i].events & EPOLLHUP)) {
                epoll_ctl(srv->efd, EPOLL_CTL_DEL, events[i].data.fd, NULL);
                srv->on_poll_ev_close(events[i].data.fd, srv);
                close(events[i].data.fd);
            } else if ((events[i].events & EPOLLIN)) {
                if (events[i].data.fd == srv->sfd) {
                    while (1) {
                        int fd, rc = srv->on_poll_ev_create(events[i].data.fd, srv, &fd);
                        if (NET_WAIT == rc)
                            break;
                        event.data.fd = fd;
                        if (NET_ERROR == rc) {
                            if (event.data.fd > 0) {
                                epoll_ctl(srv->efd, EPOLL_CTL_DEL, event.data.fd, NULL);
                                srv->on_poll_ev_close(event.data.fd, srv);
                                close(event.data.fd);
                            }
                            continue;
                        }
                        event.events = EPOLLIN | EPOLLET;
                        epoll_ctl(srv->efd, EPOLL_CTL_ADD, event.data.fd, &event);
                    }
                } else if (NET_ERROR == srv->on_poll_in(events[i].data.fd, srv)) {
                        epoll_ctl(srv->efd, EPOLL_CTL_DEL, events[i].data.fd, NULL);
                        srv->on_poll_ev_close(events[i].data.fd, srv);
                        close(events[i].data.fd);
                }
            }
            if ((events[i].events & EPOLLOUT)) {
                if (NET_WAIT != srv->on_poll_out(events[i].data.fd, srv)) {
                    epoll_ctl(srv->efd, EPOLL_CTL_DEL, events[i].data.fd, NULL);
                    srv->on_poll_ev_close(events[i].data.fd, srv);
                    close(events[i].data.fd);
                    continue;
                }
                event.data.fd = events[i].data.fd;
                event.events = EPOLLIN | EPOLLET;
                epoll_ctl(srv->efd, EPOLL_CTL_MOD, event.data.fd, &event);
            }
        }
    }
    srv->on_poll_close(srv);
}
