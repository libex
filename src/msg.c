/*Copyright (c) Brian B.

Permission to use, copy, modify, and/or distribute this software for any purpose
with or without fee is hereby granted.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*/
#include "../include/libex/msg.h"

#define MSG_SETLEN(M,L) *(uint32_t*)M->ptr = L
#define MSG_INCLEN(M,L) *(uint32_t*)M->ptr += L

#define MSG_ADD(buf,v,betoh,type) \
    if (0 == strbufsize(buf, buf->len + sizeof(type), 0)) { \
        type x = betoh(v); \
        return msg_addbuf(buf, &x, sizeof(type)); \
    } \
    return -1

#define MSG_GET(buf,htobe,type,v) \
    if (buf->ptr + buf->len < buf->ptr + buf->pos + sizeof(type)) \
        return -1; \
    *v = htobe(*(type*)(buf->ptr + buf->pos)); \
    buf->pos += sizeof(type); \
    return 0

int msg_alloc (strbuf_t *buf, msglen_t len, msglen_t chunk_size) {
    if (len < sizeof(msglen_t))
        len = sizeof(msglen_t);
    if (0 == strbufalloc(buf, len, chunk_size)) {
        *(msglen_t*)buf->ptr = 0;
        buf->len = buf->pos = sizeof(msglen_t);
        return 0;
    }
    return -1;
}

void msg_reset (strbuf_t *buf) {
    *(msglen_t*)buf->ptr = 0;
    buf->len = buf->pos = sizeof(msglen_t);
}

int msg_read (int fd, strbuf_t *buf) {
    uint32_t msg_len, buf_len;
    if (sizeof(uint32_t) != read(fd, &msg_len, sizeof(uint32_t)))
        return -1;
    buf_len = msg_len + sizeof(uint32_t);
    if (buf->ptr) {
        if (-1 == strbufsize(buf, buf_len, 0))
            return -1;
    } else if (-1 == strbufalloc(buf, buf_len, 8))
        return -1;
    *(msglen_t*)buf->ptr = msg_len;
    buf->pos = sizeof(msglen_t);
    buf->len = buf_len;
    if (msg_len != read(fd, buf->ptr + buf->pos, msg_len))
        return -1;
    return 0;
}

int msg_parse (strbuf_t *buf, msglen_t off) {
    msglen_t len = 0;
    buf->pos = 0;
    if (off > 0 && -1 == msg_skip(buf, off))
        return -1;
    if (-1 == msg_getui32(buf, &len))
        return -1;
    if (buf->len - (sizeof(msglen_t) + off) < len)
        return 0;
    return 1;
}

ssize_t msg_checklen (const char *buf, msglen_t len) {
    ssize_t l;
    if (len < sizeof(msglen_t))
        return -1;
    l = *(msglen_t*)buf;
    if (l <= len)
        return l;
    return -1;
}


int msg_addbuf(strbuf_t *buf, void *src, msglen_t len) {
    int rc;
    if (0 == (rc = strbufsize(buf, buf->len + len, 0)) &&
        0 == (rc = strbufadd(buf, src, len)))
            MSG_INCLEN(buf, len);
    return rc;
}

int msg_addstr (strbuf_t *buf, const char *str, msglen_t slen) {
    int rc;
    if (0 == (rc = strbufsize(buf, buf->len + sizeof(msglen_t) + slen, 0)) &&
        0 == (rc = msg_addui32(buf, slen)) &&
        0 == (rc = strbufadd(buf, str, slen)))
            MSG_INCLEN(buf, slen);
    return rc;
}

#define htobe8(x) x
int msg_addi8 (strbuf_t *buf, int8_t v) {
    MSG_ADD(buf, v, htobe8, int8_t);
}

int msg_addui8 (strbuf_t *buf, uint8_t v) {
    MSG_ADD(buf, v, htobe8, uint8_t);
}

int msg_addi16 (strbuf_t *buf, int16_t v) {
    MSG_ADD(buf, v, htobe16, int16_t);
}

int msg_addui16 (strbuf_t *buf, uint16_t v) {
    MSG_ADD(buf, v, htobe16, uint16_t);
}

int msg_addi32 (strbuf_t *buf, int32_t v) {
    MSG_ADD(buf, v, htobe32, int32_t);
}

int msg_addui32 (strbuf_t *buf, uint32_t v) {
    MSG_ADD(buf, v, htobe32, uint32_t);
}

int msg_addi64 (strbuf_t *buf, int64_t v) {
    MSG_ADD(buf, v, htobe64, int64_t);
}

int msg_addui64 (strbuf_t *buf, uint64_t v) {
    MSG_ADD(buf, v, htobe64, uint64_t);
}

int msg_addf4 (strbuf_t *buf, float v) {
    MSG_ADD(buf, (uint32_t)v, htobe32, uint32_t);
}

int msg_addf8 (strbuf_t *buf, double v) {
    MSG_ADD(buf, (uint64_t)v, htobe64, uint64_t);
}

int msg_addf16 (strbuf_t *buf, long double v) {
    struct {
        uint64_t i [2];
        long double d;
    } _v = { .d = v };
    MSG_ADD(buf, _v.i[0], htobe64, int64_t);
    MSG_ADD(buf, _v.i[1], htobe64, int64_t);
}


void *msg_getbuf (strbuf_t *buf, size_t len) {
    void *res;
    if (buf->ptr + buf->len < buf->ptr + buf->pos + len)
        return NULL;
    res = buf->ptr + buf->pos;
    buf->pos += len;
    return res;
}

int msg_getstr (strbuf_t *buf, strptr_t *str) {
    uint32_t len = 0;
    void *s;
    if (0 == msg_getui32(buf, &len) &&
        (s = msg_getbuf(buf, len))) {
        str->ptr = s;
        str->len = len;
        return 0;
    }
    return -1;
}

int msg_getstrcpy (strbuf_t *buf, strptr_t *str) {
    int rc;
    strptr_t tmp;
    if (0 == (rc = msg_getstr(buf, &tmp))) {
        str->ptr = strndup(tmp.ptr, tmp.len);
        str->len = tmp.len;
    }
    return rc;
}

cstr_t *msg_getcstr (strbuf_t *buf) {
    strptr_t str;
    if (!msg_getstr(buf, &str)) {
        if (0 == str.len) return NULL;
        return mkcstr(str.ptr, str.len);
    }
    return NULL;
}

#define be8toh(x) x
int msg_geti8 (strbuf_t *buf, int8_t *v) {
    MSG_GET(buf, be8toh, int8_t, v);
}

int msg_getui8 (strbuf_t *buf, uint8_t *v) {
    MSG_GET(buf, be8toh, uint8_t, v);
}

int msg_geti16 (strbuf_t *buf, int16_t *v) {
    MSG_GET(buf, be16toh, int16_t, v);
}

int msg_getui16 (strbuf_t *buf, uint16_t *v) {
    MSG_GET(buf, be16toh, uint16_t, v);
}

int msg_geti32 (strbuf_t *buf, int32_t *v) {
    MSG_GET(buf, be32toh, int32_t, v);
}

int msg_getui32 (strbuf_t *buf, uint32_t *v) {
    MSG_GET(buf, be32toh, uint32_t, v);
}

int msg_geti64 (strbuf_t *buf, int64_t *v) {
    MSG_GET(buf, be64toh, int64_t, v);
}

int msg_getui64 (strbuf_t *buf, uint64_t *v) {
    MSG_GET(buf, be64toh, uint64_t, v);
}

int msg_getf4 (strbuf_t *buf, float *v) {
    MSG_GET(buf, be32toh, uint32_t, (uint32_t*)v);
}

int msg_getf8 (strbuf_t *buf, double *v) {
    MSG_GET(buf, be64toh, uint64_t, (uint64_t*)v);
}

int msg_getf16 (strbuf_t *buf, long double *v) {
    struct {
        uint64_t i [2];
        long double d;
    } *_v = (void*)v;
    MSG_GET(buf, be64toh, uint64_t, &_v->i[0]);
    MSG_GET(buf, be64toh, uint64_t, &_v->i[1]);
}


int msg_skip (strbuf_t *buf, msglen_t len) {
    if (buf->ptr + buf->len < buf->ptr + buf->pos + len)
        return -1;
    buf->pos += len;
    return 0;
}
