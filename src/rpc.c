/*Copyright (c) Brian B.

Permission to use, copy, modify, and/or distribute this software for any purpose
with or without fee is hereby granted.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*/
#include "../include/libex/rpc.h"
DECLARE_SORTED_ARRAY_FUN(rpc_list,rpc_method_t)

static int methods_compare (rpc_method_t *x, rpc_method_t *y) {
    if (x->method > y->method) return 1;
    if (x->method < y->method) return -1;
    return 0;
}

rpc_list_t *rpc_init_methods () {
    rpc_list_t *res = create_rpc_list(64, 64, 0);
    res->on_compare = methods_compare;
    return res;
}

int rpc_add_method (rpc_list_t **methods,
                    uint32_t method,
                    rpc_method_h handle,
                    rpc_type_t *arg_types,
                    int arg_len) {
    rpc_method_t m = { .method = method, .handle = handle, .arg_types = arg_types, .arg_len = arg_len };
    add_rpc_list(methods, &m);
    return EEXIST == errno ? -1 : 0;
}

int rpc_set_method (rpc_list_t *methods,
                    uint32_t method,
                    rpc_method_h handle,
                    rpc_type_t *arg_types,
                    int arg_len) {
    rpc_method_t t = { .method = method },
                 *m = find_rpc_list(methods, NULL, &t);
    if (EEXIST == errno) {
        m->handle = handle;
        m->arg_len = arg_len;
        m->arg_types = arg_types;
        return 0;
    }
    return -1;
}

static int rpc_parse_params (strbuf_t *buf, rpc_request_t *req) {
    rpc_param_t *params = malloc(req->params_len * sizeof(rpc_param_t));
    for (int i = 0; i < req->params_len; ++i) {
        if (-1 == msg_getui16(buf, &params[i].typ))
            goto err;
        switch (params[i].typ) {
            case RPC_STRING:
                if (-1 == msg_getstr(buf, &params[i].data.s))
                    goto err;
                break;
            case RPC_INTEGER:
                if (-1 == msg_geti64(buf, &params[i].data.i))
                    goto err;
                break;
            case RPC_DOUBLE:
                if (-1 == msg_getf8(buf, &params[i].data.d))
                    goto err;
                break;
            case RPC_NULL:
            case RPC_TRUE:
            case RPC_FALSE:
                break;
            default:
                goto err;
        }
    }
    req->params = params;
    return RPC_OK;
err:
    free(params);
    return RPC_INVALID_REQUEST;
}

int rpc_parse_response(strbuf_t *buf, rpc_response_t *resp) {
    if (-1 == msg_geti32(buf, &resp->errcode))
        return RPC_PARSE_ERROR;
    return RPC_OK;
}

int rpc_parse_request (strbuf_t *buf, rpc_request_t *req) {
    if (-1 == msg_getui32(buf, &req->method) ||
        -1 == msg_geti16(buf, &req->params_len))
            return RPC_INVALID_REQUEST;
    if (0 == req->params_len)
        return RPC_OK;
    return rpc_parse_params(buf, req);
}

int rpc_parse (strbuf_t *buf, msglen_t off, rpc_t *rpc) {
    int rc = msg_parse(buf, off);
    if (-1 == rc)
        return RPC_INVALID_REQUEST;
    if (0 == rc)
        return RPC_PARSE_ERROR;
    if (-1 == msg_getui8(buf, &rpc->flags))
        return RPC_INVALID_REQUEST;
    if ((rpc->flags & RPC_IDSTR)) {
        if (-1 == msg_getstr(buf, &rpc->id.id_str))
            return RPC_INVALID_REQUEST;
    } else
    if ((rpc->flags & RPC_IDINT)) {
        if (-1 == msg_geti64(buf, &rpc->id.id_int))
            return RPC_INVALID_REQUEST;
    }
    switch (rpc->flags & RPC_TYPE) {
        case RPC_REQUEST:
            return rpc_parse_request(buf, &rpc->data.request);
        case RPC_RESPONSE:
            return rpc_parse_response(buf, &rpc->data.response);
    }
    return RPC_INVALID_REQUEST;
}

void rpc_print (rpc_t *rpc) {
    printf("type: ");
    if (RPC_REQUEST == (rpc->flags & RPC_TYPE))
        printf("REQUEST");
    else
    if (RPC_RESPONSE == (rpc->flags & RPC_TYPE))
        printf("RESPONSE");
    else
        printf("unknown");
    if ((RPC_IDSTR & rpc->flags)) {
        char *s = strndup(rpc->id.id_str.ptr, rpc->id.id_str.len);
        printf(", has id (%s)", s);
        free(s);
    }
    else
    if ((RPC_IDINT & rpc->flags))
        printf(", has id (" LONG_FMT ")", rpc->id.id_int);
    printf("\n");
    if (RPC_REQUEST == (rpc->flags & RPC_TYPE)) {
        printf("method: %u\n", rpc->data.request.method);
        printf("params count: %u\n", rpc->data.request.params_len);
        for (int i = 0; i < rpc->data.request.params_len; ++i) {
            switch (rpc->data.request.params[i].typ) {
                case RPC_STRING:
                    printf(" %d(string): %s\n", i, rpc->data.request.params[i].data.s.ptr);
                    break;
                case RPC_INTEGER:
                    printf(" %d(int): " LONG_FMT "\n", i, rpc->data.request.params[i].data.i);
                    break;
                case RPC_DOUBLE:
                    printf(" %d(double): %f\n", i, rpc->data.request.params[i].data.d);
                    break;
                case RPC_TRUE:
                    printf(" %d(bool): true\n", i);
                    break;
                case RPC_FALSE:
                    printf(" %d(bool): false\n", i);
                    break;
                case RPC_NULL:
                    printf(" %d : null\n", i);
                    break;
            }
        }
    } else
    if (RPC_RESPONSE == (rpc->flags & RPC_TYPE)) {
        printf("code: %d\n", rpc->data.response.errcode);
    }
}

static int16_t *rpc_get_params_len_pos (strbuf_t *buf) {
    int len;
    uint8_t flags = *(uint8_t*)(buf->ptr + sizeof(msglen_t));
    if (!(flags & RPC_REQUEST))
        return NULL;
    if ((flags & RPC_IDSTR)) {
        uint32_t str_len = *(uint32_t*)(buf->ptr + sizeof(msglen_t) + sizeof(uint8_t));
        len = sizeof(msglen_t) + sizeof(uint8_t) + sizeof(uint32_t) + str_len + sizeof(uint32_t);
        if (len >= buf->len)
            return NULL;
        return (int16_t*)(buf->ptr + len);
    }
    if ((flags & RPC_IDINT)) {
        len = sizeof(msglen_t) + sizeof(uint8_t) + sizeof(int64_t) + sizeof(uint32_t);
        if (len >= buf->len)
            return NULL;
        return (int16_t*)(buf->ptr + len);
    }
    len = sizeof(msglen_t) + sizeof(uint8_t) + sizeof(uint32_t);
    if (len >= buf->len)
        return NULL;
    return (int16_t*)(buf->ptr + len);
}

int rpc_add_str (strbuf_t *msg, const char *str, size_t slen) {
    int16_t *len;
    if (0 != msg_addui16(msg, RPC_STRING) ||
        0 != msg_addstr(msg, str, slen))
        return RPC_INTERNAL_ERROR;
    if ((len = rpc_get_params_len_pos(msg)))
        *len += 1;
    return RPC_OK;
}

int rpc_add_int (strbuf_t *msg, int64_t i) {
    int16_t *len;
    if (0 != msg_addui16(msg, RPC_INTEGER) ||
        0 != msg_addi64(msg, i))
        return RPC_INTERNAL_ERROR;
    if ((len = rpc_get_params_len_pos(msg)))
        *(len) += 1;
    return RPC_OK;
}

int rpc_add_double (strbuf_t *msg, double d) {
    int16_t *len;
    if (0 != msg_addui16(msg, RPC_DOUBLE) ||
        0 != msg_addf8(msg, d))
        return RPC_INTERNAL_ERROR;
    if ((len = rpc_get_params_len_pos(msg)))
        *(len) += 1;
    return RPC_OK;
}

int rpc_add_true (strbuf_t *msg) {
    int16_t *len;
    if (0 != msg_addui16(msg, RPC_TRUE))
        return RPC_INTERNAL_ERROR;
    if ((len = rpc_get_params_len_pos(msg)))
        *(len) += 1;
    return RPC_OK;
}

int rpc_add_false (strbuf_t *msg) {
    int16_t *len;
    if (0 != msg_addui16(msg, RPC_FALSE))
        return RPC_INTERNAL_ERROR;
    if ((len = rpc_get_params_len_pos(msg)))
        *(len) += 1;
    return RPC_OK;
}

int rpc_add_null (strbuf_t *msg) {
    int16_t *len;
    if (0 != msg_addui16(msg, RPC_NULL))
        return RPC_INTERNAL_ERROR;
    if ((len = rpc_get_params_len_pos(msg)))
        *(len) += 1;
    return RPC_OK;
}

static int rpc_add_hdr (strbuf_t *buf, uint8_t flags, intptr_t p, int size) {
    if (size > 0)
        return 0 == msg_addui8(buf, flags | RPC_IDSTR) && 0 == msg_addstr(buf, (char*)p, size) ? 0 : -1;
    if (size < 0)
        return 0 == msg_addui8(buf, flags | RPC_IDINT) && 0 == msg_addi64(buf, p) ? 0 : -1;
    return msg_addui8(buf, flags);
}

int rpc_create_request (strbuf_t *buf, uint32_t method, rpc_h on_params, intptr_t id, int id_len, void *userdata) {
    if (buf->ptr)
        msg_clear(buf, 0);
    else
    if (-1 == msg_alloc(buf, SRV_BUF_SIZE, SRV_BUF_SIZE))
        return RPC_INTERNAL_ERROR;
    if (-1 == rpc_add_hdr(buf, RPC_REQUEST, id, id_len) ||
        -1 == msg_addui32(buf, method) ||
        -1 == msg_addi16(buf, 0))
        return RPC_INTERNAL_ERROR;
    if (on_params)
        return on_params(buf, userdata);
    return RPC_OK;
}

int rpc_create_response (strbuf_t *buf, int32_t errcode, rpc_h on_response, intptr_t id, int id_len, void *userdata) {
    if (buf->ptr)
        msg_clear(buf, 0);
    else
    if (-1 == msg_alloc(buf, SRV_BUF_SIZE, SRV_BUF_SIZE))
        return RPC_INTERNAL_ERROR;
    if (-1 == rpc_add_hdr(buf, RPC_RESPONSE, id, id_len) ||
        -1 == msg_addi32(buf, errcode))
        return RPC_INTERNAL_ERROR;
    if (on_response)
        return on_response(buf, userdata);
    return RPC_OK;
}

void rpc_clear (rpc_t *rpc) {
    if ((rpc->flags & RPC_REQUEST) && rpc->data.request.params)
        free(rpc->data.request.params);
    memset(rpc, 0, sizeof(rpc_t));
}

int rpc_getid (rpc_t *rpc, int is_copy, intptr_t *id) {
    if ((rpc->flags & RPC_IDSTR)) {
        if (is_copy)
            *id = (intptr_t)strndup(rpc->id.id_str.ptr, rpc->id.id_str.len);
        else
            *id = (intptr_t)rpc->id.id_str.ptr;
        return rpc->id.id_str.len;
    }
    if ((rpc->flags & RPC_IDINT)) {
        *id = rpc->id.id_int;
        return -1;
    }
    *id = 0;
    return 0;
}

int rpc_execute_method (strbuf_t *buf, rpc_method_t *method, rpc_t *rpc, void *userdata) {
    int rc;
    intptr_t id = 0;
    int id_len = rpc_getid(rpc, 1, &id);
    if (rpc->data.request.params_len != method->arg_len) {
        if ((rpc->flags & RPC_REQUEST))
            rpc_create_error(buf, RPC_INVALID_PARAMS, id, id_len);
        else
            msg_clear(buf, STR_REDUCE);
        if (id && id_len > 0)
            free((void*)id);
        return RPC_INVALID_PARAMS;
    }
    for (int i = 0; i < method->arg_len; ++i) {
        if (RPC_VARG == method->arg_types[i])
            break;
        if (RPC_ANY == method->arg_types[i])
            continue;
        if (method->arg_types[i] != rpc->data.request.params[i].typ) {
            if ((rpc->flags & RPC_HASID))
                rpc_create_error(buf, RPC_INVALID_PARAMS, id, id_len);
            else
                msg_clear(buf, STR_REDUCE);
            if (id && id_len > 0)
                free((void*)id);
            return RPC_INVALID_PARAMS;
        }
    }
    if (id && id_len > 0)
        free((void*)id);
    if (0 == (rc = method->handle(buf, rpc, userdata)))
        rc = method->method;
    return rc;
}

static rpc_method_t *rpc_find_method (strbuf_t *buf, rpc_list_t *methods, rpc_t *rpc) {
    rpc_method_t t = { .method = rpc->data.request.method };
    return find_rpc_list(methods, NULL, &t);
}

int rpc_execute_parsed (strbuf_t *buf, rpc_list_t *methods, rpc_t *rpc, void *userdata) {
    rpc_method_t *method;
    intptr_t id = 0;
    int id_len = rpc_getid(rpc, 1, &id);
    if (!(method = rpc_find_method(buf, methods, rpc))) {
        if ((rpc->flags & RPC_HASID))
            rpc_create_error(buf, RPC_METHOD_NOT_FOUND, id, id_len);
        else
            msg_clear(buf, STR_REDUCE);
        if (id && id_len > 0)
            free((void*)id);
        return RPC_METHOD_NOT_FOUND;
    }
    if (id && id_len > 0)
        free((void*)id);
    return rpc_execute_method(buf, method, rpc, userdata);
}

int rpc_execute (strbuf_t *buf, size_t off, rpc_list_t *methods, void *userdata) {
    int rc;
    rpc_t rpc;
    memset(&rpc, 0, sizeof(rpc_t));
    if ((rc = rpc_parse(buf, off, &rpc)) < 0) {
        rpc_clear(&rpc);
        msg_clear(buf, STR_REDUCE);
        return rc;
    }
    if (!(rpc.flags & RPC_REQUEST)) {
        intptr_t id;
        int id_len = rpc_getid(&rpc, 1, &id);
        if ((rpc.flags & RPC_HASID))
            rpc_create_error(buf, RPC_INVALID_REQUEST, id, id_len);
        else
            msg_clear(buf, STR_REDUCE);
        rpc_clear(&rpc);
        return RPC_INVALID_REQUEST;
    }
    rc = rpc_execute_parsed(buf, methods, &rpc, userdata);
    rpc_clear(&rpc);
    return rc;
}
