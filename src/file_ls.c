/*Copyright (c) Brian B.

Permission to use, copy, modify, and/or distribute this software for any purpose
with or without fee is hereby granted.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*/
#include "../include/libex/file.h"

static int enumdir (const char *path, path_h on_path, void *userdata, int flags, int max_depth, int depth) {
    struct stat st;
    DIR *dp;
    if (max_depth > 0 && max_depth <= depth)
        return ENUM_BREAK;
    if (-1 == stat(path, &st))
        return ENUM_BREAK;
    if ((dp = opendir(path))) {
        struct dirent *ep;
        if (ENUM_BREAK == on_path(path, userdata, flags | S_INDIR, &st) && (flags & ENUM_STOP_IF_BREAK))
            return ENUM_BREAK;
        while ((ep = readdir(dp))) {
            str_t *cpath = path_combine(path, ep->d_name, NULL);
            struct stat st;
            if (0 != strcmp(ep->d_name, ".") && 0 != strcmp(ep->d_name, "..") && 0 == stat(cpath->ptr, &st)) {
                if (S_ISDIR(st.st_mode)) {
                    if (ENUM_BREAK == enumdir(cpath->ptr, on_path, userdata, flags, max_depth, depth+1) && (flags & ENUM_STOP_IF_BREAK)) {
                        closedir(dp);
                        return ENUM_BREAK;
                    }
                } else
                    if (ENUM_BREAK == on_path(cpath->ptr, userdata, flags, &st) && (flags & ENUM_STOP_IF_BREAK)) {
                        closedir(dp);
                        return ENUM_BREAK;
                    }
            }
            free(cpath);
        }
        closedir(dp);
        if (ENUM_BREAK == on_path(path, userdata, flags, &st) && (flags & ENUM_STOP_IF_BREAK))
            return ENUM_BREAK;
    }
    return ENUM_CONTINUE;
}

void ls (const char *path, path_h on_path, void *userdata, int flags, int max_depath) {
    enumdir(path, on_path, userdata, flags, max_depath, 0);
}

static int on_rmdir (const char *path, void *userdata, int flags, struct stat *st) {
    if (S_INDIR == flags)
        return ENUM_CONTINUE;
    if (S_ISDIR(st->st_mode))
        rmdir(path);
    else
        unlink(path);
    return ENUM_CONTINUE;
}

void rmdirall (const char *path) {
    enumdir(path, on_rmdir, NULL, 0, 0, 0);
}
