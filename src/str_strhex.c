/*Copyright (c) Brian B.

Permission to use, copy, modify, and/or distribute this software for any purpose
with or without fee is hereby granted.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*/
#include "../include/libex/str.h"

str_t *strhex (const char *prefix, const char *str, size_t str_len, size_t chunk_size) {
    char buf [8];
    size_t prefix_len = prefix ? strlen(prefix) : 0;
    str_t *result = stralloc(2 * str_len + prefix_len, chunk_size);
    if (prefix && prefix_len)
        strnadd(&result, prefix, prefix_len);
    for (size_t i = 0; i < str_len; ++i) {
        snprintf(buf, sizeof buf, "%02x", str[i]);
        strnadd(&result, buf, 2);
    }
    return result;
}
