/*Copyright (c) Brian B.

Permission to use, copy, modify, and/or distribute this software for any purpose
with or without fee is hereby granted.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*/
#include "../include/libex/str.h"

int strbuf_b64decode (strbuf_t *decoded, const char *input, size_t input_len, size_t chunk_size) {
    ssize_t decoded_len = b64_decoded_len(input, input_len);
    if (!decoded->ptr) {
        if (-1 == strbufalloc(decoded, decoded_len, chunk_size))
            return -1;
    } else
    if (-1 == strbufsize(decoded, decoded_len, 0))
        return -1;
    memset(decoded->ptr, 0, decoded->bufsize);
    decoded->len = decoded_len;
    if (-1 == b64_decode(input, input_len, (unsigned char*)decoded->ptr, decoded_len))
        decoded->len = 0;
    return 0 == decoded->len ? -1 : 0;
}
