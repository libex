/*Copyright (c) Brian B.

Permission to use, copy, modify, and/or distribute this software for any purpose
with or without fee is hereby granted.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE
OF THIS SOFTWARE.
*/
#include "../include/libex/json.h"

const char *jsonrpc_version = "1.0";

static int jsonrpc_parse_id (json_item_t *ji, jsonrpc_enum_t *data) {
    if (0 == cmpstr(ji->key.ptr, ji->key.len, CONST_STR_LEN("id"))) {
        if (JSON_INTEGER == ji->type || JSON_STRING == ji->type || JSON_NULL == ji->type) {
            data->jsonrpc->id = ji;
            return 1;
        } else {
            data->errcode = JSONRPC_INVALID_REQUEST;
            return -1;
        }
    }
    return 0;
}

static int jsonrpc_parse_ver (json_item_t *ji, jsonrpc_enum_t *data) {
    if (0 == cmpstr(ji->key.ptr, ji->key.len, CONST_STR_LEN("jsonrpc"))) {
        if (JSON_STRING == ji->type) {
            if (0 == cmpstr(ji->data.s.ptr, ji->data.s.len, CONST_STR_LEN("1.0"))) {
                data->jsonrpc->ver = JSONRPC_V10;
                return 1;
            } else
            if (0 == cmpstr(ji->data.s.ptr, ji->data.s.len, CONST_STR_LEN("2.0"))) {
                data->jsonrpc->ver = JSONRPC_V20;
                return 1;
            } else {
                data->errcode = JSONRPC_INVALID_REQUEST;
                return -1;
            }
        } else {
            data->errcode = JSONRPC_INVALID_REQUEST;
            return -1;
        }
    }
    return 0;
}

static int jsonrpc_parse_method (json_item_t *ji, jsonrpc_enum_t *data) {
    if (0 == cmpstr(ji->key.ptr, ji->key.len, CONST_STR_LEN("method"))) {
        if (JSON_STRING == ji->type) {
            data->jsonrpc->method = ji->data.s;
            return 1;
        } else {
            data->errcode = JSONRPC_METHOD_NOT_FOUND;
            return -1;
        }
    }
    return 0;
}

static int jsonrpc_parse_error_code (json_item_t *ji, jsonrpc_enum_t *data) {
    if (0 == cmpstr(ji->key.ptr, ji->key.len, CONST_STR_LEN("code"))) {
        if (JSON_INTEGER == ji->type) {
            data->jsonrpc->error_code = ji->data.i;
            return 1;
        } else {
            data->errcode = JSONRPC_INVALID_REQUEST;
            return -1;
        }
    }
    return 0;
}

static int jsonrpc_parse_error_message (json_item_t *ji, jsonrpc_enum_t *data) {
    if (0 == cmpstr(ji->key.ptr, ji->key.len, CONST_STR_LEN("message"))) {
        if (JSON_STRING == ji->type) {
            data->jsonrpc->error_message = ji->data.s;
            return 1;
        } else {
            data->errcode = JSONRPC_INVALID_REQUEST;
            return -1;
        }
    }
    return 0;
}

static int on_parse_error (json_item_t *ji, void *userdata) {
    jsonrpc_enum_t *data = userdata;
    if (-1 == jsonrpc_parse_error_code(ji, data))
        return -1;
    if (-1 == jsonrpc_parse_error_message(ji, data))
        return -1;
    if (0 == cmpstr(ji->key.ptr, ji->key.len, CONST_STR_LEN("data")))
        data->jsonrpc->error_data = ji;
    return ENUM_CONTINUE;
}

static int jsonrpc_parse_error (json_item_t *ji, jsonrpc_enum_t *data) {
    if (0 == cmpstr(ji->key.ptr, ji->key.len, CONST_STR_LEN("error"))) {
        if (JSON_NULL == ji->type)
            return 1;
        if (JSON_OBJECT != ji->type) {
            data->errcode = JSONRPC_INVALID_REQUEST;
            return -1;
        }
        foreach_json_object(ji->data.o, on_parse_error, (void*)data, ENUM_STOP_IF_BREAK);
        if (JSONRPC_OK != data->errcode)
            return data->errcode;
        if (!data->jsonrpc->error_code || !data->jsonrpc->error_message.ptr) {
            data->errcode = JSONRPC_INVALID_REQUEST;
            return -1;
        } else
            return 1;
    }
    return 0;
}

static int on_jsonrpc_parse_request (json_item_t *ji, jsonrpc_enum_t *data) {
    int rc;
    if (-1 == (rc = jsonrpc_parse_id(ji, data))) {
        data->errcode = JSONRPC_INVALID_REQUEST;
        return ENUM_BREAK;
    }
    if (rc > 0) return ENUM_CONTINUE;
    if (-1 == (rc = jsonrpc_parse_ver(ji, data))) {
        data->errcode = JSONRPC_INVALID_REQUEST;
        return ENUM_BREAK;
    }
    if (rc > 0) return ENUM_CONTINUE;
    if (-1 == (rc = jsonrpc_parse_method(ji, data))) {
        data->errcode = JSONRPC_INVALID_REQUEST;
        return ENUM_BREAK;
    }
    if (rc > 0) return ENUM_CONTINUE;
    if (0 == cmpstr(ji->key.ptr, ji->key.len, CONST_STR_LEN("params")))
        data->jsonrpc->params = ji;
    return ENUM_CONTINUE;
}

static int on_jsonrpc_parse_response (json_item_t *ji, jsonrpc_enum_t *data) {
    int rc;
    if (-1 == (rc = jsonrpc_parse_id(ji, data)))
        return ENUM_BREAK;
    if (rc > 0) return ENUM_CONTINUE;
    if (-1 == (rc = jsonrpc_parse_error(ji, data)))
        return ENUM_BREAK;
    if (rc > 0) return ENUM_CONTINUE;
    if (0 == cmpstr(ji->key.ptr, ji->key.len, CONST_STR_LEN("result")) && JSON_NULL != ji->type)
        data->jsonrpc->result = ji;
    return ENUM_CONTINUE;
}

static int jsonrpc_parse_request_intr (jsonrpc_t *jsonrpc) {
    int rc = JSONRPC_OK;
    jsonrpc_enum_t data = { .jsonrpc = jsonrpc, .errcode = JSONRPC_OK };
    if (JSON_OBJECT != jsonrpc->json->type) {
        rc = JSONRPC_INVALID_REQUEST;
        goto err;
    }
    foreach_json_object((json_object_t*)jsonrpc->json->data.o,
                        (int(*)(json_item_t*,void*))on_jsonrpc_parse_request,
                        &data, ENUM_STOP_IF_BREAK);
    if (data.errcode != JSONRPC_OK)
        return data.errcode;
    if (!jsonrpc->id || (jsonrpc->id->type != JSON_INTEGER
        && jsonrpc->id->type != JSON_STRING && jsonrpc->id->type != JSON_NULL)
        || jsonrpc->ver == JSONRPC_VNONE || !jsonrpc->method.len)
        return JSONRPC_INVALID_REQUEST;
    if (!jsonrpc->params || jsonrpc->params->type != JSON_ARRAY)
        return JSONRPC_INVALID_PARAMS;
    return JSONRPC_OK;
err:
    if (jsonrpc->json) {
        json_free(jsonrpc->json);
        jsonrpc->json = NULL;
    }
    return rc;
}

int jsonrpc_parse_request_len (const char *json_str, size_t json_str_len, jsonrpc_t *jsonrpc) {
    if (!(jsonrpc->json = json_parse_len(json_str, json_str_len)))
        return JSONRPC_PARSE_ERROR;
    return jsonrpc_parse_request_intr(jsonrpc);
}

int jsonrpc_parse_request (const char *json_str, jsonrpc_t *jsonrpc) {
    if (!(jsonrpc->json = json_parse(json_str)))
        return JSONRPC_PARSE_ERROR;
    return jsonrpc_parse_request_intr(jsonrpc);
}

static int jsonrpc_parse_response_intr (jsonrpc_t *jsonrpc) {
    int rc;
    jsonrpc_enum_t data = { .jsonrpc = jsonrpc, .errcode = JSONRPC_OK };
    if (JSON_OBJECT != jsonrpc->json->type) {
        rc = JSONRPC_INVALID_REQUEST;
        goto err;
    }
    foreach_json_object((json_object_t*)jsonrpc->json->data.o,
                        (int(*)(json_item_t*,void*))on_jsonrpc_parse_response,
                        &data, ENUM_STOP_IF_BREAK);
    if (data.errcode != JSONRPC_OK)
        return data.errcode;
    if (!jsonrpc->id || jsonrpc->ver != JSONRPC_VNONE || (jsonrpc->result
        && jsonrpc->error_message.ptr) || (!jsonrpc->result && !jsonrpc->error_message.ptr))
        return JSONRPC_INVALID_REQUEST;
    return JSONRPC_OK;
err:
    if (jsonrpc->json) {
        json_free(jsonrpc->json);
        jsonrpc->json = NULL;
    }
    return rc;
}
void jsonrpc_setver (jsonrpc_ver_t ver) {
    switch (ver) {
        case JSONRPC_V10: jsonrpc_version = "1.0"; break;
        case JSONRPC_V20: jsonrpc_version = "2.0"; break;
        default: break;
    }
}

int jsonrpc_parse_response_len (const char *json_str, size_t json_str_len, jsonrpc_t *jsonrpc) {
    if (!(jsonrpc->json = json_parse_len(json_str, json_str_len)))
        return JSONRPC_PARSE_ERROR;
    return jsonrpc_parse_response_intr(jsonrpc);
};

int jsonrpc_parse_response (const char *json_str, jsonrpc_t *jsonrpc) {
    if (!(jsonrpc->json = json_parse(json_str)))
        return JSONRPC_PARSE_ERROR;
    return jsonrpc_parse_response_intr(jsonrpc);
};

int jsonrpc_parse (const char *json_str, size_t json_str_len, jsonrpc_t *jsonrpc) {
    int rc;
    if (!(jsonrpc->json = json_parse_len(json_str, json_str_len)))
        return JSONRPC_PARSE_ERROR;
    if (JSONRPC_INVALID_REQUEST == (rc = jsonrpc_parse_request_intr(jsonrpc)))
        rc = jsonrpc_parse_response_intr(jsonrpc);
    if (JSONRPC_INVALID_REQUEST == rc)
        rc = JSONRPC_PARSE_ERROR;
    return rc;
}

static void jsonrpc_id (strbuf_t *buf, intptr_t id, int id_len) {
    if (id_len > 0)
        json_add_str(buf, CONST_STR_LEN("id"), (const char*)id, id_len);
    else
    if (id_len == -1)
        json_add_int(buf, CONST_STR_LEN("id"), id);
    else
    if (id == 0)
        json_add_null(buf, CONST_STR_LEN("id"));
}

void jsonrpc_prepare (strbuf_t *buf) {
    buf->len = 0;
    if (json_prefix_len > 0)
        strbufset(buf, json_prefix, json_prefix_len);
}

int jsonrpc_request (strbuf_t *buf, const char *method, size_t method_len, intptr_t id, int id_len, jsonrpc_h on_params, void *userdata) {
    int rc = 0;
    buf->len = 0;
    if (json_prefix_len > 0)
        strbufset(buf, json_prefix, json_prefix_len);
    json_begin_object(buf);
    json_add_str(buf, CONST_STR_LEN("jsonrpc"), jsonrpc_version, 3);
    json_add_str(buf, CONST_STR_LEN("method"), method, method_len);
    json_open_array(buf, CONST_STR_LEN("params"));
    if (on_params)
        rc = on_params(buf, userdata);
    json_close_array(buf);
    jsonrpc_id(buf, id, id_len);
    json_end_object(buf);
    return rc;
}

#define STRBUF_SIZE 256
int jsonrpc_requestf (strbuf_t *buf, const char *method, size_t method_len, intptr_t id, int id_len, const char *fmt, ...) {
    int n;
    size_t strsize;
    va_list ap;
    char *strbuf = malloc(STRBUF_SIZE);
    if (!strbuf)
        return -1;
    buf->len = 0;
    if (json_prefix_len > 0)
        strbufset(buf, json_prefix, json_prefix_len);
    json_begin_object(buf);
    json_add_str(buf, CONST_STR_LEN("jsonrpc"), jsonrpc_version, 3);
    json_add_str(buf, CONST_STR_LEN("method"), method, method_len);
    json_open_array(buf, CONST_STR_LEN("params"));
    va_start(ap, fmt);
    if ((n = vsnprintf(strbuf, STRBUF_SIZE, fmt, ap)) > STRBUF_SIZE - 1) {
        char *nstrbuf = realloc(strbuf, n + 1);
        if (nstrbuf) {
            strbuf = nstrbuf;
            va_end(ap);
            va_start(ap, fmt);
            strsize = vsnprintf(strbuf, n + 1, fmt, ap);
        } else {
            free(strbuf);
            strbuf = NULL;
        }
    } else
        strsize = n;
    va_end(ap);
    if (!strbuf)
        return -1;
    strbufadd(buf, strbuf, strsize);
    free(strbuf);
    json_close_array(buf);
    jsonrpc_id(buf, id, id_len);
    json_end_object(buf);
    return 0;
}

int jsonrpc_response (strbuf_t *buf, jsonrpc_h on_result, void *userdata, intptr_t id, int id_len) {
    int rc = 0;
    buf->len = 0;
    if (json_prefix_len > 0)
        strbufset(buf, json_prefix, json_prefix_len);
    json_begin_object(buf);
    json_add_key(buf, CONST_STR_LEN("result"));
    if (on_result)
        rc = on_result(buf, userdata);
    json_add_null(buf, CONST_STR_LEN("error"));
    jsonrpc_id(buf, id, id_len);
    json_end_object(buf);
    return rc;
}

int jsonrpc_error (strbuf_t *buf, int code, const char *message, size_t message_len, intptr_t id, int id_len) {
    buf->len = 0;
    if (json_prefix_len > 0)
        strbufset(buf, json_prefix, json_prefix_len);
    json_begin_object(buf);
    json_add_null(buf, CONST_STR_LEN("result"));
    json_open_object(buf, CONST_STR_LEN("error"));
    json_add_int(buf, CONST_STR_LEN("code"), code);
    json_add_str(buf, CONST_STR_LEN("message"), message, 0 == message_len ? strlen(message) : message_len);
    json_close_object(buf);
    jsonrpc_id(buf, id, id_len);
    json_end_object(buf);
    return code;
}

int jsonrpc_stderror (strbuf_t *buf, int code, intptr_t id, int id_len) {
    switch (code) {
        case JSONRPC_PARSE_ERROR:
            jsonrpc_error(buf, JSONRPC_PARSE_ERROR, CONST_STR_LEN(JSONRPC_PARSE_ERROR_STR), id, id_len);
            break;
        case JSONRPC_INVALID_REQUEST:
            jsonrpc_error(buf, JSONRPC_INVALID_REQUEST, CONST_STR_LEN(JSONRPC_INVALID_REQUEST_STR), id, id_len);
            break;
        case JSONRPC_METHOD_NOT_FOUND:
            jsonrpc_error(buf, JSONRPC_METHOD_NOT_FOUND, CONST_STR_LEN(JSONRPC_METHOD_NOT_FOUND_STR), id, id_len);
            break;
        case JSONRPC_INVALID_PARAMS:
            jsonrpc_error(buf, JSONRPC_INVALID_PARAMS, CONST_STR_LEN(JSONRPC_INVALID_PARAMS_STR), id, id_len);
            break;
        case JSONRPC_INTERNAL_ERROR:
            jsonrpc_error(buf, JSONRPC_INTERNAL_ERROR, CONST_STR_LEN(JSONRPC_INTERNAL_ERROR_STR), id, id_len);
            break;
        default:
            break;
    }
    return code;
}

int jsonrpc_getid (jsonrpc_t *jsonrpc, intptr_t *id, int *id_len) {
    if (!jsonrpc->id)
        return -1;
    switch (jsonrpc->id->type) {
        case JSON_INTEGER:
            *id = jsonrpc->id->data.i;
            *id_len = -1;
            return 0;
        case JSON_STRING:
            *id = (intptr_t)strndup(jsonrpc->id->data.s.ptr, jsonrpc->id->data.s.len);
            *id_len = jsonrpc->id->data.s.len;
            return 0;
        case JSON_NULL:
            *id = 0;
            *id_len = 0;
            return 0;
        default:
            return -1;
    }
}

typedef struct {
    json_item_t **params;
    size_t params_len;
} list_to_array_t;

static int on_list_to_array (json_item_t *li, list_to_array_t *la) {
    la->params[la->params_len++] = li;
    return ENUM_CONTINUE;
}

DECLARE_SORTED_ARRAY_FUN(jsonrpc_method_list, jsonrpc_method_t)
__thread jsonrpc_method_list_t *jsonrpc_method_list = NULL;

static int on_json_method_compare (jsonrpc_method_t *x, jsonrpc_method_t *y) {
    return cmpstr(x->method.ptr, x->method.len, y->method.ptr, y->method.len);
}

jsonrpc_method_list_t *jsonrpc_init (void) {
    jsonrpc_method_list_t *l = create_jsonrpc_method_list(64, 64, CF_UNIQUE);
    l->on_compare = on_json_method_compare;
    return l;
}

void jsonrpc_done (jsonrpc_method_list_t **methods) {
    if (methods && *methods)
        free_jsonrpc_method_list(methods);
}

int jsonrpc_add_method (jsonrpc_method_list_t **methods,
                        int id,
                        const char *method,
                        size_t len,
                        jsonrpc_method_h handle,
                        int *param_types, size_t param_len) {
    jsonrpc_method_t m = { .id = id,
                           .method = { .ptr = (char*)method, .len = len },
                           .handle = handle,
                           .param_types = param_types,
                           .param_lens = param_len };
    add_jsonrpc_method_list(methods, &m);
    return EEXIST == errno ? -1 : 0;
}

jsonrpc_method_t *jsonrpc_find_method (jsonrpc_method_list_t *methods, jsonrpc_t *jsonrpc) {
    jsonrpc_method_t mf = { .method = { .ptr = jsonrpc->method.ptr, .len = jsonrpc->method.len } };
    return find_jsonrpc_method_list(methods, NULL, &mf);
}

int jsonrpc_execute_method (strbuf_t *buf, jsonrpc_method_t *method, void *userdata, jsonrpc_t *jsonrpc) {
    int is_params_correct = 1, rc, id_len;
    intptr_t id;
    json_array_t *a = jsonrpc->params->data.a;
    if (0 != jsonrpc_getid(jsonrpc, &id, &id_len))
        return jsonrpc_stderror(buf, JSONRPC_PARSE_ERROR, 0, 0);
    list_to_array_t la = { .params = calloc(a->len, sizeof(json_item_t*)), .params_len = 0 };
    json_enum_array(jsonrpc->params->data.a, (json_item_h)on_list_to_array, &la, 0);
    if (method->param_lens > 0 && la.params_len != method->param_lens)
        is_params_correct = 0;
    if (is_params_correct && method->param_types) {
        for (size_t i = 0; i < la.params_len; ++i) {
            if (JSON_ANY == method->param_types[i])
                continue;
            if (JSON_VARG == method->param_types[i])
                break;
            if (0 == (la.params[i]->type & method->param_types[i])) {
                is_params_correct = 0;
                break;
            }
        }
    }
    if (is_params_correct) {
        if (0 == (rc = method->handle(buf, la.params, la.params_len, id, id_len, userdata)))
            rc = method->id;
    } else
        rc = jsonrpc_stderror(buf, JSONRPC_INVALID_PARAMS, id, id_len);
    free(la.params);
    if (id_len > 0)
        free((void*)id);
    return rc;
}

int jsonrpc_execute_parsed (jsonrpc_method_list_t *methods, strbuf_t *buf, size_t off,
                            void *userdata, jsonrpc_t *jsonrpc) {
    int rc;
    jsonrpc_method_t *method;
    if (!(method = jsonrpc_find_method(methods, jsonrpc))) {
        intptr_t id = 0;
        int id_len = 0;
        if (0 == (rc = jsonrpc_getid(jsonrpc, &id, &id_len))) {
            rc = jsonrpc_stderror(buf, JSONRPC_METHOD_NOT_FOUND, id, id_len);
            if (id_len > 0)
                free((void*)id);
        } else
            buf->len = 0;
        return rc;
    }
    return jsonrpc_execute_method(buf, method, userdata, jsonrpc);
}

int jsonrpc_execute (jsonrpc_method_list_t *methods, strbuf_t *buf, size_t off, void *userdata) {
    int rc;
    jsonrpc_t jsonrpc;
    memset(&jsonrpc, 0, sizeof(jsonrpc_t));
    if (JSONRPC_OK == (rc = jsonrpc_parse_request_len(buf->ptr + off, buf->len - off, &jsonrpc)))
        rc = jsonrpc_execute_parsed(methods, buf, off, userdata, &jsonrpc);
    else
        rc = jsonrpc_stderror(buf, rc, 0, 0);
    if (jsonrpc.json)
        json_free(jsonrpc.json);
    return rc;
}
