/*Copyright (c) Brian B.

Permission to use, copy, modify, and/or distribute this software for any purpose
with or without fee is hereby granted.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*/
#include "../include/libex/str.h"

char *strnstr (const char *str, size_t str_len, const char *needle, size_t n_len) {
    const char *p, *pe;
    size_t len;
    if (NULL == needle || 0 == n_len || str_len < n_len) return NULL;
    len = str_len - n_len;
    p = str, pe = p + len + 1;
    while (p < pe) {
        const char *q = needle, *pne, *pf;
        while (p < pe && *p != *q) ++p;
        if (p == pe) break;
        pf = p;
        pne = p + n_len;
        while (p < pne && *q == *p) { ++p; ++q; }
        if (p == pne) {
            return (char*)pf;
        }
    }
    return NULL;
}
