/*Copyright (c) Brian B.

Permission to use, copy, modify, and/or distribute this software for any purpose
with or without fee is hereby granted.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*/
#include "../include/libex/str.h"

typedef unsigned char utf8_t;

static int is_octal_digit (char c) {
    return c >= '0' && 'c' <= '7';
}

static int is_hex_digit(char c) {
    return  (c >= '0' && c <= '9') ||
            (c >= 'A' && c <= 'F') ||
            (c >= 'a' && c <= 'f');
}

static int u8_read_esc_seq (const char *str, uint32_t *dst) {
    char digs[9] = "\0\0\0\0\0\0\0\0";
    int dno = 0, i = 1;
    uint32_t ch = (uint32_t)*str;
    if (*str == 'n') ch = L'\n'; else
    if (*str == 'r') ch = L'\r'; else
    if (*str == 't') ch = L'\t'; else
    if (*str == 'b') ch = L'\b'; else
    if (*str == 'f') ch = L'\f'; else
    if (*str == 'v') ch = L'\v'; else
    if (*str == 'a') ch = L'\a'; else
    if (is_octal_digit(*str)) {
        i = 0;
        do {
            digs[dno++] = str[i++];
        } while (is_octal_digit(str[i]) && dno < 3);
        ch = strtol(digs, NULL, 8);
    } else
    if (*str == 'x') {
        while (is_hex_digit(str[i]) && dno < 2)
            digs[dno++] = str[i++];
        if (dno > 0)
            ch = strtol(digs, NULL, 16);
    } else
    if (*str == 'u') {
        while (is_hex_digit(str[i]) && dno < 4)
            digs[dno++] = str[i++];
        if (dno > 0)
            ch = strtol(digs, NULL, 16);
    } else
    if (*str == 'U') {
        while (is_hex_digit(str[i]) && dno < 8)
            digs[dno++] = str[i++];
        if (dno > 0)
            ch = strtol(digs, NULL, 16);
    }
    *dst = ch;
    return i;
}

static int u8_wc2utf8 (char *dst, uint32_t ch) {
    if (ch < 0x80) {
        dst[0] = (char)ch;
        return 1;
    }
    if (ch < 0x800) {
        dst[0] = (ch>>6) | 0xC0;
        dst[1] = (ch & 0x3F) | 0x80;
        return 2;
    }
    if (ch < 0x10000) {
        dst[0] = (ch>>12) | 0xE0;
        dst[1] = ((ch>>6) & 0x3F) | 0x80;
        dst[2] = (ch & 0x3F) | 0x80;
        return 3;
    }
    if (ch < 0x110000) {
        dst[0] = (ch>>18) | 0xF0;
        dst[1] = ((ch>>12) & 0x3F) | 0x80;
        dst[2] = ((ch>>6) & 0x3F) | 0x80;
        dst[3] = (ch & 0x3F) | 0x80;
        return 4;
    }
    return 0;
}

static size_t utf8size (const wchar_t *str, size_t str_len) {
    size_t r = 0;
    for (int i = 0; i < str_len; ++i) {
        wchar_t c = str[i];
        if (c < 0x80) r += 1;
        if (c < 0x800) r += 2;
        if (c < 0x10000) r += 3;
        if (c < 0x110000) r += 4;
    }
    return r;
}

str_t *str_unescape (const char *src, size_t src_len, size_t chunk_size) {
    str_t *ret = stralloc(src_len, chunk_size);
    const char *e = src + src_len;
    char *buf = ret->ptr, *be = buf + ret->bufsize, temp[4];
    int amt;
    uint32_t ch;
    while (src < e) {
        if (*src == '\\')
            amt = u8_read_esc_seq(++src, &ch);
        else {
            ch = (uint32_t)*src;
            amt = 1;
        }
        src += amt;
        amt = u8_wc2utf8(temp, ch);
        if (amt > (uintptr_t)be - (uintptr_t)buf) {
            if (-1 == strsize(&ret, ret->len + ret->chunk_size, 0)) {
                free(ret);
                return NULL;
            }
            buf = ret->ptr + ret->len;
            be = ret->ptr + ret->bufsize;
        }
        memcpy(buf, temp, amt);
        ret->len += amt;
        buf += amt;
    }
    STR_ADD_NULL(ret);
    return ret;
}

int strbuf_unescape (strbuf_t *strbuf, const char *src, size_t src_len) {
    const char *e = src + src_len;
    char *buf = strbuf->ptr + strbuf->len, *be = buf + strbuf->bufsize, temp[4];
    int amt;
    uint32_t ch;
    while (src < e) {
        if (*src == '\\')
            amt = u8_read_esc_seq(++src, &ch);
        else {
            ch = (uint32_t)*src;
            amt = 1;
        }
        src += amt;
        amt = u8_wc2utf8(temp, ch);
        if (buf + sizeof(temp) < be) {
            if (-1 == strbufsize(strbuf, strbuf->len + strbuf->chunk_size, 0))
                return -1;
            buf = strbuf->ptr + strbuf->len;
            be = strbuf->ptr + strbuf->bufsize;
        }
        memcpy(buf, temp, amt);
        strbuf->len += amt;
        buf += amt;
    }
    strbuf->ptr[strbuf->len] = '\0';
    return 0;
}

static int utf8_decode (const char *str, size_t *i) {
    const utf8_t *s = (const utf8_t*)str;
    int u = *s, l = 1;
    if (isunicode(u)) {
        int a = (u&0x20)? ((u&0x10)? ((u&0x08)? ((u&0x04)? 6 : 5) : 4) : 3) : 2;
        if (a<6 || !(u&0x02)) {
            int b;
            u = ((u<<(a+1))&0xff)>>(a+1);
            for (b = 1; b < a; ++b)
                u = (u<<6)|(s[l++]&0x3f);
        }
    }
    if (i)*i += l;
    return u;
}

str_t *str_escape (const char *src, size_t src_len, size_t chunk_size) {
    str_t *ret = stralloc(src_len, chunk_size);
    int i;
    for (i = 0; i < src_len && src[i] != '\0'; ) {
        char c = src[i];
        if (!isunicode(c)) {
            switch (c) {
                case '\"': strnadd(&ret, CONST_STR_LEN("\\\"")); break;
                case '\\': strnadd(&ret, CONST_STR_LEN("\\\\")); break;
                case '\b': strnadd(&ret, CONST_STR_LEN("\\b")); break;
                case '\f': strnadd(&ret, CONST_STR_LEN("\\f")); break;
                case '\n': strnadd(&ret, CONST_STR_LEN("\\n")); break;
                case '\r': strnadd(&ret, CONST_STR_LEN("\\r")); break;
                case '\t': strnadd(&ret, CONST_STR_LEN("\\t")); break;
                default: strnadd(&ret, &c, sizeof(char)); break;
            }
            i++;
        } else {
            char buf [8];
            size_t l = 0;
            int z = utf8_decode(&src[i], &l);
            snprintf(buf, sizeof buf, "\\u%04x", z);
            if (-1 == strnadd(&ret, buf, strlen(buf))) {
                free(ret);
                return NULL;
            } i += l;
        }
    }
    return ret;
}

typedef wint_t (*ul_conv_h) (wint_t, locale_t);
static void str_ul_conv (char *str, size_t str_len, locale_t locale, ul_conv_h ul_conv) {
    char *p = str;
    size_t i = 0;
    while (i < str_len) {
        size_t clen = 0, plen;
        wint_t wi = utf8_decode(&str[i], &clen);
        i += clen;
        wi = ul_conv(wi, locale);
        plen = u8_wc2utf8(p, wi);
        p += plen;
    }
}

inline void strwupper (char *str, size_t str_len, locale_t locale) {
    str_ul_conv(str, str_len, locale, towupper_l);
}

inline void strwlower (char *str, size_t str_len, locale_t locale) {
    str_ul_conv(str, str_len, locale, towlower_l);
}

wstr_t *str2wstr (const char *str, size_t str_len, size_t chunk_size) {
    wstr_t *ret = wstralloc(str_len, chunk_size);
    size_t i = 0, j = 0;
    while (i < str_len) {
        size_t clen = 0;
        wint_t wi = utf8_decode(&str[i], &clen);
        i += clen;
        ret->ptr[j++] = wi;
    }
    ret->len = str_len;
    return ret;
}

str_t *wstr2str (const wchar_t *str, size_t str_len, size_t chunk_size) {
    size_t i = 0;
    str_t *ret = stralloc(utf8size(str, str_len), chunk_size);
    char *p = ret->ptr;
    while (i < str_len) {
        size_t plen = u8_wc2utf8(p, str[i]);
        p += plen;
    }
    return ret;
}

int strbuf_escape (strbuf_t *dst, const char *src, size_t src_len) {
    int i;
    for (i = 0; i < src_len && src[i] != '\0'; ) {
        char c = src[i];
        if (!isunicode(c)) {
            switch (c) {
                case '\"': strbufadd(dst, CONST_STR_LEN("\\\"")); break;
                case '\\': strbufadd(dst, CONST_STR_LEN("\\\\")); break;
                case '\b': strbufadd(dst, CONST_STR_LEN("\\b")); break;
                case '\f': strbufadd(dst, CONST_STR_LEN("\\f")); break;
                case '\n': strbufadd(dst, CONST_STR_LEN("\\n")); break;
                case '\r': strbufadd(dst, CONST_STR_LEN("\\r")); break;
                case '\t': strbufadd(dst, CONST_STR_LEN("\\t")); break;
                default: strbufadd(dst, &c, sizeof(char)); break;
            }
            i++;
        } else {
            char buf [8];
            size_t l = 0;
            int z = utf8_decode(&src[i], &l);
            snprintf(buf, sizeof buf, "\\u%04x", z);
            if (-1 == strbufadd(dst, buf, strlen(buf)))
                return -1;
            i += l;
        }
    }
    return 0;
}
