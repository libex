/*Copyright (c) Brian B.

Permission to use, copy, modify, and/or distribute this software for any purpose
with or without fee is hereby granted.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*/
#include "../include/libex/file.h"

str_t *path_expand (const char *where, size_t where_len, const char *path, size_t path_len) {
    const char *p;
    size_t len;
    int fin = -1;

    int patch_path (void) {
        len = (uintptr_t)p - (uintptr_t)path;
        if (1 == len && '.' == *path) {
            path = p+1;
            path_len -= len+1;
            return 1;
        }
        if (2 == len && '.' == *(path+1)) {
            path = p+1;
            path_len -= len+1;
            if (!(p = strnrchr(where, '/', where_len)))
                return -1;
            where_len = (uintptr_t)p - (uintptr_t)where;
            return 1;
        }
        return 0;
    }

    if ('/' == *path)
        return mkstr(path, path_len, 16);
    if ((p = (const char*)strnchr(path, '/', path_len))) {
        if (0 < (fin = patch_path()))
        while ((p = (const char*)strnchr(path, '/', path_len)))
            if (0 >= (fin = patch_path()))
                break;
    } else {
        p = path;
        fin = 0;
    }
    str_t *str = NULL;
    if (-1 == fin)
        return NULL;
    if (!(str = mkstr(where, where_len, 16)))
        return NULL;
    if (-1 == strnadd(&str, CONST_STR_LEN("/")) || -1 == strnadd(&str, path, path_len)) {
        free(str);
        return NULL;
    }
    STR_ADD_NULL(str);
    return str;
}

