/*Copyright (c) Brian B.

Permission to use, copy, modify, and/or distribute this software for any purpose
with or without fee is hereby granted.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*/
#include "../include/libex/str.h"

cstr_t *vcstrfmt (const char *fmt, va_list args) {
    int len;
    va_list copy;
    cstr_t *res = NULL;
    va_copy(copy, args);
    if ((len = vsnprintf(NULL, 0, fmt, args)) >= 0) {
        if (!(res = malloc(sizeof(cstr_t) + len + 1)) || vsnprintf(res->ptr, len+1, fmt, copy) < 0) {
            if (res) free(res);
            res = NULL;
        } else
            res->len = len;
    }
    va_end(copy);
    return res;
}

__attribute__ ((format (printf, 1, 2)))
cstr_t *cstrfmt (const char *fmt, ...) {
    va_list args;
    va_start(args, fmt);
    cstr_t *res = vcstrfmt(fmt, args);
    va_end(args);
    return res;
}
