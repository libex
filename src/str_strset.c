/*Copyright (c) Brian B.

Permission to use, copy, modify, and/or distribute this software for any purpose
with or without fee is hereby granted.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*/
#include "../include/libex/str.h"

void *mkstrset (size_t count, const char **strs) {
    void *ret = NULL, *p;
    size_t *lens = (size_t*)alloca(count * sizeof(size_t)), len = 0;
    for (size_t i = 0; i < count; ++i) {
        lens[i] = strlen(strs[i]);
        len += lens[i];
    }
    size_t size = 2 * sizeof(size_t) + sizeof(size_t) * count + len + len + sizeof(size_t) + 1;
    if (!(ret = p = malloc(size))) return NULL;
    memset(ret, 0, size);
    *((size_t*)p) = size;
    p += sizeof(size_t);
    *((size_t*)p) = count;
    p += sizeof(size_t);
    for (int i = 0; i < count; ++i) {
        size_t l = lens[i];
        *((size_t*)p) = l;
        p += sizeof(size_t);
        memcpy(p, strs[i], l+1);
        p += l + 1;
    }
    *((size_t*)p) = 0;
    p += sizeof(size_t);
    *((char*)p) = 0;
    return ret;
}

void *mkstrset2 (const char **strs, size_t count, size_t *idxs) {
    void *ret = NULL, *p;
    size_t *lens = (size_t*)alloca(count * sizeof(size_t)+sizeof(size_t)), len = 0;
    for (size_t i = 0; i < count; ++i) {
        lens[i] = strlen(strs[idxs[i]]);
        len += lens[i];
    }
    size_t size = 2 * sizeof(size_t) + sizeof(size_t) * count + len + len + sizeof(size_t) + 1;
    if (!(ret = p = malloc(size))) return NULL;
    memset(ret, 0, size);
    *((size_t*)p) = size;
    p += sizeof(size_t);
    *((size_t*)p) = count;
    p += sizeof(size_t);
    for (int i = 0; i < count; ++i) {
        size_t l = lens[i];
        *((size_t*)p) = l;
        p += sizeof(size_t);
        memcpy(p, strs[idxs[i]], l+1);
        p += l + 1;
    }
    *((size_t*)p) = 0;
    p += sizeof(size_t);
    *((char*)p) = 0;
    return ret;
}

void *strset_start (void *strset, size_t *len) {
    size_t l = *((size_t*)(strset + sizeof(size_t)));
    if (0 >= l)
        return NULL;
    if (len)
        *len = l;
    return strset + 2 * sizeof(size_t);
}

strptr_t strset_fetch (void **strset_ptr) {
    void *p = *strset_ptr;
    strptr_t ret = { .len = *((size_t*)p), .ptr = (char*)(p + sizeof(size_t)) };
    p += sizeof(size_t) + ret.len + 1;
    if (0 == *((size_t*)p) && 0 == *((char*)(p+sizeof(size_t))))
        *strset_ptr = NULL;
    else
        *strset_ptr = p;
    return ret;
}

strptr_t strset_get (void *strset, size_t index) {
    strptr_t ret = { .len = 0, .ptr = NULL };
    size_t size = *((size_t*)strset), count;
    strset += sizeof(size_t);
    count = *((size_t*)strset);
    strset += sizeof(size_t);
    if (index < count) {
        for (size_t i = 0; i < index; ++i) {
            size_t len = *((size_t*)strset);
            strset += sizeof(size_t) + len + 1;
        }
        ret.len = *((size_t*)strset);
        ret.ptr = (char*)(strset + sizeof(size_t));
    } else
        strset = strset + size - sizeof(size_t) - 1;
    return ret;
}
