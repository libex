/*Copyright (c) Brian B.

Permission to use, copy, modify, and/or distribute this software for any purpose
with or without fee is hereby granted.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*/
#include "../include/libex/file.h"

str_t *path_add_path (str_t *path, const char *arg, ...) {
    va_list ap;
    va_start(ap, arg);
    if (arg) {
        const char *p = arg, *q;
        if (path->len == 0 || PATH_DELIM_C != path->ptr[path->len-1])
            strnadd(&path, CONST_STR_LEN(PATH_DELIM_S));
        q = arg;
        while (*q && *q == PATH_DELIM_C) ++q;
        if ('\0' != *q)
            strnadd(&path, q, strlen(q));
        while ((p = va_arg(ap, const char*))) {
            if (path->len == 0 || PATH_DELIM_C != path->ptr[path->len-1])
                strnadd(&path, CONST_STR_LEN(PATH_DELIM_S));
            while (*q && *q == PATH_DELIM_C) ++q;
            if ('\0' != *q)
                strnadd(&path, q, strlen(q));
        }
    }
    va_end(ap);
    STR_ADD_NULL(path);
    return path;
}
