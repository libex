/*Copyright (c) Brian B.

Permission to use, copy, modify, and/or distribute this software for any purpose
with or without fee is hereby granted.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*/
#include "../include/libex/str.h"

int strbufadd (strbuf_t *strbuf, const char *src, size_t src_len) {
    char *buf = strbuf->ptr;
    size_t nstr_len = strbuf->len + src_len;
    errno = 0;
    if (nstr_len >= strbuf->bufsize) {
        size_t nbufsize = (nstr_len / strbuf->chunk_size) * strbuf->chunk_size + strbuf->chunk_size;
        buf = realloc(buf, nbufsize);
        if (!buf) return -1;
        errno = ERANGE;
        strbuf->ptr = buf;
        strbuf->bufsize = nbufsize;
    }
    memcpy(strbuf->ptr + strbuf->len, src, src_len);
    strbuf->len = nstr_len;
    return 0;
}
