/*Copyright (c) Brian B.

Permission to use, copy, modify, and/or distribute this software for any purpose
with or without fee is hereby granted.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*/
#include "../include/libex/str.h"

int strnadd (str_t **str, const char *src, size_t src_len) {
    str_t *s = *str;
    size_t nstr_len = s->len + src_len;
    errno = 0;
    if (nstr_len >= s->bufsize) {
        size_t nbufsize = (nstr_len / s->chunk_size) * s->chunk_size + s->chunk_size;
        str_t *nstr = realloc(s, sizeof(str_t) + nbufsize);
        if (!nstr) return -1;
        errno = ERANGE;
        s = *str = nstr;
        s->bufsize = nbufsize;
    }
    memcpy(s->ptr + s->len, src, src_len);
    s->len = nstr_len;
    STR_ADD_NULL(*str);
    return 0;
}
