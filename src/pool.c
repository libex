/*Copyright (c) Brian B.

Permission to use, copy, modify, and/or distribute this software for any purpose
with or without fee is hereby granted.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*/
#include "../include/libex/task.h"

DECLARE_LIST_FUN(pool_slot_list,pool_slot_t)
DECLARE_LIST_FUN(pool_msg_list,pool_msg_t)

int pool_setopt_int (pool_t *pool, pool_opt_t opt, long arg) {
    if (pool->is_alive)
        return -1;
    switch (opt) {
        case POOL_MAXSLOTS:
            if (arg <= 0)
                pool->max_slots = sysconf(_SC_NPROCESSORS_ONLN);
            else
                pool->max_slots = arg;
            return 0;
        case POOL_TIMEOUT:
            if (arg < 0)
                return -1;
            pool->timeout = arg;
            return 0;
        case POOL_LIVINGTIME:
            if (arg < 0)
                return -1;
            pool->livingtime = arg;
            return 0;
        default: return -1;
    }
}

int pool_setopt_msg (pool_t *pool, pool_opt_t opt, pool_msg_h arg) {
    if (pool->is_alive)
        return -1;
    switch (opt) {
        case POOL_MSG:
            pool->on_msg = arg;
            return 0;
        default: return -1;
    }
}

int pool_setopt_create (pool_t *pool, pool_opt_t opt, pool_create_h arg) {
    if (pool->is_alive)
        return -1;
    switch (opt) {
        case POOL_CREATESLOT:
            pool->on_create_slot = arg;
            return 0;
        default: return -1;
    }
}

int pool_setopt_destroy (pool_t *pool, pool_opt_t opt, pool_destroy_h arg) {
    if (pool->is_alive)
        return -1;
    switch (opt) {
        case POOL_DESTROYSLOT:
            pool->on_destroy_slot = arg;
            return 0;
        case POOL_FREEMSG:
            pool->on_freemsg = arg;
            return 0;
        default: return -1;
    }
}

pool_t *pool_create () {
    pool_t *pool = calloc(1, sizeof(pool_t));
    return pool;
}

static pool_msg_t *get_next_msg (pool_slot_t *slot, pool_t *pool) {
    pool_msg_t *msg = NULL;
    pthread_mutex_lock(&pool->locker);
    while (slot->is_alive && !msg) {
        if ((msg = dequeue_pool_msg_list(pool->queue)))
            continue;
        else
        if (0 == pool->livingtime && 0 == pool->timeout)
            pthread_cond_wait(&pool->cond, &pool->locker);
        else {
            struct timespec totime;
            clock_gettime(CLOCK_MONOTONIC, &totime);
            totime.tv_sec += pool->timeout;
            if (ETIMEDOUT == pthread_cond_timedwait(&pool->cond, &pool->locker, &totime)) {
                if (pool->livingtime > 0)
                    slot->is_alive = 0;
                else if (pool->on_msg)
                    pool->on_msg(NULL, NULL);
            }
        }
    }
    pthread_mutex_unlock(&pool->locker);
    return msg;
}

typedef struct {
    pool_slot_t *slot;
    void *init_data;
} slot_data_t;

static void *slot_process (void *arg) {
    pool_msg_t *msg = NULL;
    pool_slot_t *slot = (pool_slot_t*)arg;
    pool_t *pool = slot->pool;
    if (pool->on_create_slot && -1 == pool->on_create_slot(slot, slot->init_data)) {
        pthread_mutex_lock(&pool->locker);
        del_pool_slot_list(slot, CF_FREE);
        if (pool->on_destroy_slot)
            pool->on_destroy_slot(slot);
        pthread_mutex_unlock(&pool->locker);
        return NULL;
    }
    while ((msg = get_next_msg(slot, pool))) {
        if (msg->on_msg) {
            int msg_rc = msg->on_msg(slot->data, msg);
            if (msg->mutex && msg->cond) {
                pthread_mutex_lock(msg->mutex);
                pthread_cond_signal(msg->cond);
                pthread_mutex_unlock(msg->mutex);
            }
            if (MSG_DONE == msg_rc) {
                if (pool->on_freemsg)
                    pool->on_freemsg(msg->data);
                free_pool_msg_list_item(pool->queue, msg);
            } else
            if (msg_rc == MSG_WAIT) {
                if (-1 == pool_callmsg(pool, msg, slot->init_data)) {
                    if (pool->on_freemsg)
                        pool->on_freemsg(msg->data);
                }
            }
        }
    }
    pthread_mutex_lock(&pool->locker);
    if (pool->on_destroy_slot)
        pool->on_destroy_slot(slot);
    del_pool_slot_list(slot, CF_FREE);
    pthread_mutex_unlock(&pool->locker);
    pthread_barrier_wait(&pool->barrier);
    return NULL;
}

static int add_slot (pool_t *pool, void *init_data) {
    pool_slot_t tslot = { .is_alive = 1, .pool = pool, .init_data = init_data },
           *slot;
    if (!(slot = add_tail_pool_slot_list(pool->slots, &tslot, 0)))
        return -1;
    if (0 != pthread_create(&slot->th, NULL, slot_process, slot)) {
        del_pool_slot_list(slot, CF_FREE);
        return -1;
    }
    pthread_detach(slot->th);
    return 0;
}

void pool_start (pool_t *pool) {
    pool->is_alive = 1;
    pthread_mutex_init(&pool->locker, NULL);
    if (pool->livingtime || pool->timeout) {
        pthread_condattr_init(&pool->cond_attr);
        pthread_condattr_setclock(&pool->cond_attr, CLOCK_MONOTONIC);
        pthread_cond_init(&pool->cond, &pool->cond_attr);
    } else
        pthread_cond_init(&pool->cond, NULL);
    pool->queue = create_pool_msg_list(); // FIXME : on_free
    if (pool->max_slots <= 0)
        pool->max_slots = sysconf(_SC_NPROCESSORS_ONLN);
    pool->slots = create_pool_slot_list();
    if (0 == pool->livingtime)
        for (long i = 0; i < pool->max_slots; ++i)
            add_slot(pool, NULL);
    pool->is_started = 1;
}

pool_msg_t pool_createmsg (pool_msg_h on_msg,
                       void *data,
                       pthread_mutex_t *mutex, pthread_cond_t *cond,
                       unsigned int flags) {
    pool_msg_t msg;
    memset(&msg, 0, sizeof msg);
    msg.data = data;
    msg.on_msg = on_msg;
    msg.mutex = mutex;
    msg.cond = cond;
    msg.flags = flags;
    return msg;
}

int pool_callmsg (pool_t *pool, pool_msg_t *msg, void *init_data) {
    int ret = 0;
    if (!pool->is_alive)
        return -1;
    pthread_mutex_lock(&pool->locker);
    if (pool->livingtime > 0 && (0 == pool->slots->len || (pool->queue->len > 0 && pool->slots->len < pool->max_slots)))
        ret = add_slot(pool, init_data);
    if (0 != ret)
        return ret;
    enqueue_pool_msg_list(pool->queue, msg, 0);
    pthread_cond_broadcast(&pool->cond);
    pthread_mutex_unlock(&pool->locker);
    return ret;
}

int pool_call (pool_t *pool, pool_msg_h on_msg, void *data, void *init_data, unsigned int flags) {
    if ((flags & TASK_WAIT)) {
        pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;
        pthread_cond_t cond = PTHREAD_COND_INITIALIZER;
        pthread_mutex_lock(&mutex);
        pool_msg_t msg = pool_createmsg(on_msg, data, &mutex, &cond, flags);
        int rc = pool_callmsg(pool, &msg, init_data);
        pthread_cond_wait(&cond, &mutex);
        pthread_mutex_unlock(&mutex);
        pthread_cond_destroy(&cond);
        pthread_mutex_destroy(&mutex);
        return rc;
    }
    pool_msg_t msg = pool_createmsg(on_msg, data, NULL, NULL, flags);
    return pool_callmsg(pool, &msg, init_data);
}

static int on_send_stop (pool_slot_t *slot, void *data) {
    slot->is_alive = 0;
    return ENUM_CONTINUE;
}

void pool_destroy (pool_t *pool, int timeout) {
    if (!pool->is_started) {
        free(pool);
        return;
    }
    pthread_mutex_lock(&pool->locker);
    pthread_barrier_init(&pool->barrier, NULL, pool->slots->len+1);
    foreach_pool_slot_list(pool->slots, on_send_stop, NULL, 0);
    pthread_cond_broadcast(&pool->cond);
    pthread_mutex_unlock(&pool->locker);
    pool->is_alive = 0;
    pthread_barrier_wait(&pool->barrier);
    pthread_barrier_destroy(&pool->barrier);
    if (timeout > 0)
        sleep(timeout);
    free_pool_slot_list(pool->slots);
    free_pool_msg_list(pool->queue);
    if (pool->livingtime || pool->timeout)
        pthread_condattr_destroy(&pool->cond_attr);
    pthread_cond_destroy(&pool->cond);
    pthread_mutex_destroy(&pool->locker);
    free(pool);
}
