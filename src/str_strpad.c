/*Copyright (c) Brian B.

Permission to use, copy, modify, and/or distribute this software for any purpose
with or without fee is hereby granted.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*/
#include "../include/libex/str.h"

static int strlpad (str_t **str, size_t nlen, char filler) {
    int ret = strsize(str, nlen, 0);
    if (-1 == ret) return ret;
    str_t *s = *str;
    memset(s->ptr + s->len, filler, nlen - s->len);
    s->len = nlen;
    STR_ADD_NULL(*str);
    return 0;
}

static int strrpad (str_t **str, size_t nlen, char filler) {
    int ret = strsize(str, nlen, 0);
    if (-1 == ret) return -1;
    str_t *s = *str;
    size_t n = nlen - s->len;
    memmove(s->ptr + n, s->ptr, s->len);
    memset(s->ptr, filler, n);
    s->len = nlen;
    STR_ADD_NULL(*str);
    return 0;
}

static int strcpad (str_t **str, size_t nlen, char filler) {
    int ret = strsize(str, nlen, 0);
    if (-1 == ret) return ret;
    str_t *s = *str;
    size_t n = nlen / 2 - s->len / 2;
    memmove(s->ptr + n, s->ptr, s->len);
    memset(s->ptr, filler, n);
    memset(s->ptr + s->len + n, filler, nlen - s->len - n);
    s->len = nlen;
    STR_ADD_NULL(*str);
    return 0;
}

int strpad (str_t **str, size_t nlen, char filler, int flags) {
    str_t *s = *str;
    if ((flags & STR_MAYBE_UTF)) {
        size_t cnt = strwlen(s->ptr, s->len);
        nlen += s->len - cnt;
    }
    if ((*str)->len >= nlen) return 0;
    if ((flags & STR_LEFT)) strlpad(str, nlen, filler); else
    if ((flags & STR_CENTER)) strcpad(str, nlen, filler); else
        strrpad(str, nlen, filler);
    STR_ADD_NULL(*str);
    return 0;
}
