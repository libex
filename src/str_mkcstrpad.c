/*Copyright (c) Brian B.

Permission to use, copy, modify, and/or distribute this software for any purpose
with or without fee is hereby granted.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*/
#include "../include/libex/str.h"

cstr_t *mkcstrpad (const char *str, size_t len, int c, size_t max_len) {
    cstr_t *res;
    if (0 == len)
        len = strlen(str);
    if (len >= max_len)
        return mkcstr(str, max_len);
    if (!(res = malloc(sizeof(size_t) + max_len + 1)))
        return NULL;
    res->len = max_len;
    memcpy(res->ptr, str, len);
    memset(res->ptr + len, c, max_len - len);
    res->ptr[max_len] = '\0';
    return res;
}
