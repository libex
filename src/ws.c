/*Copyright (c) Brian B.

Permission to use, copy, modify, and/or distribute this software for any purpose
with or without fee is hereby granted.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*/
#include "../include/libex/ws.h"

#define WS_RESPONSE "HTTP/1.1 101 Switching Protocols\r\n" \
    "Upgrade: websocket\r\n" \
    "Connection: Upgrade\r\n" \
    "Sec-WebSocket-Accept: %s\r\n\r\n"
#define WS_RESPONSE_LEN sizeof(WS_RESPONSE)-1

static int on_ws_headers (strptr_t *key, strptr_t *val, void *userdata) {
    ws_handshake_t *wsh = (ws_handshake_t*)userdata;
    if (0 == cmpstr(key->ptr, key->len, CONST_STR_LEN(WS_SECKEY))) {
        if (val->len < WS_SECKEY_LEN)
            return HTTP_PARTIAL_LOADED;
        else
        if (val->len > WS_SECKEY_LEN)
            return HTTP_ERROR;
        strncpy(wsh->sec_key, val->ptr, val->len);
    }
    return 0;
}

static void ws_genkey (ws_handshake_t *wsh) {
    libex_sha1_t ctx;
    uint8_t sha1sum [SHA1_LEN];
    strcpy(wsh->sec_key + WS_SECKEY_LEN, WS_UUID);
    libex_sha1_init(&ctx);
    libex_sha1_update(&ctx, (unsigned char*)wsh->sec_key, WS_SECKEY_LEN + WS_UUID_LEN);
    libex_sha1_done(&ctx, sha1sum);
    b64_encode(sha1sum, sizeof(sha1sum), wsh->sec_key, sizeof(wsh->sec_key));
}

int ws_handshake (http_request_t *req, char *buf, size_t buf_len, ws_handshake_t *wsh) {
    http_item_h h = on_http_header;
    on_http_header = on_ws_headers;
    int rc = http_parse_request(req, buf, buf_len, wsh);
    if (HTTP_LOADED == rc)
        ws_genkey(wsh);
    on_http_header = h;
    return rc;
}

void ws_make_response (strbuf_t *buf, ws_handshake_t *wsh) {
    strbufsize(buf, WS_SECKEY_LEN + WS_UUID_LEN + WS_RESPONSE_LEN + 16, 0);
    buf->len = snprintf(buf->ptr, WS_SECKEY_LEN + WS_UUID_LEN + WS_RESPONSE_LEN + 16, WS_RESPONSE, wsh->sec_key);
}

ssize_t ws_buflen (const uint8_t *buf, size_t buflen) {
    ssize_t expected_len;
    if (buflen < WS_HDRLEN)
        return WS_ERROR;
    uint8_t mlen = *(uint8_t*)(buf + sizeof(uint8_t)),
            len = mlen & 0x7f;
    if (len < 126) {
        if ((mlen & 0x80)) {
            expected_len = WS_HDRLEN + WS_MASKLEN + len;
            if (expected_len <= buflen)
                return expected_len;
            return WS_ERROR;
        }
        expected_len = WS_HDRLEN + len;
        if (expected_len <= buflen)
            return expected_len;
        return WS_ERROR;
    }
    if (126 == len) {
        uint16_t blen;
        if (buflen < WS_HDRLEN + sizeof(uint16_t))
            return WS_ERROR;
        blen = htobe16(*(uint16_t*)(buf + WS_HDRLEN));
        //blen = *(uint16_t*)(buf + WS_HDRLEN);
        if ((mlen & 0x80)) {
            expected_len = WS_HDRLEN + sizeof(uint16_t) + WS_MASKLEN + blen;
            if (expected_len <= buflen)
                return expected_len;
            return WS_ERROR;
        }
        expected_len = WS_HDRLEN + sizeof(uint16_t) + blen;
        if (expected_len <= buflen)
            return expected_len;
        return WS_ERROR;
    }
    uint64_t hlen;
    if (buflen < WS_HDRLEN + sizeof(uint64_t))
        return WS_ERROR;
    hlen = htobe64(*(uint64_t*)(buf + 2));
    //hlen = *(uint64_t*)(buf + 2);
    if ((mlen & 0x80)) {
        expected_len = WS_HDRLEN + sizeof(uint64_t) + WS_MASKLEN + hlen;
        if (expected_len <= buflen)
            return expected_len;
        return WS_ERROR;
    }
    expected_len = WS_HDRLEN + sizeof(uint64_t) + hlen;
    if (expected_len <= buflen)
        return expected_len;
    return WS_ERROR;
}

int ws_parse (const char *buf, size_t buf_len, ws_t *ws) {
    if (buf_len < sizeof(wsh_t))
        return WS_WAIT;
    ws->hdr = (ws_hdr_t*)buf;
    uint8_t mlen = WS_BODY_LEN(ws);
    uint64_t l;
    int is_mask = WS_ISMASK(ws);
    if (mlen < 126) {
        ws->type = WS_SMALL;
        ws->len = mlen;
        if (is_mask) {
            l = mlen + sizeof(wsc_small_t);
            if (buf_len < l) return WS_WAIT;
            if (buf_len > l) return WS_TOOBIG;
            ws->ptr = (uint8_t*)buf + sizeof(wsc_small_t);
            ws->mask = ws->hdr->c_small.mask;
            ws_mask(ws);
            return WS_OK;
        }
        l = mlen + sizeof(wss_small_t);
        if (buf_len < l) return WS_WAIT;
        if (buf_len > l) return WS_TOOBIG;
        ws->ptr = (uint8_t*)buf + sizeof(wss_small_t);
        ws->mask = NULL;
        return WS_OK;
    } else
    if (mlen < 65535) {
        ws->type = WS_BIG;
        if (is_mask) {
            ws->len = ws->hdr->c_big.len = htobe16(ws->hdr->c_big.len);
            l = ws->len + sizeof(wsc_big_t);
            if (buf_len < l) return WS_WAIT;
            if (buf_len > l) return WS_TOOBIG;
            ws->ptr = (uint8_t*)buf + sizeof(wsc_big_t);
            ws->mask = ws->hdr->c_big.mask;
            ws_mask(ws);
            return WS_OK;
        }
        ws->len = ws->hdr->s_big.len = htobe16(ws->hdr->s_big.len);
        l = ws->len + sizeof(wss_big_t);
        if (buf_len < l) return WS_WAIT;
        if (buf_len < l) return WS_TOOBIG;
        ws->ptr = (uint8_t*)buf + sizeof(wss_big_t);
        ws->mask = NULL;
        return WS_OK;
    }
    ws->type = WS_HUGE;

    if (is_mask) {
        ws->len = ws->hdr->c_huge.len = htobe64(ws->hdr->c_huge.len);
        l = ws->len + sizeof(wsc_huge_t);
        if (buf_len < l) return WS_WAIT;
        if (buf_len > l) return WS_TOOBIG;
        ws->ptr = (uint8_t*)buf + sizeof(wsc_huge_t);
        ws->mask = ws->hdr->c_huge.mask;
        ws_mask(ws);
        return WS_OK;
    }
    ws->len = ws->hdr->s_huge.len = htobe64(ws->hdr->s_huge.len);
    l = ws->len + sizeof(wss_huge_t);
    if (buf_len < l) return WS_WAIT;
    if (buf_len > l) return WS_TOOBIG;
    ws->ptr = (uint8_t*)buf + sizeof(wss_huge_t);
    ws->mask = NULL;
    return WS_OK;
}

void ws_mask (ws_t *ws) {
    if (!ws->ptr || !ws->len || !ws->mask)
        return;
    for (size_t i = 0; i < ws->len; ++i)
        ws->ptr[i] = ws->ptr[i] ^ ws->mask[i % 4];
}

uint8_t bytes [sizeof(uint8_t)];

#ifdef __GNUC__
__attribute__ ((constructor))
#endif
void libex_ws_init () {
    for (int i = 0; i < sizeof(uint8_t); ++i)
        bytes[i] = i;
}

static void ws_set_header_small (strbuf_t *buf, ws_t *ws, unsigned int flags) {
    if ((flags & WS_MASK)) {
        uint8_t len = buf->len - sizeof(wsc_small_t);
        ws->hdr = (ws_hdr_t*)buf->ptr;
        ws->hdr->h.mlen = 0x80 | len;
        ws->ptr = (uint8_t*)ws->hdr + sizeof(wsc_small_t);
        ws->len = buf->len - sizeof(wsc_small_t);
        ws->mask = ws->hdr->c_small.mask;
        for (int i = 0; i < 4; ++i)
            ws->mask[i] = bytes[rand() % sizeof(uint8_t)];
    } else {
        uint8_t len = buf->len - sizeof(wss_small_t);
        ws->hdr = (ws_hdr_t*)buf->ptr;
        ws->hdr->h.mlen = len;
        ws->ptr = (uint8_t*)ws->hdr + sizeof(wss_small_t);
        ws->len = buf->len - sizeof(wss_small_t);
        ws->mask = 0;
    }
}

static void ws_set_header_big (strbuf_t *buf, ws_t *ws, unsigned int flags) {
    if ((flags & WS_MASK)) {
        uint16_t len = buf->len - sizeof(wsc_big_t);
        ws->hdr = (ws_hdr_t*)buf->ptr;
        ws->hdr->h.mlen = 0x80 | 0x7e;
        ws->ptr = (uint8_t*)ws->hdr + sizeof(wsc_big_t);
        ws->len = buf->len - sizeof(wsc_big_t);
        ws->mask = ws->hdr->c_big.mask;
        ws->hdr->c_big.len = be16toh(len);
        for (int i = 0; i < 4; ++i)
            ws->mask[i] = bytes[rand() % sizeof(uint8_t)];
    } else {
        uint16_t len = buf->len - sizeof(wss_big_t);
        ws->hdr = (ws_hdr_t*)buf->ptr;
        ws->hdr->h.mlen = 0x7e;
        ws->ptr = (uint8_t*)ws->hdr + sizeof(wss_big_t);
        ws->len = buf->len - sizeof(wss_big_t);
        ws->mask = 0;
        ws->hdr->s_big.len = be16toh(len);
    }
}

static void ws_set_header_huge (strbuf_t *buf, ws_t *ws, unsigned int flags) {
    if ((flags & WS_MASK)) {
        uint64_t len = buf->len - sizeof(wsc_huge_t);
        ws->hdr = (ws_hdr_t*)buf->ptr;
        ws->hdr->h.mlen = 0x80 | 0x7f;
        ws->ptr = (uint8_t*)ws->hdr + sizeof(wsc_huge_t);
        ws->len = buf->len - sizeof(wsc_huge_t);
        ws->mask = ws->hdr->c_huge.mask;
        ws->hdr->c_huge.len = be64toh(len);
        for (int i = 0; i < 4; ++i)
            ws->mask[i] = bytes[rand() % sizeof(uint8_t)];
    } else {
        uint64_t len = buf->len - sizeof(wss_huge_t);
        ws->hdr = (ws_hdr_t*)buf->ptr;
        ws->hdr->h.mlen = 0x7f;
        ws->ptr = (uint8_t*)ws->hdr + sizeof(wss_huge_t);
        ws->len = buf->len - sizeof(wss_huge_t);
        ws->mask = 0;
        ws->hdr->s_huge.len = be64toh(len);
    }
}

void ws_set_header (strbuf_t *buf, uint32_t flags, uint8_t opcode) {
    ws_t ws;
    switch ((flags & WS_TYPE)) {
        case WS_SMALL:
            ws_set_header_small(buf, &ws, flags);
            break;
        case WS_BIG:
            ws_set_header_big(buf, &ws, flags);
            break;
        case WS_HUGE:
            ws_set_header_huge(buf, &ws, flags);
            break;
        default:
            if ((flags & WS_MASK)) {
                if (buf->len - sizeof(wsc_small_t) < 126)
                    ws_set_header_small(buf, &ws, flags);
                else
                if (buf->len - sizeof(wsc_big_t) < 65535)
                    ws_set_header_big(buf, &ws, flags);
                else
                    ws_set_header_huge(buf, &ws, flags);
            } else {
                if (buf->len - sizeof(wss_small_t) < 126)
                    ws_set_header_small(buf, &ws, flags);
                else
                if (buf->len - sizeof(wss_big_t) < 65535)
                    ws_set_header_big(buf, &ws, flags);
                else
                    ws_set_header_huge(buf, &ws, flags);
            }
    }
    ws.hdr->h.b0 = opcode;
    if ((flags & WS_FIN))
        ws.hdr->h.b0 |= WS_FIN;
}

typedef void (*ws_set_header_h) (strbuf_t*, ws_t*, unsigned int);
void ws_create (strbuf_t *buf, uint32_t flags, uint8_t opcode) {
    ws_t ws;
    int ofs;
    ws_set_header_h set_header;
    if (buf->len < 126) {
        flags |= WS_SMALL;
        if ((flags & WS_MASK))
            ofs = sizeof(wsc_small_t);
        else
            ofs = sizeof(wss_small_t);
        set_header = ws_set_header_small;
    } else
    if (buf->len < 65535) {
        flags |= WS_BIG;
        if ((flags & WS_MASK))
            ofs = sizeof(wsc_big_t);
        else
            ofs = sizeof(wss_big_t);
        set_header = ws_set_header_big;
    } else {
        flags |= WS_HUGE;
        if ((flags & WS_MASK))
            ofs = sizeof(wsc_huge_t);
        else
            ofs = sizeof(wss_huge_t);
        set_header = ws_set_header_huge;
    }
    strbufsize(buf, buf->len + ofs, 0);
    memmove(buf->ptr + ofs, buf->ptr, buf->len);
    buf->len += ofs;
    set_header(buf, &ws, flags);
    ws.hdr->h.b0 = opcode;
    if ((flags & WS_MASK)) {
        *(int32_t*)ws.mask = rand();
        for (int i = 0; i < ws.len; ++i)
            ws.ptr[i] = ws.ptr[i] ^ ws.mask[i % 4];
    }
    if ((flags & WS_FIN))
        ws.hdr->h.b0 |= WS_FIN;
}
