/*Copyright (c) Brian B.

Permission to use, copy, modify, and/or distribute this software for any purpose
with or without fee is hereby granted.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF
THIS SOFTWARE.
*/
#include "../include/libex/str.h"

str_t *strsplit (const char *s, size_t len, char delim) {
    str_t *res = mkstr(s, 0 == len ? strlen(s) : len, 0);
    if (!res) return NULL;
    char *p = res->ptr, *e = res->ptr + res->len;
    while (p < e) {
        if (*p == delim) *p = '\0';
        ++p;
    }
    return res;
}

int strnext (str_t *str, strptr_t *entry) {
    char *e = str->ptr + str->len, *s, *q;
    if (!entry) return -1;
    if (entry->ptr)
        s = entry->ptr + entry->len + 1;
    else
        s = str->ptr;
    if (s > e) return -1;
    q = s;
    while (q < e && '\0' != *q) ++q;
    entry->ptr = s;
    entry->len = (uintptr_t)q - (uintptr_t)s;
    return 0;
}

int strcount (str_t *str) {
    int rc = 0;
    char *p = str->ptr, *e = str->ptr + str->len;
    while (p <= e) {
        if ('\0' == *p) ++rc;
        ++p;
    }
    return rc;
}
