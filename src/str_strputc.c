/*Copyright (c) Brian B.

Permission to use, copy, modify, and/or distribute this software for any purpose
with or without fee is hereby granted.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*/
#include "../include/libex/str.h"

char *strputc (char *old_str, const char *new_str) {
    char *res = old_str;
    if (!res) {
        if (new_str)
            return strdup(new_str);
        else
            return NULL;
    } else
    if (!new_str)
        return res;
    size_t old_len = strlen(old_str), new_len = strlen(new_str);
    if (new_len > old_len) {
        char *p = realloc(res, new_len+1);
        if (!p)
            return NULL;
        res = p;
    }
    strcpy(res, new_str); // FIXME ?
    return res;
}
