/*Copyright (c) Brian B.

Permission to use, copy, modify, and/or distribute this software for any purpose
with or without fee is hereby granted.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*/
#include "../include/libex/str.h"

int strntok (char **str, size_t *str_len, const char *sep, size_t sep_len, strptr_t *ret) {
    char *p = *str, *e = *str + *str_len, *q;
    if (p >= e)
        return -1;
    while (p < e && strnchr(sep, *p, sep_len))
        ++p;
    q = p;
    while (q < e && !strnchr(sep, *q, sep_len))
        ++q;
    ret->ptr = p;
    ret->len = (uintptr_t)q - (uintptr_t)p;
    if (ret->len > 0)
        *ret = strntrim(ret->ptr, ret->len);
    ++q;
    while (q < e && strchr(sep, *q))
        ++q;
    *str = q;
    *str_len = e < q ? 0 : (uintptr_t)e - (uintptr_t)q;
    if (*str_len > 0) return 0;
    if (*str_len == 0) return 1;
    return -1;
}
