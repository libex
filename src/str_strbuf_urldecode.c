/*Copyright (c) Brian B.

Permission to use, copy, modify, and/or distribute this software for any purpose
with or without fee is hereby granted.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*/
#include "../include/libex/str.h"

int strbuf_url_decode (strbuf_t *decoded, const char *str, size_t str_len, size_t chunk_size) {
    char num [] = "0x0__";
    for (size_t i = 0; i < str_len; ++i) {
        if (str[i] == '%') {
            char c, *tail;
            num[3] = str[++i];
            num[4] = str[++i];
            c = strtol(num, &tail, 16);
            if (*tail || errno == ERANGE) return -1;
            if (-1 == strbufadd(decoded, &c, sizeof(char))) return -1;
        } else {
            if (-1 == strbufadd(decoded, &str[i], sizeof(char))) return -1;
        }
    }
    return 0;
}
