/*Copyright (c) Brian B.

Permission to use, copy, modify, and/or distribute this software for any purpose
with or without fee is hereby granted.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*/
#include "../include/libex/qdb.h"

#define QDB_STACK_TRUNC 0x00000001
#define QDB_STATIC 0x00000002

static int write_default_header (int fd, size_t default_size) {
    int rc;
    qdb_stack_t hdr = { .head = sizeof(qdb_stack_t), .tail = sizeof(qdb_stack_t), .size = default_size };
    if (0 == (rc = flock(fd, LOCK_EX))) {
        rc = sizeof(qdb_stack_t) == write(fd, &hdr, sizeof(qdb_stack_t)) ? 0 : -1;
        flock(fd, LOCK_UN);
    }
    return rc;
}

int qdb_open (const char *fname) {
    return qdb_copen(fname, 0);
}

static int load_header (int fd, qdb_stack_t *hdr, int flags) {
    off_t fsize, fcur;
    if (-1 == (fsize = lseek(fd, 0, SEEK_END))) return -1;
    if (-1 == (fcur = lseek(fd, 0, SEEK_SET))) return -1;
    if (sizeof(qdb_stack_t) != read(fd, hdr, sizeof(qdb_stack_t))) return -1;
    if ((flags & QDB_STACK_TRUNC) && hdr->head == hdr->tail && (fcur + sizeof(qdb_stack_t) < fsize)) {
        if (-1 == lseek(fd, 0, SEEK_SET)) return -1;
        hdr->head = hdr->tail = sizeof(qdb_stack_t);
        if (sizeof(qdb_stack_t) != write(fd, hdr, sizeof(qdb_stack_t))) return -1;
        if (-1 == ftruncate(fd, sizeof(qdb_stack_t))) return -1;
    }
    return 0;
}

static int update_header (int fd, qdb_stack_t *hdr) {
    if (-1 == lseek(fd, 0, SEEK_SET)) return -1;
    return sizeof(qdb_stack_t) == write(fd, hdr, sizeof(qdb_stack_t)) ? 0 : -1;
}

static ssize_t _put (int fd, const char *s, size_t len) {
    uint32_t l = len;
    ssize_t rc;
    if (-1 == write(fd, &l, sizeof(uint32_t)))
        return 0;
    if (-1 == (rc = write(fd, s, len)))
        return sizeof(uint32_t);
    return rc + sizeof(uint32_t);
}

int qdb_put (int fd, const void *data, size_t size) {
    int rc = -1;
    qdb_stack_t hdr;
    ssize_t wrote;
    if (-1 == flock(fd, LOCK_EX)) return -1;
    if (-1 == load_header(fd, &hdr, 1)) goto done;
    if (-1 == lseek(fd, hdr.tail, SEEK_SET)) goto done;
    if (-1 == (wrote = _put(fd, data, size))) goto done;
    hdr.tail += wrote;
    if (-1 == update_header(fd, &hdr)) goto done;
    rc = 0;
done:
    flock(fd, LOCK_UN);
    return rc;
}

static ssize_t _get (int fd, strbuf_t *buf) {
    uint32_t l;
    ssize_t readed;
    if (sizeof(uint32_t) != read(fd, &l, sizeof(uint32_t)))
        return 0;
    if (buf->ptr)
        strbufsize(buf, l, 0);
    else
        strbufalloc(buf, l, l);
    readed = read(fd, buf->ptr, l);
    if (-1 == readed)
        return sizeof(uint32_t);
    buf->len = readed;
    return sizeof(uint32_t) + buf->len;
}

int qdb_get (int fd, strbuf_t *buf) {
    int rc = -1;
    qdb_stack_t hdr;
    ssize_t readed;
    if (-1 == flock(fd, LOCK_EX)) return -1;
    if (-1 == load_header(fd, &hdr, 0)) goto done;
    if (EAGAIN == (errno = hdr.head < hdr.tail ? 0 : EAGAIN)) goto done;
    if (-1 == lseek(fd, hdr.head, SEEK_SET)) goto done;
    if (-1 == (readed = _get(fd, buf))) goto done;
    hdr.head += readed;
    if (-1 == (rc = update_header(fd, &hdr))) goto done;
done:
    flock(fd, LOCK_UN);
    return rc;
}

int qdb_copen (const char *fname, size_t default_size) {
    struct stat st;
    if (-1 != stat(fname, &st) && S_IFREG == (st.st_mode & S_IFMT))
        return open(fname, O_RDWR);
    if (ENOENT == errno) {
        int fd = open(fname, O_CREAT | O_TRUNC | O_RDWR, 0644);
        if (-1 == fd) return -1;
        if (-1 == write_default_header(fd, default_size)) {
            close(fd);
            return -1;
        }
        return fd;
    }
    return -1;
}

int qdb_cput (int fd, const void *data) {
    int rc = -1;
    qdb_stack_t hdr;
    if (-1 == flock(fd, LOCK_EX)) return -1;
    if (-1 == load_header(fd, &hdr, 1)) goto done;
    if (-1 == lseek(fd, hdr.tail, SEEK_SET)) goto done;
    if (hdr.size != write(fd, data, hdr.size)) goto done;
    hdr.tail += hdr.size;
    if (-1 == update_header(fd, &hdr)) goto done;
    rc = 0;
done:
    flock(fd, LOCK_UN);
    return rc;
}

int qdb_cget (int fd, void *data, int flags) {
    int rc = -1;
    qdb_stack_t hdr;
    if (-1 == flock(fd, LOCK_EX)) return -1;
    if (-1 == load_header(fd, &hdr, 0)) goto done;
    if (EAGAIN == (errno = hdr.head < hdr.tail ? 0 : EAGAIN)) goto done;
    if (-1 == (rc = lseek(fd, hdr.head, SEEK_SET))) goto done;
    if (data) {
        if (-1 == (rc = hdr.size == read(fd, data, hdr.size) ? 0 : -1)) goto done;
    }
    if (QDB_PEEK != flags) {
        hdr.head += hdr.size;
        if (-1 == (rc = update_header(fd, &hdr))) goto done;
    }
done:
    flock(fd, LOCK_UN);
    return rc;
}

int qdb_stat (const char *fname, qdb_stack_t *hdr) {
    int fd, rc;
    if (-1 == (fd = qdb_open(fname))) return -1;
    if (-1 == (rc = flock(fd, LOCK_EX))) goto done;
    rc = load_header(fd, hdr, 0);
    flock(fd, LOCK_UN);
done:
    close(fd);
    return rc;
}
