/*Copyright (c) Brian B.

Permission to use, copy, modify, and/or distribute this software for any purpose
with or without fee is hereby granted.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*/
#include "../include/libex/str.h"

void vstrbufmt (strbuf_t *buf, const char *fmt, va_list args) {
    int len;
    va_list ap;
    va_copy(ap, args);
    if ((len = vsnprintf(NULL, 0, fmt, ap)) > 0 && !strbufsize(buf, buf->len + len, 0)) {
        vsnprintf(buf->ptr+len, len+1, fmt, ap);
        buf->len += len;
    }
    va_end(ap);
}

void strbufmt (strbuf_t *buf, const char *fmt, ...) {
    va_list args;
    va_start(args, fmt);
    vstrbufmt(buf, fmt, args);
    va_end(args);
}
