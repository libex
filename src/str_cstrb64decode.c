/*Copyright (c) Brian B.

Permission to use, copy, modify, and/or distribute this software for any purpose
with or without fee is hereby granted.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*/
#include "../include/libex/str.h"

cstr_t *cstr_b64decode (const char *buf, size_t bufsize) {
    cstr_t *res;
    ssize_t decoded_len = b64_decoded_len(buf, bufsize);
    if (!(res = mkcstrfill(decoded_len, 0)))
        return NULL;
    res->len = decoded_len;
    if (-1 == b64_decode(buf, bufsize, (unsigned char*)res->ptr, decoded_len)) {
        free(res);
        res = NULL;
    }
    return res;
}
