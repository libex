/*Copyright (c) Brian B.

Permission to use, copy, modify, and/or distribute this software for any purpose
with or without fee is hereby granted.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*/
#include "../include/libex/str.h"

str_t *hexstr (const char *str, size_t str_len, size_t chunk_size) {
    char buf [3] = {0,0,0}, *tail;
    const char *p = str, *e = p + str_len;
    str_t *res = stralloc(str_len / 2, chunk_size);
    errno = 0;
    while (p < e) {
        buf[0] = *p++;
        if (p == e) {
            errno = ERANGE;
            return res;
        }
        buf[1] = *p++;
        buf[2] = '\0';
        int n = strtol(buf, &tail, 16);
        if ('\0' != *tail || ERANGE == errno) {
            errno = ERANGE;
            return res;
        }
        buf[0] = n;
        buf[1] = '\0';
        strnadd(&res, buf, 1);
    }
    return res;
}
