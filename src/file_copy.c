/*Copyright (c) Brian B.

Permission to use, copy, modify, and/or distribute this software for any purpose
with or without fee is hereby granted.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*/
#include "../include/libex/file.h"

int copy_file (const char *src, const char *dst) {
    int fd_src = open(src, O_RDONLY), rc = -1;
    if (fd_src > 0) {
        struct stat st;
        if (-1 != fstat(fd_src, &st)) {
            int fd_dst = open(dst, O_WRONLY | O_CREAT | O_TRUNC, S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP | S_IROTH | S_IWOTH);
            if (fd_dst > 0) {
                ssize_t readed;
                char buf [4096];
                while ((readed = read(fd_src, buf, sizeof(buf))) > 0)
                    if (-1 == (rc = write(fd_dst, buf, readed)))
                        break;
                close(fd_dst);
                rc = rc < 0 ? -1 : 0;
            }
        }
        close(fd_src);
    }
    return rc;
}
