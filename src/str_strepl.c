/*Copyright (c) Brian B.

Permission to use, copy, modify, and/or distribute this software for any purpose
with or without fee is hereby granted.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*/
#include "../include/libex/str.h"

int strepl (str_t **str, char *dst_pos, size_t dst_len, const char *src_pos, size_t src_len) {
    str_t *s = *str;
    size_t str_len = s->len;
    ssize_t dv;
    errno = 0;
    if (!src_pos) {
        errno = ERANGE;
        return 0;
    }
    if (dst_pos < s->ptr || dst_pos > s->ptr + s->len) {
        errno = ERANGE;
        return -1;
    }
    if ((dv = (dst_pos + dst_len) - (s->ptr + str_len)) >= 0) {
        s->len = dst_pos - s->ptr;
        STR_ADD_NULL(s);
        return strnadd(str, src_pos, src_len);
    }
    if ((dv = src_len - dst_len) > 0) {
        if ((size_t)s->bufsize <= (size_t)(str_len + dv)) {
            size_t nbufsize = ((1 + str_len + dv) / s->chunk_size) *
                              s->chunk_size +s->chunk_size,
                   dst = (uintptr_t)dst_pos - (uintptr_t)s->ptr;
            str_t *nstr = realloc(s, nbufsize + sizeof(str_t));
            if (!nstr) return -1;
            errno = ERANGE;
            s = *str = nstr;
            s->bufsize = nbufsize;
            dst_pos = nstr->ptr + dst;
        }
        str_len += dv;
    } else
        str_len -= dst_len - src_len;
    memmove(dst_pos + src_len, dst_pos + dst_len,
            s->len - ((uintptr_t)dst_pos - (uintptr_t)s->ptr + dst_len));
    memcpy(dst_pos, src_pos, src_len);
    s->len = str_len;
    STR_ADD_NULL(*str);
    return 0;
}
