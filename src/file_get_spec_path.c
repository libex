/*Copyright (c) Brian B.

Permission to use, copy, modify, and/or distribute this software for any purpose
with or without fee is hereby granted.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*/
#include "../include/libex/file.h"

str_t *get_spec_path (spec_path_t id) {
    str_t *ret = NULL;
    const char *dirname = NULL;
    char buf [MAX_PATH+1];
    switch (id) {
        case DIR_HOME_CONFIG:
            if ((dirname = getenv("HOME")))
                ret = path_combine(dirname, ".config", NULL);
            break;
        case DIR_HOME:
            if ((dirname = getenv("HOME")))
                ret = mkstr(dirname, strlen(dirname), 16);
            break;
        case DIR_CONFIG:
            ret = mkstr(CONST_STR_LEN("/etc"), 16);
            break;
        case DIR_USR_CONFIG:
            ret = mkstr(CONST_STR_LEN("/usr/etc"), 16);
            break;
        case DIR_LOCAL_CONFIG:
            ret = mkstr(CONST_STR_LEN("/usr/local/etc"), 16);
            break;
        case DIR_PROGRAMS:
            ret = mkstr(CONST_STR_LEN("/usr/bin"), 16);
            break;
        case DIR_CURRENT:
            if (!getcwd(buf, sizeof buf))
                return NULL;
            ret = mkstr(buf, strlen(buf), 16);
            break;
        case DIR_TEMPATH:
            ret = mkstr(CONST_STR_LEN("/tmp"), 16);
        break;
    }
    STR_ADD_NULL(ret);
    return ret;
}

