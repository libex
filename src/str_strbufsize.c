/*Copyright (c) Brian B.

Permission to use, copy, modify, and/or distribute this software for any purpose
with or without fee is hereby granted.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*/
#include "../include/libex/str.h"

int strbufsize (strbuf_t *strbuf, size_t nlen, int flags) {
    char *buf = strbuf->ptr;
    size_t bufsize;
    errno = 0;
    if ((flags & STR_KEEPLEN) && nlen < strbuf->len)
        nlen = strbuf->len;
    if ((bufsize = (nlen / strbuf->chunk_size) * strbuf->chunk_size + strbuf->chunk_size) == strbuf->bufsize)
        return 0;
    if (!(flags & STR_REDUCE) && bufsize < strbuf->bufsize) return 0;
    buf = realloc(buf, bufsize);
    if (!buf) return -1;
    errno = ERANGE;
    strbuf->bufsize = bufsize;
    strbuf->ptr = buf;
    if (nlen < strbuf->len) strbuf->len = nlen;
    return 0;
}
