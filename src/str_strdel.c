/*Copyright (c) Brian B.

Permission to use, copy, modify, and/or distribute this software for any purpose
with or without fee is hereby granted.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*/
#include "../include/libex/str.h"

void strdel (str_t **str, char *pos, size_t len, int flags) {
    str_t *s = *str;
    char *e = s->ptr + s->len, *epos = pos + len;
    if (pos > e) return;
    if (epos >= e) {
        s->len = (uintptr_t)pos - (uintptr_t)s->ptr;
        strsize(str, s->len, flags);
        return;
    }
    s->len -= len;
    memmove(pos, epos, (uintptr_t)e - (uintptr_t)epos);
    strsize(str, s->len, flags);
    STR_ADD_NULL(*str);
}
