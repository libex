/*Copyright (c) Brian B.

Permission to use, copy, modify, and/or distribute this software for any purpose
with or without fee is hereby granted.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*/
#include "../include/libex/str.h"

int strsize (str_t **str, size_t nlen, int flags) {
    str_t *s = *str;
    size_t bufsize = (nlen / s->chunk_size) * s->chunk_size + s->chunk_size;
    errno = 0;
    if (bufsize == s->bufsize) return 0;
    if (!(flags & STR_REDUCE) && bufsize < s->bufsize) return 0;
    s = realloc(s, sizeof(str_t) + bufsize);
    if (!s) return -1;
    errno = ERANGE;
    s->bufsize = bufsize;
    *str = s;
    STR_ADD_NULL(*str);
    return 0;
}
