/*Copyright (c) Brian B.

Permission to use, copy, modify, and/or distribute this software for any purpose
with or without fee is hereby granted.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE
OF THIS SOFTWARE.
*/
#include "../include/libex/wsnet.h"

#define WS_REQUEST "GET %s HTTP/1.1\r\nUpgrade: websocket\r\nConnection: Upgrade\r\nHost: %s\r\nSec-WebSocket-Key: %s\r\nSec-WebSocket-Version: 13\r\n\r\n"

char wst_ping [] = {0x89, 0x05, 0xd0, 0x9a, 0xd1, 0x83, 0x3f};
char wst_pong [] = {0x8a, 0x85, 0x45, 0x8e, 0xa0, 0x28, 0x95, 0x14, 0x71, 0xab, 0x7a};
char wst_close [] = {0x88, 0x04, 0xd0, 0x9a, 0xd1, 0x8e};

int ws_netbuf_alloc (netbuf_t *nbuf, size_t start_len, size_t chunk_size) {
    if (-1 == strbufalloc(&nbuf->buf, start_len, chunk_size) ||
        -1 == strbufalloc(&nbuf->tail, start_len, chunk_size))
        return -1;
    return 0;
}

void ws_netbuf_free (netbuf_t *nbuf) {
    if (nbuf->buf.ptr)
        free(nbuf->buf.ptr);
    if (nbuf->tail.ptr)
        free(nbuf->tail.ptr);
    memset(nbuf, 0, sizeof(netbuf_t));
}

int wsnet_handshake (int fd, strbuf_t *buf, strptr_t *url) {
    int rc = WS_ERROR;
    http_request_t req;
    ws_handshake_t wsh;
    memset(&wsh, 0, sizeof(wsh));
    memset(&req, 0, sizeof(req));
    if (net_recv(fd, buf) > 0) {
        switch ((ws_handshake(&req, buf->ptr, buf->len, &wsh))) {
            case HTTP_LOADED:
                if (url) {
                    url->ptr = strndup(req.url.ptr, req.url.len);
                    url->len = req.url.len;
                }
                ws_make_response(buf, &wsh);
                if (net_write(fd, buf->ptr, buf->len) > 0)
                    rc = WS_OK;
                break;
            case HTTP_PARTIAL_LOADED:
                rc = WS_WAIT;
                break;
        }
    }
    if (WS_ERROR == rc && url->ptr) {
        free(url->ptr);
        url->ptr = NULL;
        url->len = 0;
    }
    return rc;
}

int wsnet_try_handshake (int fd, strbuf_t *buf) {
    int rc = WS_ERROR;
    http_request_t req;
    ws_handshake_t wsh;
    if (net_recv(fd, buf) > 0) {
        if (HTTP_LOADED == (rc = ws_handshake(&req, buf->ptr, buf->len, &wsh))) {
            ws_make_response(buf, &wsh);
            if (net_write(fd, buf->ptr, buf->len) > 0)
                rc = WS_OK;
        } else
        if (HTTP_PARTIAL_LOADED == rc)
            rc = WS_WAIT;
    } else
        rc = WS_ERROR;
    return rc;
}

void wsnet_save_tail (netbuf_t *nbuf, ssize_t nbytes) {
    if (nbytes < nbuf->buf.len)
        strbufadd(&nbuf->tail, nbuf->buf.ptr + nbytes, nbuf->buf.len - nbytes);
    else
        nbuf->tail.len = 0;
    nbuf->buf.len = nbytes;
}

int wsnet_recv (int fd, netbuf_t *nbuf, ws_t *ws) {
    int rc = 0;
    ssize_t nbytes;
    if ((nbytes = ws_buflen((const uint8_t*)nbuf->buf.ptr, nbuf->buf.len)) > 0) {
        rc = ws_parse(nbuf->buf.ptr, nbytes, ws);
        wsnet_save_tail(nbuf, nbytes);
        if (WS_OK == rc)
            return WS_OK;
        return WS_WAIT;
    }
    if ((nbytes = net_recv(fd, &nbuf->buf)) > 0) {
        if (-1 == (nbytes = ws_buflen((const uint8_t*)nbuf->buf.ptr, nbuf->buf.len)))
            return WS_WAIT;
        rc = ws_parse(nbuf->buf.ptr, nbytes, ws);
        wsnet_save_tail(nbuf, nbytes);
        if (WS_OK == rc)
            return WS_OK;
        return WS_WAIT;
    }
    return WS_ERROR;
}

int wsnet_parse (netbuf_t *nbuf, ws_t *ws) {
    ssize_t nbytes;
    if ((nbytes = ws_buflen((const uint8_t*)nbuf->buf.ptr, nbuf->buf.len)) > 0) {
        int rc = ws_parse(nbuf->buf.ptr, nbytes, ws);
        wsnet_save_tail(nbuf, nbytes);
        if (WS_OK == rc)
            return WS_OK;
    }
    return NET_WAIT;
}

void wsnet_reset (netbuf_t *nbuf) {
    strbuf_t s = nbuf->buf;
    nbuf->buf = nbuf->tail;
    nbuf->tail = s;
    nbuf->tail.len = 0;
}

// WS

int check_api_ver (int fd, net_srv_t *srv, strptr_t *url) {
    char *e, *tail;
    int ver = -1;
    if (!url->ptr || !*url->ptr) return NET_ERROR;
    e = url->ptr + url->len - 1;
    while (e > url->ptr && isdigit(*e)) --e;
    ++e;
    ver = strtol(e, &tail, 0);
    if (ERANGE == errno || '\0' != *tail) return NET_ERROR;
    if (ver < 0) ver = 0; else
    if (ver > 0) ver = ver - 1;
    return ver;
}

int ev_getver (net_srv_t *srv, ev_fd_t fd) {
    int ver = -1;
    net_ev_t *ev;
    if ((ev = srv->get_ev(srv, NET_FD(fd)))) {
        ver = ev->ver;
        ev_unlock(srv, NET_FD(fd));
    }
    return ver;
}

static int ws_ev_recv (int fd, int flags, net_srv_t *srv) {
    net_ev_t *ev = srv->get_ev(srv, fd);
    int rc;
    if (!(ev->nf_flags & NF_WSCONNECTED)) {
        strptr_t url = CONST_STR_INIT_NULL;
        switch (wsnet_handshake(fd, &ev->rd_buf.buf, &url)) {
            case WS_WAIT:
                ev_unlock(srv, fd);
                return NET_WAIT;
            case WS_ERROR:
                ev_unlock(srv, fd);
                return NET_ERROR;
        }
        ev->nf_flags |= NF_WSCONNECTED;
        if (ev->rd_buf.buf.ptr) {
            free(ev->rd_buf.buf.ptr);
            ev->rd_buf.buf.ptr = NULL;
            ev->rd_buf.buf.len = 0;
        }
        if (url.ptr) {
            if (srv->on_check_url) {
                int ver = srv->on_check_url(fd, srv, &url);
                if (-1 == ver) {
                    ev_unlock(srv, fd);
                    free(url.ptr);
                    return NET_ERROR;
                }
                ev->ver = ver;
            }
            free(url.ptr);
        }
        ev_unlock(srv, fd);
        return NET_OK;
    }
    if (NF_READ == flags)
        rc = wsnet_recv(fd, &ev->rd_buf, &ev->ws);
    else
        rc = wsnet_parse(&ev->rd_buf, &ev->ws);
    switch (rc) {
        case WS_WAIT:
            ev_unlock(srv, fd);
            return NET_WAIT;
        case WS_ERROR:
            ev_unlock(srv, fd);
            return NET_ERROR;
    }
    ev_unlock(srv, fd);
    return NET_OK;
}

int ws_evbuf_set (ev_buf_t *n_ev, net_ev_t *ev) {
    n_ev->data.ws = ev->ws;
    return NET_OK;
}

int ws_ev_send (net_srv_t *srv, ev_fd_t fd, uint32_t flags, const char *buf, size_t len, uint32_t ws_flags, uint8_t ws_opcode) {
    int rc, nf_flags;
    net_ev_t *ev;
    strbuf_t wbuf;
    if (-1 == strbufalloc(&wbuf, len, SRV_BUF_SIZE))
        return NET_ERROR;
    ev = srv->get_ev(srv, NET_FD(fd));
    nf_flags = ev->nf_flags;
    ev_unlock(srv, NET_FD(fd));
    strbufput(&wbuf, buf, len, 0);
    if ((nf_flags & NF_CLIENT))
        ws_flags |= WS_MASK;
    ws_create(&wbuf, ws_flags, ws_opcode);
    rc = net_ev_send((net_srv_t*)srv, fd, flags, wbuf.ptr, wbuf.len);
    free(wbuf.ptr);
    return rc;
}

str_t *create_ws_request (http_url_t *h_url) {
    str_t *res;
    char nonce [16];
    cstr_t *key;
    for (int i = 0; i < sizeof(nonce); ++i)
        nonce[i] = rand() & 0xff;
    key = cstr_b64encode(nonce, sizeof(nonce));
    res = stralloc(sizeof(WS_REQUEST) + h_url->path.len + h_url->domain.len + key->len, 64);
    res->len = snprintf(res->ptr, res->bufsize, WS_REQUEST, h_url->path.ptr, h_url->domain.ptr, key->ptr);
    free(key);
    return res;
}

int ws_ev_connect (int fd, net_srv_t *srv, uint32_t *flags, http_url_t *h_url) {
    int rc = NET_ERROR;
    ssize_t rd;
    strbuf_t buf;
    str_t *req;
    if (!(*flags & NF_CLIENT))
        return NET_OK;
    req = create_ws_request(h_url);
    if (net_write(fd, req->ptr, req->len) != req->len) {
        free(req);
        return NET_ERROR;
    }
    free(req);
    strbufalloc(&buf, SRV_BUF_SIZE, SRV_BUF_SIZE);
    while ((rd = net_read(fd, &buf)) > 0) {
        if (buf.len > 4 && 0 == cmpstr(buf.ptr+buf.len-4, 4, CONST_STR_LEN("\r\n\r\n"))) { // FIXME : parse response, save tail
            *flags |= NF_WSCONNECTED;
            rc = NET_WAIT;
            break;
        }
    }
    free(buf.ptr);
    return rc;
}

static int ws_ev_pong (int fd, net_srv_t *srv) {
    net_ev_t *ev = srv->get_ev(srv, fd);
    if (!ev->wr_buf.ptr && -1 == strbufalloc(&ev->wr_buf, WST_PONG_LEN, SRV_BUF_SIZE)) {
        ev_unlock(srv, fd);
        return NET_ERROR;
    }
    strbufadd(&ev->wr_buf, wst_pong, WST_PONG_LEN);
    net_poll_ctl_out(srv->efd, fd);
    ev_unlock(srv, fd);
    return NET_OK;
}

int ws_ev_disconnect (net_srv_t *srv, ev_fd_t fd) {
    net_ev_t *ev = NULL;
    errno = 0;
    if (!(ev = net_ev_lock(srv, fd))) {
        errno = EBADF;
        return NET_ERROR;
    }
    if (!(ev->nf_flags & NF_CLIENT)) {
        errno = EPERM;
        ev_unlock(srv, NET_FD(fd));
        return NET_ERROR;
    }
    if (!ev->wr_buf.ptr && -1 == strbufalloc(&ev->wr_buf, WST_CLOSE_LEN, SRV_BUF_SIZE)) {
        errno = EOVERFLOW;
        ev_unlock(srv, NET_FD(fd));
        return NET_ERROR;
    }
    ev->nf_flags |= NF_CLOSE | NF_SENDBUF;
    strbufadd(&ev->wr_buf, wst_close, WST_CLOSE_LEN);
    net_poll_ctl_out(srv->efd, NET_FD(ev->fd));
    ev_unlock(srv, NET_FD(fd));
    return NET_OK;
}

int ws_ev_recvd (net_srv_t *srv, ev_buf_t *ev) {
    int rc = NET_WAIT;
    switch (WS_OPCODE(&ev->data.ws)) {
        case WS_PING:
            ws_ev_pong(NET_FD(ev->fd), srv);
            errno = 0;
            rc = NET_OK;
            break;
        case WS_CLOSE:
            net_ev_close(srv, NET_FD(ev->fd), NF_FD);
            errno = EAGAIN;
            rc = NET_OK;
            break;
        case WS_PONG:
            errno = 0;
            rc = NET_OK;
            break;
        default:
            break;
    }
    return rc;
}

int ws_close_ev (int fd, net_srv_t *srv) {
    return ws_ev_close_send(srv, fd, NF_FD);
}

net_srv_t *ws_srv_init (const char *svc) {
    net_srv_t *srv = net_srv_init(svc);
    if (!srv) return NULL;
    srv->on_connect = ws_ev_connect;
    srv->on_recv = ws_ev_recv;
    srv->on_send = ev_send;
    srv->on_recvd = ws_ev_recvd;
    srv->ev_bufsize = sizeof(ev_buf_t);
    srv->on_evbuf_set = ws_evbuf_set;
    srv->on_close_ev = ws_close_ev;
    return srv;
}

static void ping_all (net_srv_t *srv, ev_array_t *ids, time_t t0) {
    for (int i = 0; i < ids->len; ++i) {
        int nf_flags;
        fd_t *f_id = (fd_t*)&ids->ptr[i];
        net_ev_t *ev = srv->get_ev(srv, f_id->key.fd);
        time_t tm = ev->tm;
        nf_flags = ev->nf_flags;
        ev_unlock(srv, f_id->key.fd);
        if ((nf_flags & NF_CLIENT) || t0 - tm <= srv->tm_ping)
            continue;
        net_ev_send((net_srv_t*)srv, ids->ptr[i], 0, wst_ping, WST_PING_LEN);
    }
}

static void check_tm (net_srv_t *srv, ev_array_t *ids, time_t t0) {
    for (int i = 0; i < ids->len; ++i) {
        fd_t *f_id = (fd_t*)&ids->ptr[i];
        int nf_flags;
        net_ev_t *ev = srv->get_ev(srv, f_id->key.fd);
        time_t tm = ev->tm;
        nf_flags = ev->nf_flags;
        ev_unlock(srv, f_id->key.fd);
        if (!(nf_flags & NF_CLIENT) && t0 - tm > srv->tm_ping * 3) {
            net_ev_close(srv, f_id->fd, 0);
        }
    }
}

void *ws_ping_cb (void *param) {
    net_srv_t *srv = param;
    while (srv->is_active) {
        struct timespec to_time;
        time_t t0;
        ev_array_t *ids;
        clock_gettime(CLOCK_MONOTONIC, &to_time);
        to_time.tv_sec += srv->tm_ping;
        pthread_mutex_lock(&srv->ping_locker);
        if (ETIMEDOUT != pthread_cond_timedwait(&srv->ping_cond, &srv->ping_locker, &to_time)) {
            pthread_mutex_unlock(&srv->ping_locker);
            break;
        }
        pthread_mutex_unlock(&srv->ping_locker);
        ids = get_all_id_info((net_srv_t*)srv);
        t0 = time(0);
        ping_all(srv, ids, t0);
        check_tm(srv, ids, t0);
        free(ids);
        if (srv->on_ping)
            srv->on_ping(srv);
    }
    return NULL;
}

void ws_srv_start (net_srv_t *srv) {
    if (srv->tm_ping > 0) {
        pthread_mutex_init(&srv->ping_locker, NULL);
        pthread_condattr_init(&srv->ping_condattr);
        pthread_condattr_setclock(&srv->ping_condattr, CLOCK_MONOTONIC);
        pthread_cond_init(&srv->ping_cond, &srv->ping_condattr);
        srv->is_active = 1;
        pthread_create(&srv->th_ping, NULL, ws_ping_cb, srv);
    }
    net_srv_start(srv);
}

void ws_srv_done (net_srv_t *srv) {
    if (srv->is_active && srv->tm_ping > 0) {
        srv->is_active = 0;
        if (srv->th_ping) {
            pthread_mutex_lock(&srv->ping_locker);
            pthread_cond_signal(&srv->ping_cond);
            pthread_mutex_unlock(&srv->ping_locker);
            pthread_join(srv->th_ping, NULL);
        }
        pthread_cond_destroy(&srv->ping_cond);
        pthread_condattr_destroy(&srv->ping_condattr);
        pthread_mutex_destroy(&srv->ping_locker);
    }
    net_srv_done(srv);
}

// WS RPC

static int cmp_msg_info (const ev_locker_t **x, ev_locker_t **y) {
    if ((*x)->id_rpc > (*y)->id_rpc) return 1;
    if ((*x)->id_rpc < (*y)->id_rpc) return -1;
    return 0;
}

static void ev_locker_init (net_srv_t *srv, ev_locker_t *ev_l, int64_t *id_rpc,
                     pthread_mutex_t *locker, pthread_cond_t *cond,
                     pthread_condattr_t *condattr) {
    memset(ev_l, 0, sizeof(ev_locker_t));
    ev_l->id_rpc = *id_rpc;
    ev_l->locker = locker;
    ev_l->cond = cond;
    ev_l->condattr = condattr;
    pthread_mutex_init(ev_l->locker, NULL);
    if (srv->tm_recv > 0) {
        pthread_condattr_init(ev_l->condattr);
        pthread_condattr_setclock(ev_l->condattr, CLOCK_MONOTONIC);
        pthread_cond_init(ev_l->cond, ev_l->condattr);
    } else
        pthread_cond_init(ev_l->cond, NULL);
}

static void ev_locker_destroy (net_srv_t *srv, ev_locker_t *ev_l) {
    if (srv->tm_recv > 0)
        pthread_condattr_destroy(ev_l->condattr);
    pthread_cond_destroy(ev_l->cond);
    pthread_mutex_destroy(ev_l->locker);
    pthread_mutex_lock(&srv->locker);
    //del_msginfo(srv->msg_info, ev_l);
    pthread_mutex_unlock(&srv->locker);
}

//int ev_locker_set (net_srv_t *srv, ev_locker_t *ev_l) {
static ev_locker_t *ev_locker_set (net_srv_t *srv, ev_locker_t *ev_l) {
    //int rc = NET_ERROR;
    pthread_mutex_lock(&srv->locker);
    //ev_l->ti = add_msginfo(srv->msg_info, ev_l);
    ev_locker_t **ev_res = add_msginfo(srv->msg_info, &ev_l);
    if (EEXIST != errno)
        //rc = NET_OK;
        ev_res = NULL;
    pthread_mutex_unlock(&srv->locker);
    //return rc;
    return *ev_res;
}

static int ev_locker_signal (net_srv_t *srv, ev_buf_t *ev) {
    int rc = NET_ERROR;
    ev_locker_t ti = { .id_rpc = ev->data.rpc.id_rpc },
                *ti_ptr = &ti, **ev_l;
    pthread_mutex_lock(&srv->locker);
    ev_l = find_msginfo(srv->msg_info, &ti_ptr);
    pthread_mutex_unlock(&srv->locker);
    if (ev) {
        pthread_mutex_lock((*ev_l)->locker);
        (*ev_l)->buf = ev->buf;
        ev->buf.ptr = NULL;
        ev->buf.len = 0;
        (*ev_l)->ws = ev->data.ws;
        pthread_cond_signal((*ev_l)->cond);
        pthread_mutex_unlock((*ev_l)->locker);
        rc = NET_OK;
    }
    return rc;
}

int ws_rpc_srv_stop (net_srv_t *srv) {
    free_msginfo(srv->msg_info);
    return NET_OK;
}

msginfo_t *ws_create_msg_info () {
    msginfo_t *info = create_msginfo();
    info->on_compare = cmp_msg_info;
    return info;
}

// RPC

void ws_rpc_evbuf_free (ev_buf_t *n_ev) {
    rpc_clear(&n_ev->data.rpc.rpc);
    if (n_ev->buf.ptr)
        free(n_ev->buf.ptr);
    free(n_ev);
}

static int ws_jsonrpc_handler (net_srv_t *srv, ev_buf_t *ev) {
    int rc = NET_OK;
    ws_t *ws = &ev->data.ws;
    if (!WS_ISFIN(ws) || WS_TEXT != WS_OPCODE(ws))
        return NET_OK;
    if (jsonrpc_is_response(&ev->data.json.jsonrpc)) {
         if (NET_ERROR == srv->on_handle(ev))
             return NET_ERROR;
    } else if ((rc = jsonrpc_execute_parsed((jsonrpc_method_list_t*)srv->userdata, &ev->buf, WS_EV_OFFSET(ev), ev, &ev->data.json.jsonrpc)) < 0)
        rc = ws_ev_send(srv, ev->fd, 0, ev->buf.ptr, ev->buf.len, WS_FIN, WS_TEXT); // TODO : why send?
    return rc;
}

int ws_rpc_srv_event (void *dummy, pool_msg_t *msg) {
    int rc;
    ev_buf_t *n_ev = msg->data;
    net_srv_t *srv = n_ev->srv;
    rpc_t *rpc = &n_ev->data.rpc.rpc;
    if ((rc = rpc_parse(&n_ev->buf, (uintptr_t)n_ev->data.ws.ptr - (uintptr_t)n_ev->buf.ptr, rpc)) < 0) {
        intptr_t id_rpc;
        int id_rpc_len;
        if (RPC_PARSE_ERROR == rc) {
            rpc_clear(rpc);
            goto err;
        }
        id_rpc_len = rpc_getid(rpc, 1, &id_rpc);
        rpc_clear(rpc);
        if (0 == id_rpc_len)
            goto err;
        if (RPC_OK == rpc_create_error(&n_ev->buf, rc, id_rpc, id_rpc_len))
            ws_ev_send(srv, n_ev->fd, 0, n_ev->buf.ptr, n_ev->buf.len, WS_FIN, WS_BIN);
        if (id_rpc && id_rpc_len > 0)
            free((void*)id_rpc);
        goto err;
    }
    if ((rpc->flags & RPC_RESPONSE)) {
        rc = NET_ERROR;
        n_ev->data.rpc.id_rpc = rpc->id.id_int;
        if (!(rpc->flags & RPC_IDINT) || NET_ERROR == (rc = ev_locker_signal(srv, n_ev))) {
            if (NET_ERROR == (ws_jsonrpc_handler(srv, n_ev))) goto err;
        } else if (NET_ERROR == rc && NET_ERROR == ws_jsonrpc_handler(srv, n_ev))
            goto err;
    } else
        return net_srv_event(dummy, msg);
    srv->on_evbuf_free(n_ev);
    return MSG_DONE;
err:
    net_ev_close(srv, n_ev->fd, 0);
    srv->on_evbuf_free(n_ev);
    return MSG_DONE;
}

net_srv_t *ws_rpc_srv_init (const char *svc) {
    net_srv_t *srv = net_srv_init(svc);
    if (!srv) return NULL;
    srv->on_connect = ws_ev_connect;
    srv->on_recv = ws_ev_recv;
    srv->on_send = ev_send;
    srv->on_recvd = ws_ev_recvd;
    srv->ev_bufsize = sizeof(ev_buf_t);
    srv->on_evbuf_set = ws_evbuf_set;
    srv->on_stop = ws_rpc_srv_stop;
    srv->msg_info = ws_create_msg_info();
    srv->on_event = ws_rpc_srv_event;
    srv->on_evbuf_free = ws_rpc_evbuf_free;
    return srv;
}

int ws_rpc_ev_send (net_srv_t *srv,
                    ev_fd_t id,
                    uint32_t flags,
                    const char *buf, size_t len,
                    uint32_t ws_flags, uint8_t ws_opcode,
                    int64_t *id_rpc,
                    ws_t *ws,
                    strbuf_t *res) {
    int rc = NET_ERROR;
    if ((NF_SYNC & flags)) {
        pthread_mutex_t locker;
        pthread_cond_t cond;
        pthread_condattr_t condattr;
        ev_locker_t ev_l, *ev_l_ptr;
        ev_locker_init(srv, &ev_l, id_rpc, &locker, &cond, &condattr);
        //if (NET_OK == (rc = ev_locker_set(srv, &ev_l))) {
        if ((ev_l_ptr = ev_locker_set(srv, &ev_l))) {
            pthread_mutex_lock(&locker);
            if (NET_OK == (rc = ws_ev_send(srv, id, flags, buf, len, ws_flags, ws_opcode))) {
                if (srv->tm_recv > 0) {
                    struct timespec to_time;
                    clock_gettime(CLOCK_MONOTONIC, &to_time);
                    to_time.tv_sec += srv->tm_recv;
                    if (ETIMEDOUT == pthread_cond_timedwait(&cond, &locker, &to_time))
                        rc = NET_ERROR;
                } else
                    pthread_cond_wait(&cond, &locker);
                if (NET_OK == rc) {
                    if (ev_l.buf.ptr) {
                        *res = ev_l.buf;
                        *ws = ev_l.ws;
                    } else
                        rc = NET_ERROR;
                }
            }
            pthread_mutex_unlock(&locker);
            del_msginfo(&ev_l_ptr);
        }
        ev_locker_destroy(srv, &ev_l);
    } else
        rc = ws_ev_send(srv, id, flags, buf, len, ws_flags, ws_opcode);
    return rc;
}

int ws_rpc_request_send (net_srv_t *srv,
                         ev_fd_t id,
                         uint32_t method,
                         uint32_t flags,
                         strbuf_t *buf,
                         rpc_h cb,
                         rpc_t *rpc,
                         void *userdata) {
    int rc;
    ws_t ws;
    int64_t id_rpc;
    if (buf->ptr) {
        free(buf->ptr);
        buf->ptr = NULL;
        buf->len = 0;
    }
    if (rpc)
        rpc_clear(rpc);
    if ((NF_SYNC & flags)) {
        id_rpc = __atomic_inc_ulong(&srv->id_rpc) & ~0x8000000000000000;
        rc = rpc_create_request(buf, method, cb, RPC_ID_INT(id_rpc), userdata);
    } else
    if ((NF_NOTIFY & flags))
        rc = rpc_create_request(buf, method, cb, RPC_ID_NULL, userdata);
    else {
        char str [16];
        int len = snprintf(str, sizeof str, "%u", method);
        rc = rpc_create_request(buf, method, cb, (intptr_t)str, len, userdata);
    }
    if (rc < 0)
        return NET_ERROR;
    if (NET_OK == (rc = ws_rpc_ev_send(srv, id, flags, buf->ptr, buf->len, WS_FIN, WS_BIN, &id_rpc, &ws, buf))) {
        if ((NF_SYNC & flags)) {
            size_t off = (uintptr_t)ws.ptr - (uintptr_t)buf->ptr;
            memmove(buf->ptr, ws.ptr, buf->len);
            buf->len -= off;
            memset(rpc, 0, sizeof(rpc_t));
            if (rpc && RPC_OK != rpc_parse(buf, 0, rpc)) {
                rpc_clear(rpc);
                rc = NET_ERROR;
            }
        } else
            buf->len = 0;
    }
    return rc;
}

int ws_rpc_response_send (net_srv_t *srv,
                          ev_fd_t id,
                          strbuf_t *buf,
                          intptr_t id_rpc,
                          int id_rpc_len,
                          rpc_h cb,
                          void *userdata) {
    int rc = NET_ERROR;
    if (RPC_OK == rpc_create_response(buf, 0, cb, id_rpc, id_rpc_len, userdata))
        rc = ws_ev_send(srv, id, 0, buf->ptr, buf->len, WS_FIN, WS_BIN);
    return rc;
}

int ws_rpc_error_send (net_srv_t *srv,
                       ev_fd_t id,
                       strbuf_t *buf,
                       int errcode,
                       intptr_t id_rpc, int id_rpc_len) {
    int rc = NET_ERROR;
    if (RPC_OK == rpc_create_error(buf, errcode, id_rpc, id_rpc_len))
        rc = ws_ev_send(srv, id, 0, buf->ptr, buf->len, WS_FIN, WS_BIN);
    return rc;
}
// WS JSONRPC

void ws_jsonrpc_evbuf_free (ev_buf_t *n_ev) {
    if (n_ev->data.json.jsonrpc.json)
        json_free(n_ev->data.json.jsonrpc.json);
    if (n_ev->buf.ptr)
        free(n_ev->buf.ptr);
    free(n_ev);
}

int ws_jsonrpc_srv_event (void *dummy, pool_msg_t *msg) {
    ev_buf_t *ev = msg->data;
    net_srv_t *srv = ev->srv;
    int rc;
    if (WS_BIN == WS_OPCODE(&ev->data.ws)) {
        if (NET_ERROR == ws_jsonrpc_handler(srv, ev))
            goto err;
        goto done;
    } else
    if (JSONRPC_PARSE_ERROR == (rc = jsonrpc_parse((const char*)ev->data.ws.ptr, ev->buf.len, &ev->data.json.jsonrpc)))
        goto err;
    if (jsonrpc_is_response(&ev->data.json.jsonrpc)) {
        rc = NET_ERROR;
        ev->data.rpc.id_rpc = ev->data.json.jsonrpc.id->data.i;
        if (JSON_INTEGER != ev->data.json.jsonrpc.id->type || NET_ERROR == (rc = ev_locker_signal(srv, ev))) {
            if (srv->on_handle && NET_ERROR == srv->on_handle(ev))
                goto err;
        } else if (NET_ERROR == rc && NET_ERROR == ws_jsonrpc_handler(srv, ev))
            goto err;
    } else
    if (JSONRPC_OK != rc) {
        jsonrpc_stderror(&ev->buf, rc, JSONRPC_ID_INT(ev->data.rpc.id_rpc));
        if (NET_ERROR == ws_ev_send(srv, ev->fd, 0, ev->buf.ptr, ev->buf.len, WS_FIN, WS_TEXT))
            goto err;
    } else
        return net_srv_event(dummy, msg);
    goto done;
err:
    net_ev_close(srv, ev->fd, 0);
done:
    srv->on_evbuf_free(ev);
    return MSG_DONE;
}

net_srv_t *ws_jsonrpc_srv_init (const char *svc) {
    net_srv_t *srv = ws_rpc_srv_init(svc);
    if (!srv) return NULL;
    srv->ev_bufsize = sizeof(ev_buf_t);
    srv->on_evbuf_free = ws_jsonrpc_evbuf_free;
    srv->on_event = ws_jsonrpc_srv_event;
    srv->userdata = jsonrpc_init();
    return srv;
}

json_t *ws_jsonrpc_request_send (net_srv_t *srv,
                                 ev_fd_t id,
                                 const char *method,
                                 size_t method_len,
                                 uint32_t flags,
                                 strbuf_t *buf,
                                 jsonrpc_h cb,
                                 void *userdata) {
    int64_t id_rpc;
    json_t *json = NULL;
    ws_t ws;
    int rc;
    if (buf->ptr)
        buf->len = 0;
    else
    if (-1 == strbufalloc(buf, SRV_BUF_SIZE, SRV_BUF_SIZE))
        return NULL;
    if ((NF_SYNC & flags)) {
        id_rpc = __atomic_inc_ulong(&srv->id_rpc) & ~0x8000000000000000;
        rc = jsonrpc_request(buf, method, method_len, JSONRPC_ID_INT(id_rpc), cb, userdata);
    } else
    if ((NF_NOTIFY & flags))
        rc = jsonrpc_request(buf, method, method_len, JSONRPC_ID_NULL, cb, userdata);
    else
        rc = jsonrpc_request(buf, method, method_len, (intptr_t)method, method_len, cb, userdata);
    if (0 != rc) {
        buf->len = 0;
        return NULL;
    }
    if (NET_OK == ws_rpc_ev_send(srv, id, flags, buf->ptr, buf->len, WS_FIN, WS_TEXT, &id_rpc, &ws, buf)) {
        if ((NF_SYNC & flags)) {
            size_t off = (uintptr_t)ws.ptr - (uintptr_t)buf->ptr;
            memmove(buf->ptr, ws.ptr, buf->len);
            buf->len -= off;
            json = json_parse_len(buf->ptr, buf->len);
        }
        else
            json = (void*)(intptr_t)-1;
    }
    return json;
}

int ws_jsonrpc_response_send (net_srv_t *srv,
                              ev_fd_t id,
                              strbuf_t *buf,
                              intptr_t id_rpc, int id_rpc_len,
                              jsonrpc_h cb,
                              void *userdata) {
    if (buf->ptr)
        buf->len = 0;
    else
    if (-1 == strbufalloc(buf, SRV_BUF_SIZE, SRV_BUF_SIZE))
        return NET_ERROR;
    jsonrpc_response(buf, cb, userdata, id_rpc, id_rpc_len);
    return ws_ev_send(srv, id, 0, buf->ptr, buf->len, WS_FIN, WS_TEXT);
}

int ws_jsonrpc_error_send (net_srv_t *srv,
                           ev_fd_t id,
                           strbuf_t *buf,
                           int code,
                           const char *msg, size_t lmsg,
                           intptr_t id_rpc, int id_rpc_len) {

    if (buf->ptr)
        buf->len = 0;
    else
    if (-1 == strbufalloc(buf, SRV_BUF_SIZE, SRV_BUF_SIZE))
        return NET_ERROR;
    jsonrpc_error(buf, code, msg, lmsg, id_rpc, id_rpc_len);
    return ws_ev_send(srv, id, 0, buf->ptr, buf->len, WS_FIN, WS_TEXT);
}

int ws_jsonrpc_stderror_send (net_srv_t *srv,
                              ev_fd_t id,
                              strbuf_t *buf,
                              int code,
                              intptr_t id_rpc, int id_rpc_len) {
    switch (code) {
        case JSONRPC_PARSE_ERROR:
            return ws_jsonrpc_error_send(srv, id, buf, code, CONST_STR_LEN(JSONRPC_PARSE_ERROR_STR), id_rpc, id_rpc_len);
        case JSONRPC_INVALID_REQUEST:
            return ws_jsonrpc_error_send(srv, id, buf, code, CONST_STR_LEN(JSONRPC_INVALID_REQUEST_STR), id_rpc, id_rpc_len);
        case JSONRPC_METHOD_NOT_FOUND:
            return ws_jsonrpc_error_send(srv, id, buf, code, CONST_STR_LEN(JSONRPC_METHOD_NOT_FOUND_STR), id_rpc, id_rpc_len);
        case JSONRPC_INVALID_PARAMS:
            return ws_jsonrpc_error_send(srv, id, buf, code, CONST_STR_LEN(JSONRPC_INVALID_PARAMS_STR), id_rpc, id_rpc_len);
        case JSONRPC_INTERNAL_ERROR:
            return ws_jsonrpc_error_send(srv, id, buf, code, CONST_STR_LEN(JSONRPC_INTERNAL_ERROR_STR), id_rpc, id_rpc_len);
    }
    if (code >= JSONRPC_ERROR_MIN && code <= JSONRPC_ERROR_MAX)
        return ws_jsonrpc_error_send(srv, id, buf, code, CONST_STR_LEN("Server error"), id_rpc, id_rpc_len);
    return NET_ERROR;
}

#ifdef USE_SSL
static int sslws_ev_connect (int fd, net_srv_t *srv, uint32_t *flags, http_url_t *h_url) {
    int rc = NET_ERROR;
    ssize_t rd;
    net_ev_t *ev = srv->get_ev(srv, fd);
    str_t *req;
    strbuf_t buf;
    if (!(*flags & NF_CLIENT))
        return NET_OK;
    switch (ssl_connect(srv->ctx_cli, fd, &ev->ssl)) {
        case -1:
            return NET_ERROR;
        case 0:
            return NET_ERROR;
        case 1:
            ev->nf_flags |= NF_SSLACCEPTED;
            break;
    }
    req = create_ws_request(h_url);
    strbufalloc(&buf, SRV_BUF_SIZE, SRV_BUF_SIZE);
    if (req->len == ssl_send(ev->ssl, req->ptr, req->len)) {
        while ((rd = ssl_recv(ev->ssl, &buf)) > 0) {
            if (buf.len > 4 && 0 == cmpstr(buf.ptr+buf.len-4, 4, CONST_STR_LEN("\r\n\r\n"))) { // FIXME : parse response, save tail
                net_ev_t *ev = srv->get_ev(srv, fd);
                rc = NET_WAIT;
                ev->nf_flags |= NF_WSCONNECTED;
                break;
            }
        }
    }
    free(buf.ptr);
    free(req);
    return rc;
}

static int sslws_handshake (SSL *ssl, strbuf_t *buf, strptr_t *url) {
    int rc = WS_ERROR;
    ssize_t readed;
    while ((readed = ssl_recv(ssl, buf)) > 0);
    if (0 == readed && SSL_ERROR_WANT_READ == ssl_errno)
        return WS_WAIT;
    if (-1 == readed)
        return WS_ERROR;
    http_request_t req;
    ws_handshake_t wsh;
    memset(&req, 0, sizeof req);
    memset(&wsh, 0, sizeof wsh);
    switch ((rc = ws_handshake(&req, buf->ptr, buf->len, &wsh))) {
        case HTTP_LOADED:
            if (url) {
                url->ptr = strndup(req.url.ptr, req.url.len);
                url->len = req.url.len;
            }
            ws_make_response(buf, &wsh);
            if ((readed = ssl_send(ssl, buf->ptr, buf->len)) > 0) {
                rc = WS_OK;
                buf->len = 0;
                return rc;
            }
            rc = WS_ERROR;
            buf->len = 0;
            if (url) {
                if (url->ptr) {
                    free(url->ptr);
                    url->ptr = NULL;
                }
                url->len = 0;
            }
            break;
        case HTTP_ERROR:
            rc = WS_ERROR;
            break;
        default:
            rc = WS_WAIT;
            break;
    }
    return rc;
}

static int sslws_recv (SSL *ssl, netbuf_t *nbuf, ws_t *result) {
    int rc = 0;
    ssize_t nbytes, ntotal = 0;
    if ((nbytes = ws_buflen((const uint8_t*)nbuf->buf.ptr, nbuf->buf.len)) > 0) {
        rc = ws_parse(nbuf->buf.ptr, nbytes, result);
        wsnet_save_tail(nbuf, nbytes);
        if (WS_OK == rc)
            return WS_OK;
        return WS_WAIT;
    }
    while ((nbytes = ssl_recv(ssl, &nbuf->buf)) > 0)
        ntotal += nbytes;
    if (0 == nbytes && SSL_ERROR_WANT_READ == ssl_errno)
        return WS_WAIT;
    if (-1 == nbytes)
        return WS_ERROR;
    if (-1 == (nbytes = ws_buflen((const uint8_t*)nbuf->buf.ptr, nbuf->buf.len)))
        return WS_WAIT;
    rc = ws_parse(nbuf->buf.ptr, nbytes, result);
    wsnet_save_tail(nbuf, nbytes);
    if (WS_OK == rc)
        return WS_OK;
    return WS_WAIT;
}

static int sslws_ev_recv (int fd, int flags, net_srv_t *srv) {
    net_ev_t *ev = srv->get_ev(srv, fd);
    int rc;
    if (!(ev->nf_flags & NF_SSLACCEPTED)) {
        switch (ssl_accept(srv->ctx_srv, fd, &ev->ssl)) {
            case -1:
                return NET_ERROR;
            case 0:
                return NET_WAIT;
            case 1:
                ev->nf_flags |= NF_SSLACCEPTED;
                break;
        }
    }
    if (!(ev->nf_flags & NF_WSCONNECTED)) {
        strptr_t url = CONST_STR_INIT_NULL;
        switch (sslws_handshake(ev->ssl, &ev->rd_buf.buf, &url)) {
            case WS_WAIT:
                return NET_WAIT;
            case WS_ERROR:
                return NET_ERROR;
        }
//        ticket_lock(&ev->locker);
        ev->nf_flags |= NF_WSCONNECTED;
        if (ev->rd_buf.buf.ptr) {
            free(ev->rd_buf.buf.ptr);
            ev->rd_buf.buf.ptr = NULL;
        }
        if (url.ptr) {
            if (srv->on_check_url) {
                int ver;
                if (NET_ERROR == (ver = srv->on_check_url(fd, srv, &url))) {
                    //ticket_unlock(&ev->locker);
                    ev_unlock(srv, fd);
                    free(url.ptr);
                    return NET_ERROR;
                }
                ev->ver = ver;
            }
            free(url.ptr);
        }
        //ticket_unlock(&ev->locker);
        ev_unlock(srv, fd);
        return NET_OK;
    }
    if (NF_READ == flags)
        rc = sslws_recv(ev->ssl, &ev->rd_buf, &ev->ws);
    else
        rc = wsnet_parse(&ev->rd_buf, &ev->ws);
    switch (rc) {
        case WS_WAIT:
            return NET_WAIT;
        case WS_ERROR:
            return NET_ERROR;
    }
    return NET_OK;
}

static int ssl_ev_send (int fd, net_srv_t *srv) {
    ssize_t rc;
    cstr_t *buf;
    net_ev_t *ev = srv->get_ev(srv, fd);
    if (!ev->ssl)
        return NET_OK;
    //ticket_lock(&ev->locker);
    buf = mkcstr(ev->wr_buf.ptr + ev->wrote, ev->wr_buf.len - ev->wrote);
    //ticket_unlock(&ev->locker);
    if ((rc = ssl_send(ev->ssl, buf->ptr, buf->len)) > 0)
        ev->wrote += rc;
    ev_unlock(srv, fd);
    free(buf);
    return rc;
}

static int ssl_close_ev (int fd, net_srv_t *srv) {
    net_ev_t *ev = srv->get_ev(srv, fd);
    if (ev->ssl) {
        SSL_shutdown(ev->ssl);
        SSL_free(ev->ssl);
        ev->ssl = NULL;
    }
    return NET_OK;
}

int ssl_free_ev (int fd, net_srv_t *srv) {
    net_ev_t *ev = srv->get_ev(srv, fd);
    if (ev->ssl) {
        SSL_shutdown(ev->ssl);
        SSL_free(ev->ssl);
    }
    return ev_free_ev(fd, srv);
}

static int on_ws_handle (ev_buf_t *ev) {
    int rc = NET_ERROR;
    jsonrpc_method_list_t *methods = ev->srv->userdata;
    ws_t *ws = &ev->data.ws;
    if (!WS_ISFIN(ws) ||
        WS_TEXT != WS_OPCODE(ws) ||
        jsonrpc_is_response(&ev->data.json.jsonrpc)) {
        ev->buf.len = 0;
        return NET_OK;
    }
    if ((rc = jsonrpc_execute_parsed(methods, &ev->buf, WS_EV_OFFSET(ev), NULL, &ev->data.json.jsonrpc)) < 0)
        return ws_ev_send(ev->srv, ev->fd, 0, ev->buf.ptr, ev->buf.len, WS_FIN, WS_TEXT);
    return NET_OK;
}

net_srv_t *sslws_srv_init (const char *svc, const char *cert_file, const char *key_file) {
    net_srv_t *srv = net_srv_init(svc);
    SSL_CTX *ctx_cli = NULL, *ctx_srv = NULL;
    if (!srv) return NULL;
    if (!(ctx_cli = ssl_create(SSL_CLIENT)) ||
        !(ctx_srv = ssl_create(SSL_SERVER)) ||
        -1 == ssl_config(ctx_cli, cert_file, key_file) ||
        -1 == ssl_config(ctx_srv, cert_file, key_file))
            goto err;
    srv->ctx_cli = ctx_cli;
    srv->ctx_srv = ctx_srv;
    srv->on_connect = sslws_ev_connect;
    srv->on_handle = on_ws_handle;
    srv->on_recv = sslws_ev_recv;
    srv->on_send = ssl_ev_send;
    srv->on_recvd = ws_ev_recvd;
    srv->ev_bufsize = sizeof(ev_buf_t);
        srv->on_evbuf_set = ws_evbuf_set;
    srv->on_evbuf_free = ws_rpc_evbuf_free;
    srv->on_event = ws_rpc_srv_event;
    srv->on_close_ev = ssl_close_ev;
    srv->on_free_ev = ssl_free_ev;
    srv->msg_info = ws_create_msg_info();
    srv->userdata = jsonrpc_init();
    return srv;
err:
    if (ctx_srv) SSL_CTX_free(ctx_srv);
    if (ctx_cli) SSL_CTX_free(ctx_cli);
    net_srv_done(srv);
    return NULL;
}

void sslws_srv_done (net_srv_t *srv) {
    SSL_CTX *ctx;
    if ((ctx = srv->ctx_srv))
        SSL_CTX_free(ctx);
    if ((ctx = srv->ctx_cli))
        SSL_CTX_free(ctx);
    free_msginfo(srv->msg_info);
    ws_srv_done(srv);
}

net_srv_t *sslws_jsonrpc_srv_init (const char *svc, const char *cert_file, const char *key_file) {
    net_srv_t *srv = sslws_srv_init(svc, cert_file, key_file);
    if (srv) {
        srv->userdata = jsonrpc_init();
        srv->on_evbuf_free = ws_jsonrpc_evbuf_free;
        srv->on_event = ws_jsonrpc_srv_event;
        srv->on_check_url = check_api_ver;
    }
    return srv;
}

void sslws_jsonrpc_srv_done (net_srv_t *srv) {
    jsonrpc_done((jsonrpc_method_list_t**)&srv->userdata);
    sslws_srv_done(srv);
}

#endif
