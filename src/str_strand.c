/*Copyright (c) Brian B.

Permission to use, copy, modify, and/or distribute this software for any purpose
with or without fee is hereby granted.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*/
#include "../include/libex/str.h"

static char al_nums ['9'-'0'+1+'Z'-'A'+1+'z'-'a'+1];
static char als ['Z'-'A'+1+'z'-'a'+1];
static char al_reg_nums ['9'-'0'+1+'Z'-'A'+1];
static char al_regs ['Z'-'A'+1];
static char nums ['9'-'0'+1];

void b64_generate_decode_table(void);
#ifdef __GNUC__
__attribute__ ((constructor))
#endif
void libex_str_init () {
    char c,
         *p_al_nums = al_nums,
         *p_als = als,
         *p_al_reg_nums = al_reg_nums,
         *p_al_regs = al_regs,
         *p_nums = nums;
    c = '0';
    while (c <= '9')
        *p_nums++ = *p_al_nums++ = *p_al_reg_nums++ = c++;
    c = 'A';
    while (c <= 'Z')
        *p_al_nums++ = *p_als++ = *p_al_reg_nums++ = *p_al_regs++ = c++;
    c = 'a';
    while (c <= 'z')
        *p_al_nums++ = *p_als++ = c++;
    srand(time(0));
    b64_generate_decode_table();
}

typedef int (*charconv_h) (int);

static void rand_gen (char *buf, size_t len, char *templ, size_t templ_len, charconv_h conv) {
    int i;
    if (conv) {
        for (i = 0; i < len-1; ++i)
            buf[i] = conv(templ[rand() % templ_len]);
    } else {
        for (i = 0; i < len-1; ++i)
            buf[i] = templ[rand() % templ_len];
    }
    buf[i] = '\0';
}

#define RAND_WHAT_CHARS 0x000f
#define RAND_WHAT_REG 0x00f0
int strand (char *outbuf, size_t outlen, int flags) {
    char *buf = NULL;
    size_t buf_len;
    charconv_h conv = NULL;
    int type_chars = flags & RAND_WHAT_CHARS,
        type_reg = flags & RAND_WHAT_REG;
    if ((type_chars & RAND_ALNUM))
        buf = al_nums;
    else
    if ((type_chars & RAND_ALPHA))
        buf = als;
    else
        buf = nums;
    if (type_chars && type_reg) {
        if (buf == al_nums)
            buf = al_reg_nums;
        else
        if (buf == als)
            buf = al_regs;
        if ((type_reg & RAND_UPPER))
            conv = toupper;
        else
        if ((type_reg & RAND_LOWER))
            conv = tolower;
    }
    if (buf == al_nums)
        buf_len = sizeof(al_nums);
    else
    if (buf == als)
        buf_len = sizeof(als);
    else
    if (buf == al_reg_nums)
        buf_len = sizeof(al_reg_nums);
    else
    if (buf == al_regs)
        buf_len = sizeof(al_regs);
    else
        buf_len = sizeof(nums);
    rand_gen(outbuf, outlen, buf, buf_len, conv);
    return 0;
}
