/*Copyright (c) Brian B.

Permission to use, copy, modify, and/or distribute this software for any purpose
with or without fee is hereby granted.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*/
#include "../include/libex/task.h"

static int runp (exit_process_t *ep, const char *cmd, const char **args) {
    int status;
    pid_t pid = fork();
    if (-1 == pid) return -1;
    if (0 == pid) {
        execvp(args[0], (char * const*)args);
        exit(0);
    }
    waitpid(pid, &status, WUNTRACED | WCONTINUED);
    if (WIFEXITED(status)) {
        ep->status = EP_EXITED;
        ep->code = WEXITSTATUS(status);
    } else if (WIFSIGNALED(status)) {
        ep->status = EP_SIGNALED;
        ep->code = WTERMSIG(status);
    } else if (WIFSTOPPED(status)) {
        ep->status = EP_STOPPED;
        ep->code = WSTOPSIG(status);
    }
    return 0;
}

static int run_args (va_list ap) {
    int argc = 1;
    while (va_arg(ap, const char*))
        ++argc;
    return argc;
}

static int runv (exit_process_t *ep, const char *cmd, int argc, va_list ap) {
    const char **args;
    int rc;
    args = calloc(argc, sizeof(const char*));
    args[0] = cmd;
    for (int i = 1; i < argc; ++i)
        args[i] = va_arg(ap, const char*);
    rc = runp(ep, cmd, args);
    free(args);
    return rc;
}

int runf (exit_process_t *ep, const char *cmd, ...) {
    va_list ap;
    va_start(ap, cmd);
    int argc = run_args(ap);
    va_end(ap);
    va_start(ap, cmd);
    int rc = runv(ep, cmd, argc, ap);
    va_end(ap);
    return rc;
}

int run (exit_process_t *ep, const char *cmd) {
    char *s = strdup(cmd),  *p = s, c, *e, **args;
    int argc = 1, i = 1, rc;
    while (*p) {
        while (isspace(*p)) ++p;
        if (*p) ++argc;
        c = *p;
        if ('\'' == c || '"' == c) {
            ++p;
            while (*p && c != *p++);
            if (c == *p) ++p;
        } else
            while (*p && !isspace(*p++));
    }
    e = p;
    p = s;
    args = calloc(argc+1, sizeof(const char*));
    while (p < e) {
        while (isspace(*p))
            *p++ = '\0';
        if (*p) {
            args[i] = p;
            c = *p;
            if ('\'' == c || '"' == c) {
                args[i] = ++p;
                while (*p && *p != c) ++p;
                if (*p == c) *p++ = '\0';
                ++i;
            } else {
                while (!isspace(*p)) ++p;
                if (*p) *p++ = '\0';
                ++i;
            }
        }
    }
    args[0] = args[1];
    rc = runp(ep, args[0], (const char**)args);
    free(args);
    free(s);
    return rc;
}
