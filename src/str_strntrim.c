/*Copyright (c) Brian B.

Permission to use, copy, modify, and/or distribute this software for any purpose
with or without fee is hereby granted.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*/
#include "../include/libex/str.h"

strptr_t strntrim (char *str, size_t str_len) {
    strptr_t res = { .ptr = str, .len = str_len };
    if (str_len == 0) return res;
    char *e = res.ptr + res.len - 1;
    while (res.ptr <= e && isspace(*res.ptr)) ++res.ptr;
    while (res.ptr <= e && isspace(*e)) --e;
    if (!isspace(*e)) ++e;
    res.len = (uintptr_t)e - (uintptr_t)res.ptr;
    return res;
}
