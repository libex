/*Copyright (c) Brian B.

Permission to use, copy, modify, and/or distribute this software for any purpose
with or without fee is hereby granted.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*/
#include "../include/libex/str.h"

wstr_t *wstralloc (size_t len, size_t chunk_size) {
    size_t nlen = len;
    if (!chunk_size) chunk_size = DEFAULT_STRLEN;
    if (nlen < chunk_size) nlen = chunk_size;
    size_t bufsize = (nlen / chunk_size) * chunk_size;
    if (len >= bufsize) bufsize += chunk_size;
    wstr_t *ret = malloc(sizeof(wstr_t) + bufsize * sizeof(wchar_t));
    if (!ret) return NULL;
    ret->ptr[0] = 0;
    ret->len = 0;
    ret->bufsize = bufsize;
    ret->chunk_size = chunk_size;
    return ret;
}
