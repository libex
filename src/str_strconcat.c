/*Copyright (c) Brian B.

Permission to use, copy, modify, and/or distribute this software for any purpose
with or without fee is hereby granted.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*/
#include "../include/libex/str.h"

str_t *strconcat (str_t *res, size_t chunk_size, const char *arg, size_t arg_len, ...) {
    size_t start_len = arg_len;
    va_list ap;
    errno = 0;
    if (!arg || !arg_len)
        return NULL;
    if (!res) {
        va_start(ap, arg_len);
        while (1) {
            const char *s = va_arg(ap, const char*);
            if (!s)
                break;
            size_t n = va_arg(ap, size_t);
            start_len += n;
        }
        va_end(ap);
        res = stralloc(start_len, chunk_size);
        if (!res)
            return NULL;
    }
    if (-1 == strnadd(&res, arg, arg_len))
        return res;
    va_start(ap, arg_len);
    while (1) {
        const char *s = va_arg(ap, const char*);
        if (!s)
            break;
        size_t n = va_arg(ap, size_t);
        if (!n)
            break;
        if (-1 == strnadd(&res, s, n))
            break;
    }
    va_end(ap);
    return res;
}
