/*Copyright (c) Brian B.

Permission to use, copy, modify, and/or distribute this software for any purpose
with or without fee is hereby granted.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE
OF THIS SOFTWARE.
*/
#include "../include/libex/json.h"

const char *delims = "{}[],:";

enum { JSON_OK, JSON_FIN, JSON_ERROR };

DECLARE_LIST_FUN(json_array,json_item_t)
DECLARE_RBTREE_FUN(json_object,json_item_t)

static int json_get_token (json_t *j, strptr_t *token) {
    char *p = j->text_ptr, *q, *e = j->text + j->text_len;
    token->ptr = NULL;
    token->len = 0;
    while (p < e && isspace(*p)) ++p;
    if (p >= e) {
        errno = EAGAIN;
        return JSON_FIN;
    }
    q = p;
    if ('"' == *p) {
        token->ptr = p++;
        while (p < e) {
            if (*p == '"')
                break;
            if (*p == '\\')
                ++p;
            ++p;
        }
        if (p >= e) {
            errno = EAGAIN;
            return JSON_ERROR;
        }
        j->text_ptr = ++p;
        token->ptr = q;
        token->len = (uintptr_t)p - (uintptr_t)q;
        return JSON_OK;
    }
    switch (*p++) {
        case '{':
        case '}':
        case ',':
        case ':':
        case '[':
        case ']':
            token->ptr = q;
            token->len = 1;
            j->text_ptr = p;
            return JSON_OK;
    }
    while (p < e && ! isspace(*p) && !strchr(delims, *p)) ++p;
    e = p;
    if (e > q) {
        token->ptr = q;
        token->len = (uintptr_t)p - (uintptr_t)q;
        j->text_ptr = p;
    }
    return JSON_OK;
}

// free json objects
static void json_free_object (json_object_t *jo);
static void json_free_array (json_array_t *ja);
static void json_free_item (json_item_t *ji);

static void on_json_item_free (json_item_t *value) {
    json_free_item(value);
}

static void json_free_object (json_object_t *jo) {
    free_json_object(jo);
}

static void json_free_array (json_array_t *ja) {
    free_json_array(ja);
}

static void json_clear_item (json_item_t *ji) {
    switch (ji->type) {
        case JSON_OBJECT:
            if (ji->data.o)
                json_free_object(ji->data.o);
            break;
        case JSON_ARRAY:
            if (ji->data.a)
                json_free_array(ji->data.a);
            break;
        default: break;
    }
}

static void json_free_item (json_item_t *ji) {
    json_clear_item(ji);
}

void json_free (json_t *j) {
    switch (j->type) {
        case JSON_OBJECT:
            json_free_object(j->data.o);
            break;
        case JSON_ARRAY:
            json_free_array(j->data.a);
            break;
        default: break;
    }
    free(j);
}

int json_str2long (strptr_t *str, int64_t *l) {
    char *s = strndup(str->ptr, str->len), *tail = NULL;
    if (!isdigit(*s) && '-' != *s && '+' != *s) {
        free(s);
        return -1;
    }
    *l = strtoll(s, &tail, 0);
    int ret = *tail != '\0' || errno == ERANGE ? -1 : 0;
    free(s);
    return ret;
}

int json_str2double (strptr_t *str, long double *d) {
    char *s = strndup(str->ptr, str->len), *tail = NULL;
    if (!isdigit(*s) && '-' != *s && '+' != *s) {
        free(s);
        return -1;
    }
    *d = strtold(s, &tail);
    int ret = *tail != '\0' || errno == ERANGE ? -1 : 0;
    free(s);
    return ret;
}

static void json_set_item_value (json_t *j, json_item_t *ji, strptr_t *token) {
    if ('"' == token->ptr[0] && '"' == token->ptr[token->len-1]) {
        token->ptr++;
        token->len -= 2;
        ji->type = JSON_STRING;
        ji->str = ji->data.s = *token;
    } else {
        ji->str = *token;
        if (0 == json_str2long(token, &ji->data.i))
            ji->type = JSON_INTEGER;
        else if (0 == json_str2double(token, &ji->data.d))
            ji->type = JSON_DOUBLE;
        else if (0 == cmpcasestr(token->ptr, token->len, CONST_STR_LEN("TRUE")))
            ji->type = JSON_TRUE;
        else if (0 == cmpcasestr(token->ptr, token->len, CONST_STR_LEN("FALSE")))
            ji->type = JSON_FALSE;
        else if (0 == cmpcasestr(token->ptr, token->len, CONST_STR_LEN("NULL")))
            ji->type = JSON_NULL;
        else {
            ji->type = JSON_STRING;
            ji->data.s = *token;
        }
    }
}

static json_array_t *json_parse_array (json_t *j, strptr_t *token);
static int json_parse_item (json_t *j, json_item_t *ji, strptr_t *token);
static json_object_t *json_parse_object(json_t *j, strptr_t *token);

static json_array_t *json_parse_array (json_t *j, strptr_t *token) {
    json_array_t *a = create_json_array();
    a->on_free = on_json_item_free;
    while (JSON_OK == json_get_token(j, token)) {
        json_item_t ji;
        if (0 == cmpstr(token->ptr, token->len, CONST_STR_LEN("]")))
            break;
        memset(&ji, 0, sizeof ji);
        if (0 == cmpstr(token->ptr, token->len, CONST_STR_LEN("{"))) {
            ji.type = JSON_OBJECT;
            if (!(ji.data.o = json_parse_object(j, token))) {
                json_free_item(&ji);
                goto err;
            }
        } else
        if (0 == cmpstr(token->ptr, token->len, CONST_STR_LEN("["))) {
            ji.type = JSON_ARRAY;
            if (!(ji.data.a = json_parse_array(j, token))) {
                json_free_item(&ji);
                goto err;
            }
        } else
            json_set_item_value(j, &ji, token);
        add_tail_json_array(a, &ji, 0);
        if (JSON_OK != json_get_token(j, token)) goto err;
        if (0 == cmpstr(token->ptr, token->len, CONST_STR_LEN(",")))
            continue;
        if (0 == cmpstr(token->ptr, token->len, CONST_STR_LEN("]")))
            break;
        goto err;
    }
    return a;
err:
    json_free_array(a);
    return NULL;
}

static int json_parse_item (json_t *j, json_item_t *ji, strptr_t *token) {
    if ('"' != token->ptr[0] || '"' != token->ptr[token->len-1])
        return -1;
    token->ptr++;
    token->len -= 2;
    ji->key = *token;
    if (JSON_OK != json_get_token(j, token) ||
        0 != cmpstr(token->ptr, token->len, CONST_STR_LEN(":")) ||
        JSON_OK != json_get_token(j, token))
            return -1;
    if (0 == cmpstr(token->ptr, token->len, CONST_STR_LEN("{"))) {
        ji->type = JSON_OBJECT;
        if (!(ji->data.o = json_parse_object(j, token)))
            return -1;
    } else
    if (0 == cmpstr(token->ptr, token->len, CONST_STR_LEN("["))) {
        ji->type = JSON_ARRAY;
        if (!(ji->data.a = json_parse_array(j, token)))
            return -1;
    } else
        json_set_item_value(j, ji, token);
    return 0;
}

static int on_json_item_compare (const json_item_t *x, json_item_t *y) {
    return cmpstr(x->key.ptr, x->key.len, y->key.ptr, y->key.len);
}

static json_object_t *json_parse_object(json_t *j, strptr_t *token) {
    json_object_t *o = create_json_object();
    o->on_compare = on_json_item_compare;
    o->on_free = on_json_item_free;
    while (JSON_OK == json_get_token(j, token)) {
        json_item_t ji;
        if (0 == cmpstr(token->ptr, token->len, CONST_STR_LEN("}")))
            break;
        memset(&ji, 0, sizeof ji);
        if (-1 == json_parse_item(j, &ji, token))
            goto err;
        add_json_object(o, &ji);
        if (EEXIST == errno) {
            json_free_item(&ji);
            goto err;
        }
        if (JSON_OK != json_get_token(j, token))
            goto err;
        if (0 == cmpstr(token->ptr, token->len, CONST_STR_LEN(",")))
            continue;
        if (0 == cmpstr(token->ptr, token->len, CONST_STR_LEN("}")))
            break;
        goto err; // FIXME
    }
    return o;
err:
    json_free_object(o);
    return NULL;
}

json_t *json_parse_len (const char *json_str, size_t json_str_len) {
    json_t *j = calloc(1, sizeof(json_t));
    strptr_t token;
    errno = 0;
    j->text = j->text_ptr = (char*)json_str;
    j->text_len = json_str_len;
    if (JSON_ERROR == json_get_token(j, &token)) goto err;
    if (0 == cmpstr(token.ptr, token.len, CONST_STR_LEN("{"))) {
        j->type = JSON_OBJECT;
        if (!(j->data.o = json_parse_object(j, &token))) goto err;
    } else
    if (0 == cmpstr(token.ptr, token.len, CONST_STR_LEN("["))) {
        j->type = JSON_ARRAY;
        if (!(j->data.a = json_parse_array(j, &token))) goto err;
    } else
        goto err;
    return j;
err:
    free(j);
    return NULL;
}

json_item_t *json_find (json_object_t *jo, const char *key, size_t key_len, int type) {
    json_item_t x = { .key = { .ptr = (char*)key, .len = key_len } },
               *y = find_json_object(jo, &x);
    return y && y->type == type ? y : NULL;
}

cstr_t *json_item_as_str (json_item_t *ji) {
    char buf [32];
    int len;
    switch (ji->type) {
        case JSON_NULL:
            return mkcstr(CONST_STR_LEN("null"));
        case JSON_STRING:
            return mkcstr(ji->data.s.ptr, ji->data.s.len);
        case JSON_INTEGER:
            len = snprintf(buf, sizeof buf, LONG_FMT, ji->data.i);
            return mkcstr(buf, len);
        case JSON_DOUBLE:
            len = snprintf(buf, sizeof buf, "%Lf", ji->data.d);
            return mkcstr(buf, len);
        case JSON_TRUE:
            return mkcstr(CONST_STR_LEN("true"));
        case JSON_FALSE:
            return mkcstr(CONST_STR_LEN("false"));
        case JSON_OBJECT:
            return mkcstr(CONST_STR_LEN("(object)"));
        case JSON_ARRAY:
            return mkcstr(CONST_STR_LEN("(array)"));
    }
    return NULL;
}
