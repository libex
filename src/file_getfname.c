/*Copyright (c) Brian B.

Permission to use, copy, modify, and/or distribute this software for any purpose
with or without fee is hereby granted.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*/
#include "../include/libex/file.h"

int getfname (const char *path, size_t path_len, strptr_t *result) {
    memset(result, 0, sizeof(strptr_t));
    if (0 == path_len) return -1;
    char *p = strnrchr(path, PATH_DELIM_C, path_len);
    if (!p) p = (char*)path;
    if (*p == PATH_DELIM_C) {
        if (1 == path_len) return -1;
        ++p;
    }
    result->ptr = p;
    result->len = path_len - ((uintptr_t)p - (uintptr_t)path);
    return 0;
}
