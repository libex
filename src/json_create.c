/*Copyright (c) Brian B.

Permission to use, copy, modify, and/or distribute this software for any purpose
with or without fee is hereby granted.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE
OF THIS SOFTWARE.
*/
#include "../include/libex/json.h"

size_t json_prefix_len = 0;
char json_prefix = ' ';

int json_enum_array (json_array_t *a, json_item_h fn, void *userdata, int flags) {
    return foreach_json_array(a, fn, userdata, flags);
}

int json_enum_object (json_object_t *obj, json_item_h fn, void *userdata, int flags) {
    return foreach_json_object(obj, fn, userdata, flags);
}

void json_add_key (strbuf_t *buf, const char *key, size_t key_len) {
    strbufadd(buf, CONST_STR_LEN("\""));
    strbufadd(buf, key, key_len);
    strbufadd(buf, CONST_STR_LEN("\":"));
}

#define IS_JSON_START(x) (x)->len > json_prefix_len && \
                         (x)->ptr[(x)->len-1] != '{' && \
                         (x)->ptr[(x)->len-1] != '[' && \
                         (x)->ptr[(x)->len-1] != ':'

void json_add_str (strbuf_t *buf, const char *key, size_t key_len, const char *val, size_t val_len) {
    if (IS_JSON_START(buf))
        strbufadd(buf, CONST_STR_LEN(","));
    if (key && key_len)
        json_add_key(buf, key, key_len);
    strbufadd(buf, CONST_STR_LEN("\""));
    strbufadd(buf, val, val_len);
    strbufadd(buf, CONST_STR_LEN("\""));
}

void json_add_escape_str (strbuf_t *buf, const char *key, size_t key_len, const char *val, size_t val_len) {
    if (IS_JSON_START(buf))
        strbufadd(buf, CONST_STR_LEN(","));
    if (key && key_len)
        json_add_key(buf, key, key_len);
    strbufadd(buf, CONST_STR_LEN("\""));
    strbuf_escape(buf, val, val_len);
    strbufadd(buf, CONST_STR_LEN("\""));
}

void json_add_unescape_str (strbuf_t *buf, const char *key, size_t key_len, const char *val, size_t val_len) {
    if (IS_JSON_START(buf))
        strbufadd(buf, CONST_STR_LEN(","));
    if (key && key_len)
        json_add_key(buf, key, key_len);
    strbufadd(buf, CONST_STR_LEN("\""));
    strbuf_unescape(buf, val, val_len);
    strbufadd(buf, CONST_STR_LEN("\""));
}

void json_add_int (strbuf_t *buf, const char *key, size_t key_len, int64_t val) {
    char str [24];
    int len = snprintf(str, sizeof(str), LONG_FMT, val);
    if (IS_JSON_START(buf))
        strbufadd(buf, CONST_STR_LEN(","));
    if (key && key_len)
        json_add_key(buf, key, key_len);
    strbufadd(buf, str, len);
}

void json_add_double_p (strbuf_t *buf, const char *key, size_t key_len, long double val, int prec) {
    char str [48];
    if (prec <= 0)
        prec = JSON_DOUBLE_PRECISION;
    int len = snprintf(str, sizeof(str), "%.*Lf", prec, val);
    if (IS_JSON_START(buf))
        strbufadd(buf, CONST_STR_LEN(","));
    if (key && key_len)
        json_add_key(buf, key, key_len);
    strbufadd(buf, str, len);
}

void json_add_null (strbuf_t *buf, const char *key, size_t key_len) {
    if (IS_JSON_START(buf))
        strbufadd(buf, CONST_STR_LEN(","));
    if (key && key_len)
        json_add_key(buf, key, key_len);
    strbufadd(buf, CONST_STR_LEN("null"));
}

void json_add_bool (strbuf_t *buf, const char *key, size_t key_len, int val) {
    char *str = 0 == val ? "false" : "true";
    if (IS_JSON_START(buf))
        strbufadd(buf, CONST_STR_LEN(","));
    if (key && key_len)
        json_add_key(buf, key, key_len);
    strbufadd(buf, str, strlen(str));
}

void json_open_array (strbuf_t *buf, const char *key, size_t key_len) {
    if (IS_JSON_START(buf))
        strbufadd(buf, CONST_STR_LEN(","));
    if (key && key_len)
        json_add_key(buf, key, key_len);
    strbufadd(buf, CONST_STR_LEN("["));
}

void json_close_array (strbuf_t *buf) {
    if (buf->ptr[buf->len-1] == ',')
        buf->ptr[buf->len-1] = ']';
    else
        strbufadd(buf, CONST_STR_LEN("]"));
}

void json_open_object (strbuf_t *buf, const char *key, size_t key_len) {
    if (IS_JSON_START(buf))
        strbufadd(buf, CONST_STR_LEN(","));
    if (key && key_len)
        json_add_key(buf, key, key_len);
    strbufadd(buf, CONST_STR_LEN("{"));
}

void json_close_object (strbuf_t *buf) {
    if (buf->ptr[buf->len-1] == ',')
        buf->ptr[buf->len-1] = '}';
    else
        strbufadd(buf, CONST_STR_LEN("}"));
}
