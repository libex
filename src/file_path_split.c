/*Copyright (c) Brian B.

Permission to use, copy, modify, and/or distribute this software for any purpose
with or without fee is hereby granted.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*/
#include "../include/libex/file.h"

void path_split (const char *path, size_t path_len, str_t **dir, str_t **fname, str_t **suf) {
    char *d = (char*)path, *f, *s;
    size_t dl = path_len, fl, sl;
    if ((f = strnrchr(path, PATH_DELIM_C, path_len))) {
        --dl;
        fl = dl - ((uintptr_t)f - (uintptr_t)path);
        dl -= fl;
        ++f;
        if ((s = strnchr(f, '.', fl))) {
            --fl;
            sl = fl - ((uintptr_t)s - (uintptr_t)f);
            fl -= sl;
            ++s;
        }
    } else {
        f = d;
        fl = dl;
        d = NULL;
        if ((s = strnrchr(f, '.', fl))) {
            sl = fl - ((uintptr_t)s - (uintptr_t)f);
            fl -= sl;
            ++s;
            --sl;
        }
    }
    *dir = *fname = *suf = NULL;
    if (d) { *dir = mkstr(d, dl, 8); STR_ADD_NULL(*dir); }
    if (f) { *fname = mkstr(f, fl, 8); STR_ADD_NULL(*fname); }
    if (s) { *suf = mkstr(s, sl, 8); STR_ADD_NULL(*suf); }
}

