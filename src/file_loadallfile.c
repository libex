/*Copyright (c) Brian B.

Permission to use, copy, modify, and/or distribute this software for any purpose
with or without fee is hereby granted.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*/
#include "../include/libex/file.h"

str_t *load_all_file (const char *path, size_t chunk_size, size_t max_size) {
    str_t *ret = NULL;
    errno = 0;
    int fd = open(path, O_RDONLY);
    if (-1 != fd) {
        struct stat st;
        if (-1 != fstat(fd, &st) && ((max_size > 0 && st.st_size <= max_size) || 0 == max_size)) {
            ret = stralloc(st.st_size, chunk_size);
            #ifndef NDEBUG
            memset(ret->ptr, 0, st.st_size+1);
            #endif
            if (st.st_size == read(fd, ret->ptr, st.st_size))
                ret->len = st.st_size;
            else {
                free(ret);
                ret = NULL;
            }
        }
        close(fd);
    }
    return ret;
}
