/*Copyright (c) Brian B.
  Copyright (c) PostgreSQL Global Development Group

Permission to use, copy, modify, and/or distribute this software for any purpose
with or without fee is hereby granted.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*/
#include "../include/libex/time.h"

static const char *mon_names [] = {
    "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"
};

#ifdef __m64__
#define CONST_I64(x) (x##LL)
#define CONST_UI64(x) (x##ULL)
#else
#define CONST_I64(x) (x##L)
#define CONST_UL64(x) (x##UL)
#endif

#define MONTHS_PER_YEAR 12

#define IS_LEAP_YEAR(y) (((y) % 4) == 0 && (((y) % 100) != 0 || ((y) % 400) == 0))

#define JTM_MINYEAR (-4713)
#define JTM_MINMON (11)
#define JTM_MINDAY (24)
#define JTM_MAXYEAR (5874898)
#define JTM_MAXMON (6)
#define JTM_MAXDAY (3)
#define JTM_BASE (2451545)

#define SECS_PER_YEAR (36525 * 864)
#define SECS_PER_DAY 86400
#define SECS_PER_HOUR 3600
#define SECS_PER_MINUTE 60
#define MINS_PER_HOUR 60

#define USECS_PER_DAY CONST_I64(86400000000)
#define USECS_PER_HOUR CONST_I64(3600000000)
#define USECS_PER_MINUTE CONST_I64(60000000)
#define USECS_PER_SEC CONST_I64(1000000)

#define IS_JTMVALID(y,m,d) \
    (((y) > JTM_MINYEAR || \
      ((y) == JTM_MINYEAR && ((m) >= JTM_MINMON))) && \
     ((y) < JTM_MAXYEAR || \
      ((y) == JTM_MAXYEAR && ((m) < JTM_MAXMON))))
#define MIN_TIMESTAMP CONST_I64(-211813488000000000)
#define END_TIMESTAMP CONST_I64(9223371331200000000)
#define IS_TM_VALID(t) (MIN_TIMESTAMP <= (t) && (t) < END_TIMESTAMP)

#define TMOD(t,q,u) \
do { \
    (q) = ((t) / (u)); \
    if ((q) != 0) (t) -= ((q) * (u)); \
} while(0)

#define DT_NOBEGIN (-CONST_I64(0x7fffffffffffffff) - 1)
#define DT_NOEND (CONST_I64(0x7fffffffffffffff))

#define IS_TM_NOBEGIN(j) ((j) == DT_NOBEGIN)
#define IS_TM_NOEND(j) ((j) == DT_NOEND)
#define TM_NOT_FIN(j) (IS_TM_NOBEGIN(j) || IS_TM_NOEND(j))

static const int day_tab[2][13] =
{
    {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31, 0},
    {31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31, 0}
};

static void j2dat (int jd, int *year, int *month, int *day) {
    unsigned int j;
    unsigned int quad;
    unsigned int extra;
    int y;
    j = jd;
    j += 32044;
    quad = j / 146097;
    extra = (j - quad * 146097) * 4 + 3;
    j += 60 + quad * 3 + extra / 146097;
    quad = j / 1461;
    j -= quad * 1461;
    y = j * 4 / 1461;
    j = ((y != 0) ? ((j + 305) % 365) : ((j + 306) % 366)) + 123;
    y += quad * 4;
    *year = y - 4800;
    quad = j * 2141 / 65536;
    *day = j - 7834 * quad / 256;
    *month = (quad + 10) % MONTHS_PER_YEAR + 1;
}

static int dat2j (int y, int m, int d) {
    int j;
    int century;
    if (m > 2) {
        m += 1;
        y += 4800;
    } else {
        m += 13;
        y += 4799;
    }
    century = y / 100;
    j = y * 365 - 32167;
    j += y / 4 - century + century / 4;
    j += 7834 * m / 256 + d;
    return j;
}

static void dt2time (tm_t jd, int *hour, int *min, int *sec) {
    tm_t time;
    time = jd;
    *hour = time / USECS_PER_HOUR;
    time -= ((tm_t)*hour) * USECS_PER_HOUR;
    *min = time / USECS_PER_MINUTE;
    time -= ((tm_t)*min) * USECS_PER_MINUTE;
    *sec = time / USECS_PER_SEC;
}

static tm_t tim2t(const int hour, const int min, const int sec) {
    return (((((tm_t)hour * MINS_PER_HOUR) + (tm_t)min) * SECS_PER_MINUTE) + (tm_t)sec) * USECS_PER_SEC;
}

tm_t tm_enc (struct tm *tm) {
    int dat;
    tm_t tim, res;
    errno = ERANGE;
    if (!IS_JTMVALID(tm->tm_year, tm->tm_mon, tm->tm_mday))
        return -1;
    dat = dat2j(tm->tm_year, tm->tm_mon, tm->tm_mday) - JTM_BASE;
    tim = tim2t(tm->tm_hour, tm->tm_min, tm->tm_sec);
    res = dat * USECS_PER_DAY + tim;
    if ((res - tim) / USECS_PER_DAY != dat)
        return -1;
    if ((res < 0 && dat > 0) || (res > 0 && dat < -1))
        return -1;
    if (!IS_TM_VALID(res))
        return -1;
    return res;
}

struct tm *tm_dec (tm_t t, struct tm *tm) {
    tm_t dat, dat0, tim;
    dat0 = JTM_BASE;
    tim = t;
    TMOD(tim, dat, USECS_PER_DAY);
    if (tim < CONST_I64(0)) {
        tim += USECS_PER_DAY;
        dat -= 1;
    }
    dat += dat0;
    if (dat < 0 || dat > (tm_t)INT_MAX)
        return NULL;
    if (!tm) tm = calloc(1, sizeof(struct tm));
    j2dat((int)dat, &tm->tm_year, &tm->tm_mon, &tm->tm_mday);
    dt2time(tim, &tm->tm_hour, &tm->tm_min, &tm->tm_sec);
    tm->tm_isdst = -1;
    tm->tm_yday = dat - dat2j(tm->tm_year, 1, 1) + 1;
    return tm;
}

tm_t tm_add (tm_t t, tmdiff_t *dif) {
    errno = ERANGE;
    if (TM_NOT_FIN(t))
        return t;
    if (0 != dif->mon) {
        struct tm tm;
        if (!tm_dec(t, &tm))
            return -1;
        tm.tm_mon += dif->mon;
        if (tm.tm_mon > MONTHS_PER_YEAR) {
            tm.tm_year += (tm.tm_mon - 1) / MONTHS_PER_YEAR;
            tm.tm_mon = (tm.tm_mon - 1) % MONTHS_PER_YEAR + 1;
        } else if (tm.tm_mon < 1) {
            tm.tm_year += tm.tm_mon / MONTHS_PER_YEAR - 1;
            tm.tm_mon = tm.tm_mon % MONTHS_PER_YEAR + MONTHS_PER_YEAR;
        }
        if (tm.tm_mday > day_tab[IS_LEAP_YEAR(tm.tm_year)][tm.tm_mon-1])
            tm.tm_mday = day_tab[IS_LEAP_YEAR(tm.tm_year)][tm.tm_mon-1];
        if (-1 == (t = tm_enc(&tm)))
            return -1;
    }
    t += dif->time;
    return t;
}

tm_t tm_sub (tm_t t, tmdiff_t *dif) {
    struct tm_diff d = { .time = -dif->time, .mon = -dif->mon };
    return tm_add(t, &d);
}

tmdiff_t tm_diff (tm_t begin, tm_t end) {
    tmdiff_t dif;
    errno = 0;
    if (TM_NOT_FIN(begin) || TM_NOT_FIN(end)) {
        errno = ERANGE;
        return (tmdiff_t){ .time = -1, .mon = -1 };
    }
    dif.time = end - begin;
    dif.mon = 0;
    return dif;
}

struct tm *tm_decdiff (tmdiff_t dif, struct tm *tm) {
    tm_t tim;
    if (0 != dif.mon) {
        tm->tm_year = dif.mon / MONTHS_PER_YEAR;
        tm->tm_mon = dif.mon % MONTHS_PER_YEAR;
    } else {
        tm->tm_year = 0;
        tm->tm_mon = 0;
    }
    tim = dif.time;
    tm->tm_mday = tim / USECS_PER_DAY;
    tim -= tm->tm_mday * USECS_PER_DAY;
    tm->tm_hour = tim / USECS_PER_HOUR;
    tim -= tm->tm_hour * USECS_PER_HOUR;
    tm->tm_min = tim / USECS_PER_MINUTE;
    tim -= tm->tm_min * USECS_PER_MINUTE;
    tm->tm_sec = tim / USECS_PER_SEC;
    return tm;
}

tmdiff_t tm_encdiff (struct tm *tm) {
    tmdiff_t dif = { .mon = -1, .time = -1 };
    errno = 0;
    if ((double)tm->tm_year * MONTHS_PER_YEAR + tm->tm_mon > INT_MAX ||
          (double)tm->tm_year * MONTHS_PER_YEAR + tm->tm_mon < INT_MIN) {
        errno = ERANGE;
        return dif;
    }
    dif.mon = tm->tm_year * MONTHS_PER_YEAR + tm->tm_mon;
    dif.time = ((tm_t)((((((tm->tm_mday * CONST_I64(24)) +
                    tm->tm_hour) * CONST_I64(60)) +
                    tm->tm_min) * CONST_I64(60)) +
                    tm->tm_sec) * USECS_PER_SEC);
    return dif;
}

const char *tm_moname (int mon) {
    --mon;
    if (mon >= 0 && mon < sizeof(mon_names)/sizeof(char*))
        return mon_names[mon];
    return "???";
}

tm_t tm_now () {
    struct tm tm;
    time_t ts = time(NULL);
    localtime_r(&ts, &tm);
    tm.tm_year += 1900;
    tm.tm_mon++;
    return tm_enc(&tm);
}
