/*Copyright (c) Brian B.

Permission to use, copy, modify, and/or distribute this software for any purpose
with or without fee is hereby granted.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*/
#include "../include/libex/file.h"

const char *log_fname = NULL;

static inline int log_fopen (const char *fname) {
    return open(fname, O_WRONLY | O_CREAT | O_APPEND, 0644);
}

int loginit (const char *fname) {
    struct stat st;
    if (-1 == stat(fname, &st)) {
        if (errno == ENOENT) {
            int fd = log_fopen(fname);
            if (fd >= 0) close(fd); else return -1;
        } else
            return -1;
    }
    log_fname = fname;
    return 0;
}

#define LOGTIME_FMT "%a, %d %b %Y %H:%M:%S "
#define STRFTIME_LEN 32

__attribute__ ((format (printf, 1, 2)))
void slogf (const char *fmt, ...) {
    va_list ap;
    if (!log_fname)
        return;
    va_start(ap, fmt);
    const time_t t = time(0);
    struct tm tm;
    int buflen = 0;
    char *buf = NULL;
    localtime_r(&t, &tm);
    buflen = vsnprintf(NULL, 0, fmt, ap) + STRFTIME_LEN;
    va_end(ap);
    va_start(ap, fmt);
#ifdef DEBUG
    if ((buf = (char*)calloc(1, buflen+2))) {
#else
    if ((buf = (char*)malloc(buflen+2))) {
#endif
        int fd = log_fopen(log_fname);
        if (-1 != fd) {
            int len = strftime(buf, buflen, LOGTIME_FMT, &tm);
            buflen = vsnprintf(buf+len, buflen-len, fmt, ap) + len;
            buf[buflen] = '\n';
            write(fd, buf, buflen+1);
        }
        free(buf);
        close(fd);
    }
    va_end(ap);
}

void slog (const char *str, size_t len) {
    int fd;
    if (!log_fname || -1 == (fd = open(log_fname, O_WRONLY | O_CREAT | O_APPEND, 0644)))
        return;
    time_t t0 = time(0);
    struct tm tm;
    char buf [64];
    localtime_r(&t0, &tm);
    int l = strftime(buf, sizeof buf, "%a, %d %b %Y %H:%M:%S GMT ", &tm);
    write(fd, buf, l);
    if (0 == len)
        len = strlen(str);
    write(fd, str, len);
    write(fd, "\n", 1);
    close(fd);
}
