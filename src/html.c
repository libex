/*Copyright (c) Brian B.

Permission to use, copy, modify, and/or distribute this software for any purpose
with or without fee is hereby granted.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*/
#include "../include/libex/html.h"

#define FOUND_NONE 0
#define FOUND_TAG 1
#define FOUND_TEXT 2
#define TAG_START 0x0001
#define TAG_FIN 0x0002

DECLARE_LIST_FUN(html_tags,html_tag_t)

static void html_clear_tag (html_tag_t *tag) {
    if (tag->tag_s) free(tag->tag_s);
    if (tag->tag_t) free(tag->tag_t);
    if (tag->tag_e) free(tag->tag_e);
    free_html_tags((html_tags_t*)tag->tags);
}

static html_tag_t *html_tag_alloc (html_tag_t *parent, int add_to_head) {
    html_tag_t ntag, *tag = NULL;
    memset(&ntag, 0, sizeof ntag);
    ntag.tags = create_html_tags();
    ntag.parent = parent;
    ((html_tags_t*)ntag.tags)->on_free = html_clear_tag;
    if (parent) {
        if (add_to_head)
            tag = add_head_html_tags((html_tags_t*)parent->tags, &ntag, 0);
        else
            tag = add_tail_html_tags((html_tags_t*)parent->tags, &ntag, 0);
    }
    return tag;
}

void html_clear_tags (html_tags_t *tags) {
    if (tags)
        clear_html_tags(tags);
}

void html_clear (html_t *html) {
    if (html->head && html->head->tags)
        free_html_tags(html->head->tags);
}

static int is_autoclose_tag (strptr_t *tag_name) {
    if (0 == cmpstr(tag_name->ptr, tag_name->len, CONST_STR_LEN("meta"))) return 1;
    if (0 == cmpstr(tag_name->ptr, tag_name->len, CONST_STR_LEN("link"))) return 1;
    if (0 == cmpstr(tag_name->ptr, tag_name->len, CONST_STR_LEN("br"))) return 1;
    if (0 == cmpstr(tag_name->ptr, tag_name->len, CONST_STR_LEN("input"))) return 1;
    if (0 == cmpstr(tag_name->ptr, tag_name->len, CONST_STR_LEN("img"))) return 1;
    if (0 == cmpcasestr(tag_name->ptr, tag_name->len, CONST_STR_LEN("!doctype"))) return 1;
    return 0;
}

int html_get_next (char **ptr, size_t *clen) {
    size_t l = *clen;
    char *p = *ptr;
    while (l > 0 && isspace(*p)) { --l; ++p; }
    if (0 == l) return FOUND_NONE;
    if (*p == '<') {
        *ptr = p; *clen = l;
        return FOUND_TAG;
    }
    return FOUND_TEXT;
}

int html_get_tag (char **ptr, size_t *clen, str_t **tag) {
    int flags = TAG_START;
    char *p = strnchr(*ptr, '>', *clen);
    if (!p || *(p-1) == '<') return -1;
    if (*(p-1) == '/') {
        if (*(p-2) == '<') return -1;
        flags |= TAG_FIN;
    } else
    if (*(*ptr+1) == '/')
        flags = TAG_FIN;
    ++p;
    if (!(*tag = mkstr(*ptr, (uintptr_t)p - (uintptr_t)*ptr, 32))) return -1;
    *clen -= (*tag)->len;
    *ptr = p;
    return flags;
}

int html_get_text (char **ptr, size_t *clen, str_t **tag) {
    char *p = *ptr, *e = p + *clen;
    while (p < e && *p != '<') {
        if (*p == '"') {
            ++p;
            while (p < e && *p != '"') ++p;
        }
        ++p;
    }
    if (p >= e) return -1;
    if (!(*tag = mkstr(*ptr, (uintptr_t)p - (uintptr_t)*ptr, 32))) return -1;
    *ptr = p;
    *clen -= (*tag)->len;
    return 0;
}

int html_get_tag_attr (str_t *tag, int idx, strptr_t *val) {
    if (!tag->ptr) return -1;
    char *p = tag->ptr+1, *q;
    size_t l = tag->len-2;
    for (int i = 0; i < idx; ++i) {
        while (l > 0) {
            while (l > 0 && !isspace(*p)) { --l; ++p; }
            if (l > 0 && *p == '"') {
                --l; ++p;
                while (l > 0 && *p != '"') { --l; ++p; }
            }
            if (0 == l) return -1;
            --l; ++p;
        }
        while (l > 0 && isspace(*p)) { --l; ++p; }
    }
    if (0 == l) return -1;
    q = p;
    while (l > 0 && !isspace(*q)) {
        if (*q == '"') {
            --l; ++p;
            while (l > 0 && *q != '"') { --l; ++p; };
            if (0 == l) return -1;
        }
        --l; ++q;
    }
    val->ptr = p;
    val->len = (uintptr_t)q - (uintptr_t)p;
    return val->len > 0 ? 0 : -1;
}

int html_get_tags (char **ptr, size_t *clen, html_tag_t *parent) {
    int ret;
    while (0 < (ret = html_get_next(ptr, clen))) {
        if (FOUND_TAG == ret) {
            str_t *tag;
            int type;
            if (-1 == (type = html_get_tag(ptr, clen, &tag))) { ret = -1; break; }
            if ((type & TAG_START)) {
                strptr_t tag_name;
                html_tag_t *ntag = html_tag_alloc(parent, 0);
                ntag->tag_s = tag;
                if (-1 == (ret = html_get_tag_attr(tag, 0, &tag_name))) break;
                if (!is_autoclose_tag(&tag_name) && !(type & TAG_FIN)) {
                    if (-1 == (ret = html_get_tags(ptr, clen, ntag))) break;
                }
            } else
            if ((type & TAG_FIN)) {
                parent->tag_e = tag;
                break;
            }
        } else
        if (FOUND_TEXT == ret) {
            str_t *tag;
            if (-1 == (ret = html_get_text(ptr, clen, &tag))) break;
            html_tag_t *ntag = html_tag_alloc(parent, 0);
            ntag->tag_t = tag;
        }
    }
    return ret;
}

int html_parse (char *content, size_t content_length, html_t *html) {
    memset(html, 0, sizeof(html_t));
    html->head = html_tag_alloc(NULL, 0);
    while (content_length > 0 && isspace(*content)) {
        --content_length;
        ++content;
    }
    return html_get_tags(&content, &content_length, html->head);
}

int html_add_tag (html_tag_t *parent,
                  const char *tag_s, size_t tag_s_len,
                  const char *tag_t, size_t tag_t_len,
                  const char *tag_e, size_t tag_e_len,
                  int add_to_head) {
    html_tag_t *ntag = html_tag_alloc(parent, add_to_head);
    if (tag_s && tag_s_len && !(ntag->tag_s = mkstr(tag_s, tag_s_len, 32))) return -1;
    if (tag_t && tag_t_len && !(ntag->tag_t = mkstr(tag_t, tag_t_len, 32))) return -1;
    if (tag_e && tag_e_len && !(ntag->tag_e = mkstr(tag_e, tag_e_len, 32))) return -1;
    return 0;
}

void html_mktags (html_tag_t *tag, str_t **content) {
    html_tag_t *item = get_html_tags_head((html_tags_t*)tag->tags);
    if (tag->tag_s)
        strnadd(content, tag->tag_s->ptr, tag->tag_s->len);
    if (tag->tag_t)
        strnadd(content, tag->tag_t->ptr, tag->tag_t->len);
    if (item) {
        foreach_html_tags((html_tags_t*)item->tags, ({
            int fn (html_tag_t *itag, void *dummy) {
                html_mktags(itag, content);
                return ENUM_CONTINUE;
            } fn;
        }), NULL, 0);
    }
    if (tag->tag_e)
        strnadd(content, tag->tag_e->ptr, tag->tag_e->len);
}

str_t *html_mkcontent (html_t *html, size_t start_len, size_t chunk_size) {
    str_t *content = stralloc(start_len, chunk_size);
    if (html->head)
        html_mktags(html->head, &content);
    return content;
}
