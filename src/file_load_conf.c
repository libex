/*Copyright (c) Brian B.

Permission to use, copy, modify, and/or distribute this software for any purpose
with or without fee is hereby granted.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*/
#include "../include/libex/file.h"

int load_conf_exactly (const char *fname, load_conf_h fn) {
    struct stat st;
    int fd;
    if (-1 == stat(fname, &st)) return -1;
    if (-1 == (fd = open(fname, O_RDONLY))) return -1;
    char *buf = malloc(st.st_size);
    ssize_t readed = read(fd, buf, st.st_size);
    close(fd);
    if (st.st_size != readed) {
        free(buf);
        return -1;
    }
    strptr_t str = { readed, buf };
    strptr_t line;
    while (-1 != strntok(&str.ptr, &str.len, CONST_STR_LEN("\r\n"), &line)) {
        char *p = strnchr(line.ptr, '#', line.len);
        if (p)
            line.len -= (uintptr_t)(line.ptr + line.len) - (uintptr_t)p;
        strptr_t name, val;
        if (0 == strntok(&line.ptr, &line.len, CONST_STR_LEN("="), &name) && -1 != strntok(&line.ptr, &line.len, CONST_STR_LEN("\r\n"), &val))
            fn(fname, &name, &val);
    }
    free(buf);
    return 0;
}

static int load_conf_from_spec (spec_path_t id, const char *fname, load_conf_h fn, int hidden) {
    str_t *path = get_spec_path(id);
    if (!path) return -1;
    if (hidden) {
        path = path_add_path(path, ".", NULL);
        strnadd(&path, fname, strlen(fname));
        STR_ADD_NULL(path);
    } else
        path = path_add_path(path, fname, NULL);
    int rc = load_conf_exactly(path->ptr, fn);
    free(path);
    return rc;
}

int load_conf (const char *fname, load_conf_h fn) {
    int rc = -1;
    rc = load_conf_from_spec(DIR_CONFIG, fname, fn, 0);
    rc = load_conf_from_spec(DIR_USR_CONFIG, fname, fn, 0);
    rc = load_conf_from_spec(DIR_LOCAL_CONFIG, fname, fn, 0);
    rc = load_conf_from_spec(DIR_HOME_CONFIG, fname, fn, 0);
    rc = load_conf_from_spec(DIR_HOME, fname, fn, 1);
    rc = load_conf_from_spec(DIR_CURRENT, fname, fn, 1);
    return rc;
}
