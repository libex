/*Copyright (c) Brian B.

Permission to use, copy, modify, and/or distribute this software for any purpose
with or without fee is hereby granted.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*/
#include "../include/libex/str.h"

int strbuf_url_encode (strbuf_t *encoded, const char *str, size_t str_len, size_t chunk_size) {
    for (size_t i = 0; i < str_len; ++i) {
        unsigned char c = str[i];
        if (!isalnum(c) && '.' != c && '-' != c && '_' != c && '~' != c) {
            if (-1 == strbufsize(encoded, encoded->len + 4, 0)) return -1;
            encoded->len += snprintf(encoded->ptr + encoded->len, 4, "%%%02X", c);
        } else
            if (-1 == strbufadd(encoded, (char*)&c, sizeof(char))) return -1;
    }
    return 0;
}
