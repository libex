/*Copyright (c) Brian B.

Permission to use, copy, modify, and/or distribute this software for any purpose
with or without fee is hereby granted.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*/
#include "../include/libex/file.h"

#define TEMP_NAMLEN 16
#define TEMP_TIMLEN 24
char templ ['9'-'0'+1+'Z'-'A'+1+'z'-'a'+1];

#ifdef __GNUC__
__attribute__ ((constructor))
#endif
void libex_file_init () {
    char *p = templ;
    char c = '0';
    while (c <= '9') { *p++ = c++; };
    c = 'A';
    while (c <= 'Z') { *p++ = c++; };
    c = 'a';
    while (c <= 'z') { *p++ = c++; };
    srand(time(0));
}
str_t *mktempnam (const char *dir, size_t dir_len, const char *prefix, size_t prefix_len) {
    char tempdir [MAX_PATH+1];
    size_t tempdir_len;
    strptr_t oname = { .ptr = NULL, .len = 0 };
    if (-1 == getoname(prefix, prefix_len, &oname))
        return NULL;
    if (!dir || !dir_len) {
        strcpy(tempdir, "/tmp");
        tempdir_len = sizeof("/tmp")-1;
    } else {
        if (dir_len > sizeof(tempdir))
            return NULL;
        tempdir_len = dir_len;
        strncpy(tempdir, dir, dir_len);
    }
    str_t *path = stralloc(tempdir_len + prefix_len + TEMP_NAMLEN + TEMP_TIMLEN + 1, 16);
    strnadd(&path, tempdir, tempdir_len);
    if (oname.ptr && oname.len ) {
        if (path->ptr[path->len-1] != PATH_DELIM_C)
            strnadd(&path, CONST_STR_LEN("/"));
        strnadd(&path, oname.ptr, oname.len);
    }
    for (int i = 0; i < TEMP_NAMLEN; ++i)
        path->ptr[i+path->len] = templ[rand()%sizeof(templ)];
    char buf [TEMP_TIMLEN];
    snprintf(buf, TEMP_TIMLEN, TIME_FMT, time(0));
    size_t buflen = strlen(buf);
    memcpy(path->ptr+path->len+TEMP_NAMLEN, buf, buflen);
    path->len += TEMP_NAMLEN+buflen;
    if (getsuf(prefix, prefix_len, &oname))
        strnadd(&path, oname.ptr, oname.len);
    STR_ADD_NULL(path);
    return path;
}

