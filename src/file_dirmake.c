/*Copyright (c) Brian B.

Permission to use, copy, modify, and/or distribute this software for any purpose
with or without fee is hereby granted.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE
OF THIS SOFTWARE.
*/
#include "../include/libex/file.h"

int makedir (const char *dir, mode_t mode) {
    str_t *s = strsplit(dir, strlen(dir), PATH_DELIM_C), *p;
    strptr_t e = { .ptr = NULL, .len = 0 };
    char *env;
    if (!s) return -1;
    int rc = -1;
    p = stralloc(s->len, 0);
    if (PATH_DELIM_C == *dir)
        p = path_add_path(p, "/", NULL);
    else if (!strnext(s, &e)) {
        if (!cmpstr(e.ptr, e.len, CONST_STR_LEN("~"))) {
            if ((env = getenv("HOME")))
                p = path_add_path(p, env, NULL);
            else {
                errno = ENOTDIR;
                goto done;
            }
        }
    }
    while (!strnext(s, &e)) {
        char *se;
        if ('$' == *e.ptr) {
            se = alloca(e.len);
            strncpy(se, e.ptr+1, e.len-1);
            se[e.len-1] = '\0';
            if ((env = getenv(se)))
                p = path_add_path(p, env, NULL);
            else {
                errno = ENOTDIR;
                goto done;
            }
        } else {
            se = alloca(e.len+1);
            strncpy(se, e.ptr, e.len);
            se[e.len] = '\0';
            p = path_add_path(p, se, NULL);
        }
    }
    rc = mkdir(p->ptr, mode);
done:
    free(p);
    free(s);
    return rc;
}
