#!/bin/sh

mkdir -p debian/usr/include/libex
mkdir -p debian/usr/lib/pkgconfig
mkdir -p debian/usr/share/doc/libex-dev
cp libex.pc debian/usr/lib/pkgconfig
cp .zbuild/_standard/_libs/libex.a debian/usr/lib
cp include/libex/* debian/usr/include/libex
cp copyright debian/usr/share/doc/libex-dev
cp changelog debian/usr/share/doc/libex-dev/changelog.Debian
gzip debian/usr/share/doc/libex-dev/changelog.Debian

cd debian
md5deep -r -l usr > DEBIAN/md5sums
cd ..
fakeroot dpkg-deb --build debian
mv debian.deb libex-dev_`cat debian/DEBIAN/control | grep Version | awk '{print $2}'`_`cat debian/DEBIAN/control | grep Architecture | awk '{print $2}'`.deb
rm -fr debian/usr/*
rmdir debian/usr
rm debian/DEBIAN/md5sums
